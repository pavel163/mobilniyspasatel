package mcs.nsign.by.mcs.screens.contacts;

import android.net.ConnectivityManager;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IPresenterContacts {

    void addContact(String user_id, String name, String surname, String phone, String accesstoken,
                    ConnectivityManager connectivityManager, int countOfContacts);

    void getAllContacts(String accesstoken, String id);

    void changeContact(String id, String name, String surname, String phone, String accesstoken,
                       ConnectivityManager connectivityManager);

    void deleteContact(String id, String accesstoken, ConnectivityManager connectivityManager);
}
