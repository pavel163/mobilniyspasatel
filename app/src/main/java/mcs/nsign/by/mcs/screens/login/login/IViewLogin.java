package mcs.nsign.by.mcs.screens.login.login;

import mcs.nsign.by.mcs.utils.NetworkCallback;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IViewLogin<T> extends NetworkCallback, ServerCallback<T> {

     void showErrorName(String error);

     void showErrorConnection(String error);
}
