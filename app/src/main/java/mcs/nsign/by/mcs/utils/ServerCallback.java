package mcs.nsign.by.mcs.utils;

/**
 * Created by Bakht on 01.10.2016.
 */
public interface ServerCallback<T> {
    void success(T response);

    void error(String message);
}
