package mcs.nsign.by.mcs.utils.power;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.widget.SosService;
import mcs.nsign.by.mcs.widget.SosWidget;

import static mcs.nsign.by.mcs.widget.WidgetReceiver.WIDGET_RESET;

public class PowerService extends Service {

    private BroadcastReceiver receiver;
    private static final String POWER_TIME = "power_time";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (((MCSApp) getApplicationContext()).getValue(Constants.TOKEN).equals("")) {
            stopSelf();
        }

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int count;
                String sCount = SettingsService.get(context, SosWidget.WIDGET_SOS);
                if (sCount.equals("")) {
                    count = 0;
                } else {
                    count = Integer.parseInt(sCount);
                }

                if (SettingsService.get(context, POWER_TIME).equals("") || System.currentTimeMillis() - Long.parseLong(SettingsService.get(context, POWER_TIME)) >= 4000) {
                    count = 0;
                    Log.d("power1", "count = 0");
                    SettingsService.set(context, POWER_TIME, String.valueOf(System.currentTimeMillis()));
                }

                Log.d("power1", "count = " + (count+1));
                if (count + 1 >= 6) {
                    Log.d("power1", "sos");
                    SettingsService.set(context, SosWidget.WIDGET_SOS, "");
                    Intent service = new Intent(context, SosService.class);
                    context.startService(service);
                } else {
                    SettingsService.set(context, SosWidget.WIDGET_SOS, String.valueOf(count + 1));
                }
            }
        };
        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, screenStateFilter);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }


    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            SettingsService.set(getApplicationContext(), WIDGET_RESET, "0");
        }
    }
}
