package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by Bakht on 01.10.2016.
 */
public interface IDirectView {
    void drawDirect(List<LatLng> points);

    void error(String error);

    void showProgress();

    void hideProgress();

    boolean checkConnect();
}
