package mcs.nsign.by.mcs.screens.info;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IViewInfo {
    void setVisibleMenu();

    void initMenuItems(List<Map<String, Object>> items);
    void setVisibleSearchList();
    void notifySearchList();
    void initSearchList(List<Map<String, Object>> data);
    boolean checkConnect();
    void showError(String error);
}
