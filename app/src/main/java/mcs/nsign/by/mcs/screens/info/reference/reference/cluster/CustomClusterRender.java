package mcs.nsign.by.mcs.screens.info.reference.reference.cluster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.google.maps.android.ui.SquareTextView;

import mcs.nsign.by.mcs.R;

/**
 * Created by Bakht on 01.10.2016.
 */
public class CustomClusterRender extends DefaultClusterRenderer<MapClusterItem> {
    private final float mDensity;
    private IconGenerator mIconGenerator;
    private SquareTextView squareTextView;
    private ShapeDrawable drawable;

    public CustomClusterRender(Context context, GoogleMap googleMap, ClusterManager<MapClusterItem> clusterManager) {
        super(context, googleMap, clusterManager);
        mDensity = context.getResources().getDisplayMetrics().density;
        mIconGenerator = new IconGenerator(context);
        mIconGenerator.setContentView(makeSquareTextView(context));
        mIconGenerator.setTextAppearance(R.style.ClusterIcon_TextAppearance);
        mIconGenerator.setBackground(makeClusterBackground());
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<MapClusterItem> cluster, MarkerOptions markerOptions) {
        drawable.getPaint().setColor(Color.parseColor("#e3e6e8"));
        Bitmap icon = mIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
    }

    private ShapeDrawable makeClusterBackground() {
        drawable = new ShapeDrawable(new OvalShape());
        return drawable;
    }

    private SquareTextView makeSquareTextView(Context context) {
        squareTextView = new SquareTextView(context);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        squareTextView.setLayoutParams(layoutParams);
        squareTextView.setId(R.id.amu_text);
        squareTextView.setTextColor(Color.BLACK);
        int twelveDpi = (int) (12 * mDensity);
        squareTextView.setPadding(twelveDpi, twelveDpi, twelveDpi, twelveDpi);
        return squareTextView;
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster cluster) {
        return cluster.getSize() > 1;
    }

    @Override
    protected String getClusterText(int bucket) {
        if (bucket < 2) {
            return String.valueOf(bucket);
        } else {
            return String.valueOf(bucket);
        }
    }

    @Override
    protected int getColor(int clusterSize) {
        return Color.parseColor("#e3e6e8");
    }

    @Override
    protected void onBeforeClusterItemRendered(MapClusterItem item, MarkerOptions markerOptions) {
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(item.getImage()));
    }
}
