package mcs.nsign.by.mcs.screens.info.firs_help;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.ebr163.core.util.ItemClickSupport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FExpandListBinding;
import mcs.nsign.by.mcs.screens.info.adapter.SearchListAdapter;
import mcs.nsign.by.mcs.ui.recyclerview.DividerItemDecoration;
import mcs.nsign.by.mcs.utils.FragmentFactory;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

/**
 * Created by mac1 on 09.09.16.
 */
public class FirstHelpFragment extends BaseBindingFragment<FExpandListBinding> implements IViewFirstHelp, SearchView.OnQueryTextListener {

    protected IModelFirstHelp modelFirstHelp;
    protected IPresenterFirstHelp presenterFirstHelp;
    protected SearchListAdapter searchListAdapter;
    private SearchListAdapter expandableAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void afterBinding() {
        super.afterBinding();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getActivity().getIntent().getStringExtra(Constants.NAME));

        modelFirstHelp = new ModelFirsHelp(getActivity().getIntent().getStringExtra(Constants.ID), getActivity().getIntent().getStringExtra(Constants.TYPE));
        presenterFirstHelp = new PresenterFirstHelp(this, modelFirstHelp);

        presenterFirstHelp.initExpandList();
        presenterFirstHelp.initSearchList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Ключевое слово");
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public int getLayout() {
        return R.layout.f_expand_list;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        presenterFirstHelp.search(newText, getActivity().getIntent().getStringExtra(Constants.TYPE));
        return true;
    }

    @Override
    public void setVisibleMenu() {
        fragmentBinder.recycler.setVisibility(View.GONE);
        fragmentBinder.expandRecycler.setVisibility(View.VISIBLE);
    }

    @Override
    public void initMenuItems(List<Map<String, Object>> items) {

    }

    @Override
    public void setVisibleSearchList() {
        fragmentBinder.recycler.setVisibility(View.VISIBLE);
        fragmentBinder.expandRecycler.setVisibility(View.GONE);
    }

    @Override
    public void notifySearchList() {
        fragmentBinder.recycler.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void initSearchList(List<Map<String, Object>> data) {
        putParentIcon(data);
        if (searchListAdapter == null) {
            searchListAdapter = new SearchListAdapter(getActivity(), data);
        }
        fragmentBinder.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentBinder.recycler.setAdapter(searchListAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.recycler.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.recycler).setOnItemClickListener((recyclerView1, i, view1) -> {
            Intent intent = new Intent(getActivity(), ActivityWithFragment.class);
            Map<String, Object> map = modelFirstHelp.getSearchItem(i);
            intent.putExtra(Constants.ID, map.get(Constants.ID).toString());
            intent.putExtra(Constants.NAME, map.get(Constants.NAME).toString());
            intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REVIEW);
            startActivity(intent);
        });
    }

    @Override
    public boolean checkConnect() {
        return false;
    }

    @Override
    public void showError(String error) {

    }

    private void putParentIcon(List<Map<String, Object>> items) {
        String icon = getActivity().getIntent().getStringExtra(Constants.PARENT_ICON);
        for (Map<String, Object> item : items) {
            item.put(Constants.PARENT_ICON, icon);
        }
    }

    @Override
    public void initExpandList(List<Map<String, Object>> items) {
        putParentIcon(items);
        if (expandableAdapter == null) {
            expandableAdapter = new SearchListAdapter(getActivity(), items);
        }
        fragmentBinder.expandRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentBinder.expandRecycler.setAdapter(expandableAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.expandRecycler.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.expandRecycler).setOnItemClickListener((recyclerView1, i, view1) -> {
            Map<String, Object> map = modelFirstHelp.getItem(i);
            Map<String, String> params = new HashMap<>();
            params.put(Constants.ID, map.get(Constants.ID).toString());
            params.put(Constants.NAME, map.get(Constants.NAME).toString());
            params.put(Constants.TYPE, getActivity().getIntent().getStringExtra(Constants.TYPE));
            if (map.get(Constants.ICON) != null) {
                params.put(Constants.PARENT_ICON, map.get(Constants.ICON).toString());
            } else {
                params.put(Constants.PARENT_ICON, map.get(Constants.PARENT_ICON).toString());
            }
            if ((int) map.get(Constants.CONTENT) == 0) {
                AppUtils.startActivityFromFragment(this, FragmentFactory.FIRST_HELP, params);
            } else {
                AppUtils.startActivityFromFragment(this, FragmentFactory.REVIEW, params);
            }
        });
    }

    @Override
    public void showEmptyMessage(String message) {
        fragmentBinder.empty.setVisibility(View.VISIBLE);
        fragmentBinder.empty.setText(message);
    }

    @Override
    public void hideEmptyMessage() {
        fragmentBinder.empty.setVisibility(View.GONE);
    }
}
