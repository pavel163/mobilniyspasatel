package mcs.nsign.by.mcs.screens.settings;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.databinding.FragmentSettingsBinding;
import mcs.nsign.by.mcs.screens.contacts.ContactsActivity;
import mcs.nsign.by.mcs.screens.feedback.FeedBackActivity;
import mcs.nsign.by.mcs.screens.profile.ProfileActivity;
import mcs.nsign.by.mcs.utils.utils.AppUtils;


public class FragmentSettings extends Fragment {

    private FragmentSettingsBinding binding;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Настройки");
        toolbar.setBackgroundColor(Color.parseColor("#e5e8e9"));

        binding.itemContacts.setOnClickListener(v1 -> {
            if (AppUtils.checkAccessNetwork(getActivity())) {
                startActivity(new Intent(getActivity(), ContactsActivity.class));
            } else {
                AppUtils.showErrorConnectDialog(getActivity(), "Проверьте подключение к интернету");
            }
        });
        binding.itemProf.setOnClickListener(v1 -> startActivity(new Intent(getActivity(), ProfileActivity.class)));
        binding.itemCallback.setOnClickListener(v-> startActivity(new Intent(getActivity(), FeedBackActivity.class)));
        return binding.getRoot();
    }
}
