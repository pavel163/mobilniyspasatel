package mcs.nsign.by.mcs.screens.info;


import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 10.09.2016.
 */
public class PresenterInfo implements IPresenterInfo {

    private IModelInfo modelInfo;
    private IViewInfo view;
    private Subscription searchSubscription;

    public PresenterInfo(IViewInfo view, IModelInfo modelInfo) {
        this.view = view;
        this.modelInfo = modelInfo;
    }

    @Override
    public void initSearchList() {
        view.initSearchList(modelInfo.getDefualtInfo());
    }

    @Override
    public void initMenu() {
        view.initMenuItems(modelInfo.getMenuItems());
    }

    @Override
    public void search(String text, String type) {
        if (text.length() >= 3) {
            Observable<Integer> observable = Observable.create((Observable.OnSubscribe<Integer>) subscriber -> {
                modelInfo.getInfo(text);
                subscriber.onNext(1);
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            searchSubscription = observable.subscribe(o -> {
                view.setVisibleSearchList();
                view.notifySearchList();
            });
        } else {
            view.setVisibleMenu();
            view.notifySearchList();
        }
    }

    @Override
    public void unSubscribe() {
        searchSubscription.unsubscribe();
    }
}
