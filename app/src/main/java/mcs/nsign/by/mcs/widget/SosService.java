package mcs.nsign.by.mcs.widget;

import android.Manifest;
import android.app.NotificationManager;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.signal.FragmentSignal;
import mcs.nsign.by.mcs.screens.signal.IPresenterSignal;
import mcs.nsign.by.mcs.screens.signal.IViewSignal;
import mcs.nsign.by.mcs.screens.signal.PresenterSignal;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.WidgetUtils;

public class SosService extends Service implements IViewSignal, LocationListener {

    public final static String ACTION_CHANGE_SOS = "mchs.send.sos.ACTION_SOS";
    public final static String ACTION_SUCCESS_SOS = "mchs.send.sos.ACTION_SUCCESS_SOS";
    public final static String ACTION_SUCCESS_STATUS = "mchs.send.sos.ACTION_SUCCESS_STATUS";
    public final static String ACTION_ERROR_STATUS = "mchs.send.sos.ACTION_ERROR_STATUS";

    private IPresenterSignal presenterSignal;
    private int widgetId;
    private String token;
    private String id;
    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private LocationManager locationManager;
    private Gson gson;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        id = ((MCSApp) getApplicationContext()).getValue("id");
        token = ((MCSApp) getApplicationContext()).getValue(Constants.TOKEN);
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();

        if (!SettingsService.get(getApplicationContext(), AppWidgetManager.EXTRA_APPWIDGET_ID).equals("")) {
            widgetId = Integer.parseInt(SettingsService.get(getApplicationContext(), AppWidgetManager.EXTRA_APPWIDGET_ID));
        } else {
            widgetId = 0;
        }

        presenterSignal = new PresenterSignal(this);
        initNotification();
        initLocationManager();

        if (SettingsService.get(getApplicationContext(), SosWidget.WIDGET_BLOCK).equals("1") && !SettingsService.get(getApplicationContext(), Constants.TIME).equals("")) {
            TimerTask timerTask = new TimerTask();
            timerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            WidgetUtils.sendMessageWidget(getApplicationContext(), widgetId, "Сигнал SOS отправлен");
        } else {
            SettingsService.set(getApplicationContext(), SosWidget.WIDGET_BLOCK, "1");
            String test = SettingsService.get(getApplicationContext(), Constants.DEMO).equals("0") ? "0" : "1";
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                presenterSignal.getSos(getApplicationContext(), token, id, Constants.latitude, Constants.latitude, test);
            } else if (AppUtils.onGPS(getApplicationContext())) {
                presenterSignal.getSos(getApplicationContext(), token, id, Constants.latitude, Constants.longitude, test);
            } else {
                if (locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                    presenterSignal.getSos(getApplicationContext(), token, id,
                            locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude(), locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude(), test);
                } else if (locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                    presenterSignal.getSos(getApplicationContext(), token, id,
                            locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude(), locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude(), test);
                } else {
                    presenterSignal.getSos(getApplicationContext(), token, id, Constants.latitude, Constants.longitude, test);
                }
            }
        }
        return START_STICKY;
    }

    private void initNotification() {
        notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.bullseye_white)
                .setContentTitle("Статус доставки:")
                .setContentText("Пожалуйста подождите")
                .setAutoCancel(true);
    }

    private void initLocationManager() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
    }

    @Override
    public void showErrorConnection(String error) {
        SettingsService.set(getApplicationContext(), SosWidget.WIDGET_BLOCK, "0");
        WidgetUtils.sendMessageWidget(getApplicationContext(), widgetId, "Сеть недоступна");
        WidgetUtils.sendErrorWidget(getApplicationContext(), error);
    }

    @Override
    public void sendSosTrue(Object obj) {
        SettingsService.set(getApplicationContext(), Constants.TIME, String.valueOf(System.currentTimeMillis()));
        TimerTask timerTask = new TimerTask();
        timerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        Double sosId = (double) ((Map) obj).get("sos_id");
        SettingsService.set(getApplicationContext(), Constants.SOS_ID, Integer.toString(sosId.intValue()));

        String message = gson.toJson(obj);
        WidgetUtils.sendSuccesSignal(getApplicationContext(), message);
    }

    @Override
    public void sendSmsStatusTrue(Object obj) {
        String response = "";
        String message = gson.toJson(obj);
        WidgetUtils.sendSuccesStatus(getApplicationContext(), message);
        if (obj instanceof List) {
            List<Map<String, Object>> contacts = (List<Map<String, Object>>) obj;
            if (contacts.size() != 0) {
                response += contacts.get(0).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(0).get(Constants.STATUS)) + "\n";
                SettingsService.set(getApplicationContext(), Constants.CONTACT_1, contacts.get(0).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(0).get(Constants.STATUS)));
            }
            if (contacts.size() > 1) {
                response += contacts.get(1).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(1).get(Constants.STATUS)) + "\n";
                SettingsService.set(getApplicationContext(), Constants.CONTACT_2, contacts.get(1).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(1).get(Constants.STATUS)));
            }
            if (contacts.size() > 2) {
                response += contacts.get(2).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(2).get(Constants.STATUS));
                SettingsService.set(getApplicationContext(), Constants.CONTACT_3, contacts.get(2).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(2).get(Constants.STATUS)));
            }
            if (contacts.size() != 0) {
                notificationBuilder.setContentText(response);
                notificationManager.notify(0, notificationBuilder.build());
            }
        }
    }

    @Override
    public void errorSend(String message) {
        SettingsService.set(getApplicationContext(), SosWidget.WIDGET_BLOCK, "0");
        WidgetUtils.sendMessageWidget(getApplicationContext(), widgetId, message);
        WidgetUtils.sendErrorWidget(getApplicationContext(), message);
    }

    @Override
    public boolean checkAccessNetwork() {
        return AppUtils.checkAccessNetwork(getApplicationContext());
    }

    @Override
    public void onLocationChanged(Location location) {
        Constants.latitude = location.getLatitude();
        Constants.longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    class TimerTask extends AsyncTask<Void, Integer, Void> {

        public long time;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            WidgetUtils.sendMessageWidget(getApplicationContext(), widgetId, "Сигнал SOS отправлен");
            time = FragmentSignal.SIGNAL_TASK - (System.currentTimeMillis() - Long.parseLong(SettingsService.get(getApplicationContext(), Constants.TIME)));
            time = TimeUnit.MILLISECONDS.toSeconds(time);
        }

        @Override
        protected Void doInBackground(Void... v) {
            try {
                for (int i = (int) time; i >= 0; i--) {
                    TimeUnit.SECONDS.sleep(1);
                    if (isCancelled()) {
                        return null;
                    }
                    publishProgress(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (values[0] > 60) {
                if (values[0] % 10 == 0) {
                    presenterSignal.getSmsStatus(getApplicationContext(), token, id);
                }
            } else {
                if (values[0] != 0 && values[0] % 60 == 0) {
                    presenterSignal.getSmsStatus(getApplicationContext(), token, id);
                }
            }
            if (values[0] % 10 == 0) {
                presenterSignal.sendMyCurrentLocation(token, SettingsService.get(getApplicationContext(), Constants.SOS_ID), String.valueOf(Constants.latitude), String.valueOf(Constants.longitude));
            }
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            SettingsService.set(getApplicationContext(), SosWidget.WIDGET_BLOCK, "0");
            WidgetUtils.sendResetWidget(getApplicationContext(), widgetId);

            if (locationManager != null) {
                if (ActivityCompat.checkSelfPermission(SosService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SosService.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    stopSelf();
                    return;
                }
                locationManager.removeUpdates(SosService.this);
            }
            stopSelf();
        }
    }
}
