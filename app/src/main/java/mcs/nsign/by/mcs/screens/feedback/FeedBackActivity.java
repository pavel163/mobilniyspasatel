package mcs.nsign.by.mcs.screens.feedback;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.ebr163.core.newApi.base.activity.BaseBindingActivity;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.AFeedBackBinding;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.ViewUtils;

public class FeedBackActivity extends BaseBindingActivity<AFeedBackBinding> implements IViewFeedBack {

    private IPresenterFeedBack presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new PresenterFeedBack(this);
        setSupportActionBar(activityBinder.appBar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activityBinder.appBar.toolbar.setTitle("Обратная связь");
        activityBinder.message.getEditText().addTextChangedListener(getTextWatcher());

        String email = SettingsService.get(this, Constants.EMAIL);
        if (!"".equals(email)) {
            activityBinder.email.setVisibility(View.GONE);
        } else {
            activityBinder.email.getEditText().setText(email);
        }

        activityBinder.send.setOnClickListener(view -> {
            if (!isEmailValid()) {
                activityBinder.email.setErrorEnabled(true);
                activityBinder.email.setError("Неправильный email");
                return;
            } else {
                activityBinder.email.setErrorEnabled(false);
            }

            if (activityBinder.email.getEditText().getText().length() == 0 && activityBinder.email.getVisibility() == View.VISIBLE) {
                activityBinder.email.setErrorEnabled(true);
                activityBinder.email.setError("Пустое поле");
                return;
            } else {
                activityBinder.email.setErrorEnabled(false);
            }

            String id = ((MCSApp) getApplicationContext()).getValue("id");
            PackageManager manager = getPackageManager();
            PackageInfo info = null;
            try {
                info = manager.getPackageInfo(getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            String message = activityBinder.message.getEditText().getText().toString();
            if (activityBinder.email.getVisibility() == View.VISIBLE) {
                message += "\nEmail: " + activityBinder.email.getEditText().getText();
            }
            message += "\nВерсия приложения: " + info.versionName;
            message += "\nОС: Андроид API" + "" + Build.VERSION.SDK_INT;
            presenter.sendMessage(this, id, message);
        });
    }

    boolean isEmailValid() {
        String email = activityBinder.email.getEditText().getText().toString();
        if (email.length() == 0) {
            return true;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (activityBinder.message.getEditText().getText().toString().length() > 1) {
                    ViewUtils.setClickableStateButton(activityBinder.send);
                } else {
                    ViewUtils.setUnClickableStateButton(activityBinder.send);
                }
            }
        };
    }

    @Override
    public void showErrorConnection(String error) {
        AppUtils.showErrorConnectDialog(this, "Проверьте подключение к интернету");
    }

    @Override
    public int getLayout() {
        return R.layout.a_feed_back;
    }

    @Override
    public boolean checkAccessNetwork() {
        return AppUtils.checkAccessNetwork(this);
    }

    @Override
    public void success(Object response) {
        Toast.makeText(FeedBackActivity.this, "Сообщение отправлено", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void error(String message) {
        Toast.makeText(FeedBackActivity.this, "Сообщение отправлено", Toast.LENGTH_SHORT).show();
        finish();
    }
}
