package mcs.nsign.by.mcs.model;

/**
 * Created by Ergashev on 29.11.2016.
 */

public class RegisterModel {

    public String name;
    public String surname;
    public String phone;
    public String imei = "imei";
    public String os = "android";
    public String pushToken = "pushtoken";
}
