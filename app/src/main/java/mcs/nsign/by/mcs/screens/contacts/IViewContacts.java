package mcs.nsign.by.mcs.screens.contacts;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IViewContacts {

     void showToast(String error);

     void showErrorConnection(String error);

     void showProgressDialog();

     void hideProgressDialog();
}
