package mcs.nsign.by.mcs.model;

import java.io.Serializable;

/**
 * Created by Ruslan on 30.03.2016.
 */
public class ContactsObject implements Serializable {

    private String id;

    private String user_id;

    private String name;

    private String phone;

    private String created_by;

    private String last_name;

    public ContactsObject(String id, String user_id, String name, String phone, String created_by, String last_name){
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.phone = phone;
        this.created_by = created_by;
        this.last_name = last_name;
    }

    public String getId(){
        return id;
    }

    public String getUserId(){
        return user_id;
    }

    public String getName(){
        return name;
    }

    public String getPhone(){
        return phone;
    }

    public String getCreatedBy(){
        return created_by;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLastName() {
        return last_name;
    }
}
