package mcs.nsign.by.mcs.utils;

import java.util.Comparator;
import java.util.Map;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.utils.utils.MapUtils;

/**
 * Created by mac1 on 21.09.16.
 */
public class ReferenceComparator implements Comparator<Map<String, Object>> {
    @Override
    public int compare(Map<String, Object> marker1, Map<String, Object> marker2) {
        double distance1 = MapUtils.getDestantionInDouble(marker1);
        double distance2 = MapUtils.getDestantionInDouble(marker2);

        marker1.put(Constants.DISTANCE, MapUtils.parseInDistantion(distance1));
        marker2.put(Constants.DISTANCE, MapUtils.parseInDistantion(distance2));

        if (distance1 > distance2) {
            return 1;
        } else if (distance1 < distance2) {
            return -1;
        } else {
            return 0;
        }
    }
}
