package mcs.nsign.by.mcs.model;

/**
 * Created by mac1 on 18.08.16.
 */
public class Profile {

    public String name;
    public String phone;
    public String email;
    public String surname;
    public String age;
    public String blood;
    public String illness;
}
