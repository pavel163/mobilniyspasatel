package mcs.nsign.by.mcs.screens.info.review;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 11.09.2016.
 */
public interface IModelReview {
    List<Map<String, Object>> getInfo(String parentId);
}
