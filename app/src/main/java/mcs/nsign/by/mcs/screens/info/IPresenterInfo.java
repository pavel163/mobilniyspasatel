package mcs.nsign.by.mcs.screens.info;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IPresenterInfo {

    void initSearchList();

    void initMenu();

    void search(String text, String type);

    void unSubscribe();
}
