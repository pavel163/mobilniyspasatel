package mcs.nsign.by.mcs.utils.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.Map;

import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by mac1 on 06.10.16.
 */

public class MapUtils {

    public static String getDestantion(Map<String, Object> map) {
        LatLng myPosition = new LatLng(Constants.latitude, Constants.longitude);
        LatLng positionMarker = new LatLng(Double.parseDouble(map.get(Constants.LATITUDE).toString()), Double.parseDouble(map.get(Constants.LONGITUDE).toString()));
        double distance = SphericalUtil.computeDistanceBetween(myPosition, positionMarker);
        return parseInDistantion(distance);
    }

    public static String parseInDistantion(double distance) {
        if (distance < 1000) {
            return String.valueOf((int) distance) + " метров";
        } else {
            return String.format("%.2f", distance / 1000) + " км";
        }
    }

    public static double getDestantionInDouble(Map<String, Object> map) {
        LatLng myPosition = new LatLng(Constants.latitude, Constants.longitude);
        LatLng positionMarker = new LatLng(Double.parseDouble(map.get(Constants.LATITUDE).toString()), Double.parseDouble(map.get(Constants.LONGITUDE).toString()));
        return SphericalUtil.computeDistanceBetween(myPosition, positionMarker);
    }
}
