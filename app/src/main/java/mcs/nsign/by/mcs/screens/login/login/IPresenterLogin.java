package mcs.nsign.by.mcs.screens.login.login;

import mcs.nsign.by.mcs.model.RegisterModel;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IPresenterLogin {

    void tryToEnter(RegisterModel registerModel);
}
