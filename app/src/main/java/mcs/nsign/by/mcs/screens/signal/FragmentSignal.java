package mcs.nsign.by.mcs.screens.signal;

import android.Manifest;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.flurry.android.FlurryAgent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yandex.metrica.YandexMetrica;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.adapters.SamplePagerAdapter;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FSignalBinding;
import mcs.nsign.by.mcs.screens.contacts.ContactsActivity;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.widget.SosService;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class FragmentSignal extends BaseBindingFragment<FSignalBinding> implements LocationListener {

    private double myX, myY;
    private long firstTime = 0, secondTime = 0;
    private LocationManager locationManager;
    private SignalTask signalTask;
    private TimerTask timerTask;
    private Toolbar toolbar;
    private int AUTO_SCROLL = 4000;
    public static final int SIGNAL_TASK = 120000;
    private BroadcastReceiver broadcastReceiver;
    private Gson gson;
    private Spannable spannable;
    private int textSosSize = 18;
    private IntentFilter intentFilter;

    @Override
    protected void afterBinding() {
        super.afterBinding();
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Сигнал SOS");
        toolbar.setBackgroundColor(Color.parseColor("#e5e8e9"));
        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();
        AppUtils.sendScreenNameStatistic("signal_screen", "Сигнал");

        spannable = new SpannableString("нажми\nSOS\nудерживай");
        spannable.setSpan(new RelativeSizeSpan(4f), 6, 10, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        fragmentBinder.btnSos.setText(spannable);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase(SosService.ACTION_SUCCESS_SOS)) {
                    Map<String, Object> response = gson.fromJson(intent.getStringExtra(Constants.MESSAGE), Map.class);
                    sendSosTrue(response);
                } else if (intent.getAction().equalsIgnoreCase(SosService.ACTION_SUCCESS_STATUS)) {
                    List<Map<String, Object>> response = gson.fromJson(intent.getStringExtra(Constants.MESSAGE), List.class);
                    sendSmsStatusTrue(response);
                } else if (intent.getAction().equalsIgnoreCase(SosService.ACTION_ERROR_STATUS)) {
                    String error = intent.getStringExtra(Constants.MESSAGE);
                    if (error.equals("0")) {
                        Toast.makeText(getActivity(), "Нет доступа к сети", Toast.LENGTH_LONG).show();
                    } else {
                        errorSend("");
                    }
                }
            }
        };

        intentFilter = new IntentFilter();
        intentFilter.addAction(SosService.ACTION_SUCCESS_SOS);
        intentFilter.addAction(SosService.ACTION_ERROR_STATUS);
        intentFilter.addAction(SosService.ACTION_SUCCESS_STATUS);

        if (SettingsService.get(getActivity(), Constants.DEMO).equals("0")) {
            fragmentBinder.swSignal.switchButton.setChecked(true);
            fragmentBinder.swSignal.switchButton.setText("Уведомлять МЧС");
            fragmentBinder.swSignal.switchButton.setTextColor(Color.RED);
        } else {
            fragmentBinder.swSignal.switchButton.setText("Не уведомлять МЧС");
            fragmentBinder.swSignal.switchButton.setTextColor(Color.parseColor("#455a64"));
            fragmentBinder.swSignal.switchButton.setAlpha(0.6f);
        }

        fragmentBinder.swSignal.switchButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                SettingsService.set(getActivity(), Constants.DEMO, "0");
                fragmentBinder.swSignal.switchButton.setText("Уведомлять МЧС");
                fragmentBinder.swSignal.switchButton.setTextColor(Color.RED);
            } else {
                SettingsService.set(getActivity(), Constants.DEMO, "1");
                fragmentBinder.swSignal.switchButton.setText("Не уведомлять МЧС");
                fragmentBinder.swSignal.switchButton.setTextColor(Color.parseColor("#455a64"));
                fragmentBinder.swSignal.switchButton.setAlpha(0.6f);
            }
        });

        Animation blinkAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.blinking_animation);
        fragmentBinder.locationMessage.startAnimation(blinkAnimation);

        // TODO: 16.08.2016 переписать на rx
        fragmentBinder.btnSos.setOnTouchListener((v1, event) -> {
            int action = event.getActionMasked();
            if (action == MotionEvent.ACTION_DOWN) {
                firstTime = System.currentTimeMillis();
                signalTask = new SignalTask();
                signalTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                fragmentBinder.viewPager.stopAutoScroll();
                fragmentBinder.viewPager.setCurrentItem(2);
            } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
                secondTime = System.currentTimeMillis();
                if (secondTime - firstTime < 5000) {
                    signalTask.cancel(true);
                    firstTime = 0;
                    secondTime = 0;
                    fragmentBinder.btnSos.setTextSize(textSosSize);
                    fragmentBinder.btnSos.setText(spannable);
                    fragmentBinder.progressLeft.setVisibility(View.VISIBLE);
                    fragmentBinder.progressRight.setVisibility(View.VISIBLE);
                    fragmentBinder.content.stopRippleAnimation();
                    fragmentBinder.viewPager.startAutoScroll(AUTO_SCROLL);
                }
            }
            return false;
        });

        tryToShowGPSDialog();
        FragmentSignalPermissionsDispatcher.createLocationManagerWithCheck(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initVP();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!AppUtils.checkTimePeriod(getActivity())) {
            drawRedBackground();
            if (!SettingsService.get(getActivity(), Constants.CONTACT_1).equals("")) {
                fragmentBinder.signalSucces.contact1.setText(SettingsService.get(getActivity(), Constants.CONTACT_1));
            }
            if (!SettingsService.get(getActivity(), Constants.CONTACT_2).equals("")) {
                fragmentBinder.signalSucces.contact2.setText(SettingsService.get(getActivity(), Constants.CONTACT_2));
            }
            if (!SettingsService.get(getActivity(), Constants.CONTACT_3).equals("")) {
                fragmentBinder.signalSucces.contact3.setText(SettingsService.get(getActivity(), Constants.CONTACT_3));
            }
            timerTask = new TimerTask();
            timerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            SettingsService.set(getActivity(), Constants.CONTACT_1, "");
            SettingsService.set(getActivity(), Constants.CONTACT_2, "");
            SettingsService.set(getActivity(), Constants.CONTACT_3, "");
            fragmentBinder.viewPager.startAutoScroll(AUTO_SCROLL);
        }
    }

    private void initVP() {
        List<View> pages = new ArrayList<>();

        pages.add(getVPPage(R.string.signal_msg2));

        View view = getVPPage(R.string.signal_msg3);
        ((TextView) view.findViewById(R.id.text)).setText(Html.fromHtml(getString(R.string.signal_msg3)));
        view.setOnClickListener(view1 -> {
            startActivity(new Intent(getActivity(), ContactsActivity.class));
        });

        pages.add(view);
        pages.add(getVPPage(R.string.signal_msg1));
        pages.add(getVPPage(R.string.signal_msg_dot4));

        SamplePagerAdapter pagerAdapter = new SamplePagerAdapter(pages);
        fragmentBinder.viewPager.setAdapter(pagerAdapter);
        fragmentBinder.viewPager.setInterval(AUTO_SCROLL);

        fragmentBinder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    fragmentBinder.indicator.dot1.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot));
                    fragmentBinder.indicator.dot2.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot3.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot4.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                } else if (position == 1) {
                    fragmentBinder.indicator.dot1.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot2.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot));
                    fragmentBinder.indicator.dot3.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot4.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                } else if (position == 2) {
                    fragmentBinder.indicator.dot1.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot2.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot3.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot));
                    fragmentBinder.indicator.dot4.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                } else {
                    fragmentBinder.indicator.dot1.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot2.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot3.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot_unselect));
                    fragmentBinder.indicator.dot4.setBackgroundDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.dot));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private View getVPPage(@StringRes int stringRes) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View page = inflater.inflate(R.layout.page_vp, null);
        TextView textView = (TextView) page.findViewById(R.id.text);
        textView.setText(getString(stringRes));
        return page;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragmentBinder.viewPager.stopAutoScroll();
        if (timerTask != null) {
            timerTask.cancel(true);
        }
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(this);
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void createLocationManager() {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        FragmentSignalPermissionsDispatcher.onRequestPermissionsResult(FragmentSignal.this, requestCode, grantResults);
    }

    public void showAccuracy(Location location) {
        String acc = String.valueOf(((int) location.getAccuracy()));
        String last_number = String.valueOf(acc.charAt(acc.length() - 1));
        if (last_number.equals("0") || last_number.equals("5") || last_number.equals("6")
                || last_number.equals("7") || last_number.equals("8") || last_number.equals("9")) {
            fragmentBinder.locationMessage.setText("Точность: " + acc + " метров");
        }

        if (last_number.equals("1")) {
            fragmentBinder.locationMessage.setText("Точность: " + acc + " метр");
        }

        if (last_number.equals("2") || last_number.equals("3") || last_number.equals("4")) {
            fragmentBinder.locationMessage.setText("Точность: " + acc + " метра");
        }

        fragmentBinder.locationMessage.clearAnimation();
        myX = location.getLatitude();
        myY = location.getLongitude();
        Constants.latitude = myX;
        Constants.longitude = myY;
        if (AppUtils.checkTimePeriod(getActivity())) {
            fragmentBinder.btnSos.setEnabled(true);
        }
    }

    private void tryToShowGPSDialog() {
        if (!AppUtils.onGPS(getActivity())) {
            getActivity().runOnUiThread(() -> new AlertDialog.Builder(getActivity())
                    .setTitle("Включить GPS?")
                    .setPositiveButton(getResources().getString(R.string.OK),
                            (dialog, which) -> {
                                Intent gpsOptionsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(gpsOptionsIntent);
                            })
                    .setNegativeButton("Нет", (dialog, which) -> {
                    }).show());
        }

    }

    public void sendSosTrue(Object obj) {
        Map<String, Object> response = (Map<String, Object>) obj;
        List<Map<String, Object>> status = (List<Map<String, Object>>) response.get("contacts");
        Toast.makeText(getActivity(), "Сигнал SOS успешно послан", Toast.LENGTH_SHORT).show();
        fragmentBinder.content.stopRippleAnimation();
        drawRedBackground();
        SettingsService.set(getActivity(), Constants.TIME, String.valueOf(System.currentTimeMillis()));
        timerTask = new TimerTask();
        timerTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        if (status instanceof List) {
            if (status.size() != 0) {
                fragmentBinder.signalSucces.contact1.setText(status.get(0).get(Constants.NAME).toString() + ": Доставляется");
                SettingsService.set(getActivity(), Constants.CONTACT_1, status.get(0).get(Constants.NAME).toString() + ": Доставляется");
                if (status.size() > 1) {
                    fragmentBinder.signalSucces.contact2.setText(status.get(1).get(Constants.NAME).toString() + ": Доставляется");
                    SettingsService.set(getActivity(), Constants.CONTACT_2, status.get(1).get(Constants.NAME).toString() + ": Доставляется");
                }
                if (status.size() > 2) {
                    fragmentBinder.signalSucces.contact3.setText(status.get(2).get(Constants.NAME).toString() + ": Доставляется");
                    SettingsService.set(getActivity(), Constants.CONTACT_3, status.get(2).get(Constants.NAME).toString() + ": Доставляется");
                }
            }
        } else {
            errorSend("Нет подключения к интернет");
        }
    }

    public void sendSmsStatusTrue(Object obj) {
        if (obj instanceof List) {
            List<Map<String, Object>> contacts = (List<Map<String, Object>>) obj;
            if (contacts.size() != 0) {
                fragmentBinder.signalSucces.contact1.setText(contacts.get(0).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(0).get(Constants.STATUS)));
            }
            if (contacts.size() > 1) {
                fragmentBinder.signalSucces.contact2.setText(contacts.get(1).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(1).get(Constants.STATUS)));
            }
            if (contacts.size() > 2) {
                fragmentBinder.signalSucces.contact3.setText(contacts.get(2).get(Constants.NAME).toString() + ": " + Constants.getStatus((Double) contacts.get(2).get(Constants.STATUS)));
            }
        }
    }

    private void drawRedBackground() {
        fragmentBinder.btnSos.setTextSize(textSosSize);
        fragmentBinder.swSignal.swContainer.setVisibility(View.GONE);
        fragmentBinder.btnSos.setText("Сигнал\nSOS\nпослан");
        fragmentBinder.progressLeft.setVisibility(View.INVISIBLE);
        fragmentBinder.progressRight.setVisibility(View.INVISIBLE);
        fragmentBinder.viewPager.setVisibility(View.GONE);
        fragmentBinder.indicator.indicator.setVisibility(View.GONE);
        fragmentBinder.signalSucces.getRoot().setVisibility(View.VISIBLE);
        if (SettingsService.get(getActivity(), Constants.DEMO).equals("1")) {
            fragmentBinder.signalSucces.mchs.setVisibility(View.GONE);
        } else {
            fragmentBinder.signalSucces.mchs.setVisibility(View.VISIBLE);
        }
        fragmentBinder.getRoot().setBackgroundColor(Color.parseColor("#CCcc0000"));
        getActivity().findViewById(R.id.toolbar).setBackgroundColor(Color.parseColor("#CCcc0000"));
        fragmentBinder.viewPager.stopAutoScroll();
        fragmentBinder.btnSos.setEnabled(false);
    }

    public void errorSend(String message) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Интернет соединение не стабильное, звоните 112")
                .setPositiveButton(getResources().getString(R.string.OK),
                        (dialog, which) -> {
                            FragmentSignalPermissionsDispatcher.opendDualWithCheck(FragmentSignal.this);
                            showDialogBadConnection(dialog);
                        })
                .setNegativeButton("Отмена", (dialog, which) -> showDialogBadConnection(dialog)).show();
    }

    private void showDialogBadConnection(DialogInterface dialog) {
        viewSosNormalState();
        fragmentBinder.content.stopRippleAnimation();
        dialog.dismiss();
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    protected void opendDual() {
        Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "112"));
        startActivity(dialIntent);
    }

    @Override
    public void onLocationChanged(Location location) {
        showAccuracy(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
    }

    @Override
    public void onProviderEnabled(String s) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationManager.getLastKnownLocation(s) != null) {
            showAccuracy(locationManager.getLastKnownLocation(s));
        }
    }

    @Override
    public void onProviderDisabled(String s) {
    }

    @Override
    public int getLayout() {
        return R.layout.f_signal;
    }

    private void viewSosNormalState() {
        fragmentBinder.signalSucces.getRoot().setVisibility(View.GONE);
        toolbar.setBackgroundColor(Color.parseColor("#e5e8e9"));
        fragmentBinder.getRoot().setBackgroundColor(Color.parseColor("#e5e8e9"));
        fragmentBinder.swSignal.swContainer.setVisibility(View.VISIBLE);
        fragmentBinder.btnSos.setTextSize(textSosSize);
        fragmentBinder.btnSos.setText(spannable);
        fragmentBinder.progressLeft.setVisibility(View.VISIBLE);
        fragmentBinder.progressRight.setVisibility(View.VISIBLE);
        fragmentBinder.viewPager.setVisibility(View.VISIBLE);
        fragmentBinder.indicator.indicator.setVisibility(View.VISIBLE);
        fragmentBinder.btnSos.setEnabled(true);
        fragmentBinder.viewPager.startAutoScroll(AUTO_SCROLL);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(broadcastReceiver);
        FlurryAgent.onEndSession(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
        FlurryAgent.onStartSession(getActivity());
    }

    private void callSos(){
        if (fragmentBinder.swSignal.switchButton.isChecked()){
            Intent i = new Intent(Intent.ACTION_CALL);
            i.setData(Uri.parse("tel:112"));
            startActivity(i);
        }
    }

    class SignalTask extends AsyncTask<Void, Integer, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fragmentBinder.viewPager.setCurrentItem(2);
            fragmentBinder.btnSos.setText("5");
            fragmentBinder.btnSos.setTextSize(80);
            fragmentBinder.progressLeft.setVisibility(View.INVISIBLE);
            fragmentBinder.progressRight.setVisibility(View.INVISIBLE);
            fragmentBinder.content.startRippleAnimation();
        }

        @Override
        protected Void doInBackground(Void... v) {
            try {
                for (int i = 1; i <= 5; i++) {
                    TimeUnit.SECONDS.sleep(1);
                    if (isCancelled()) {
                        return null;
                    }
                    publishProgress(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            fragmentBinder.btnSos.setText(String.valueOf(5 - values[0]));
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            AppUtils.sendCustomEventStatistic("id", "Сигнал сос послан", "sos");
            FlurryAgent.logEvent("Сигнал сос послан");
            YandexMetrica.reportEvent("Сигнал сос послан");
            fragmentBinder.btnSos.setEnabled(false);
            SettingsService.set(getActivity(), AppWidgetManager.EXTRA_APPWIDGET_ID, SettingsService.get(getActivity(), "widgetID"));
            Intent service = new Intent(getActivity(), SosService.class);
            getActivity().startService(service);
            callSos();
        }
    }


    class TimerTask extends AsyncTask<Void, Integer, Void> {

        public long time;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            time = SIGNAL_TASK - (System.currentTimeMillis() - Long.parseLong(SettingsService.get(getActivity(), Constants.TIME)));
            time = TimeUnit.MILLISECONDS.toSeconds(time);
        }

        @Override
        protected Void doInBackground(Void... v) {
            try {
                for (int i = (int) time; i >= 0; i--) {
                    TimeUnit.SECONDS.sleep(1);
                    if (isCancelled()) {
                        return null;
                    }
                    publishProgress(i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            fragmentBinder.signalSucces.textInfo.setText("Сообщение о ЧС отправлено. " +
                    "Повторная отправка SOS возможна через " + values[0] + " сек.");
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            viewSosNormalState();
        }
    }
}
