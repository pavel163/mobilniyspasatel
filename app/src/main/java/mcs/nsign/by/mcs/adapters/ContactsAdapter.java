package mcs.nsign.by.mcs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.model.ContactsObject;
import mcs.nsign.by.mcs.screens.contacts.EventCallBack;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

public class ContactsAdapter extends BaseAdapter {

    private List<ContactsObject> list;
    private LayoutInflater layoutInflater;
    private Context context;
    private EventCallBack eventCallBack;

    public ContactsAdapter(Context ctx, List list, EventCallBack eventCallBack) {
        this.context = ctx;
        this.list = list;
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.eventCallBack = eventCallBack;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ContactsObject getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View currentView, ViewGroup parent) {
        View view = currentView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_contacts_adapter, parent, false);
        }
        ContactsObject contObj = getContactsObj(position);

        TextView name = (TextView) view.findViewById(R.id.name);
        name.setText(contObj.getName());

        TextView phone = (TextView) view.findViewById(R.id.phone);
        AppUtils.getPhoneMask().installOn(phone);
        phone.setText("+" + contObj.getPhone().substring(1));

        TextView surname = (TextView) view.findViewById(R.id.surname);
        surname.setText(contObj.getLastName());

        ImageButton change = (ImageButton) view.findViewById(R.id.btnChange);
        change.setTag(position);
        ImageButton delete = (ImageButton) view.findViewById(R.id.btnClear);
        delete.setTag(position);

        change.setOnClickListener(v -> change((Integer) change.getTag()));
        delete.setOnClickListener(v -> delete((Integer) delete.getTag()));
        return view;
    }

    private void delete(int position) {
        eventCallBack.delete(list.get(position));
    }

    private void change(int position) {
        eventCallBack.change(list.get(position));
    }

    private ContactsObject getContactsObj(int position) {
        return list.get(position);
    }
}
