package mcs.nsign.by.mcs.screens.info.reference.reference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.reference.ModelReference;

/**
 * Created by mac1 on 14.09.16.
 */
public class ModelReferences extends ModelReference{

    private List<Map<String, Object>> referenceList;
    private String parentId;

    public ModelReferences(String parentId) {
        super();
        this.parentId = parentId;
        referenceList = new ArrayList<>();
    }

    @Override
    public List<Map<String, Object>> getReferenceList() {
        List<Map<String, Object>> result = MCSApp.dbHelper.getInstitutions(parentId);

        for (Map<String, Object> map : result) {
            map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getReferenceIcon(map.get(Constants.REFERENCE).toString()));
        }

        referenceList.clear();
        referenceList.addAll(result);
        return referenceList;
    }

    @Override
    public void loadNewSearchList(String text, String type) {
        List<Map<String, Object>> result = MCSApp.dbHelper.getInstitutionsToNameAndType(text, parentId);

        for (Map<String, Object> map : result) {
            map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getReferenceIcon(map.get(Constants.REFERENCE).toString()));
        }

        listSearch.clear();
        listSearch.addAll(result);
    }
}
