package mcs.nsign.by.mcs.screens.profile;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.widget.DatePicker;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FProfileBinding;
import mcs.nsign.by.mcs.model.Profile;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.ViewUtils;
import rx.Observable;

public class FragmentProfile extends BaseBindingFragment<FProfileBinding> implements IViewProfile, DatePickerDialog.OnDateSetListener {

    private IPresenterProfile presenter;
    private String accesstoken;
    private String id;
    private ProgressDialog progressDialog;
    private boolean checkfields = false;

    @Override
    protected void afterBinding() {
        super.afterBinding();
        initFields();
        DatePickerDialog datePicker;
        AppUtils.sendScreenNameStatistic("profile_screen", "Экран профиля");
        if (fragmentBinder.age.getEditText().getText().length() != 0) {
            String[] data = fragmentBinder.age.getEditText().getText().toString().split(".");
            datePicker = new DatePickerDialog(getActivity(), this, Integer.parseInt(data[2]), Integer.parseInt(data[1]), Integer.parseInt(data[0]));
        } else {
            datePicker = new DatePickerDialog(getActivity(), this, 1980, 01, 01);
        }

        accesstoken = ((MCSApp) getActivity().getApplicationContext()).getValue("access_token");
        id = (((MCSApp) getActivity().getApplicationContext()).getValue("id"));

        presenter = new PresenterProfile(this);
        fragmentBinder.btnSave.setOnClickListener(v -> saveProfileInfo());
        fragmentBinder.age.getEditText().setOnClickListener(v -> datePicker.show());
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Пожалуйста подождите...");
        presenter.getProfile(getActivity(), accesstoken, id);
    }

    private void initFields() {
        Observable<Boolean> shouldEnableButtons = Observable.combineLatest(
                RxTextView.afterTextChangeEvents(fragmentBinder.etName),
                RxTextView.afterTextChangeEvents(fragmentBinder.etSurname),
                RxTextView.afterTextChangeEvents(fragmentBinder.etEmail),
                (name, surname, email) -> {
                    if (name.editable().length() != 0 && surname.editable().length() != 0 && isEmailValid(email.editable()) && email.editable().length() != 0) {
                        return true;
                    } else {
                        return false;
                    }
                });

        shouldEnableButtons.subscribe(enabled -> {
            showError();
            if (enabled) {
                ViewUtils.setClickableStateButton(fragmentBinder.btnSave);
            } else {
                ViewUtils.setUnClickableStateButton(fragmentBinder.btnSave);
            }
        });
        AppUtils.getPhoneMask().installOn(fragmentBinder.etPhone);
        fragmentBinder.etPhone.setText(SettingsService.get(getActivity(), Constants.PHONE));
    }

    private void showError() {
        if (!checkfields) {
            return;
        }
        checkNotEmptyFields(fragmentBinder.name);
        checkNotEmptyFields(fragmentBinder.surname);
        checkNotEmptyFields(fragmentBinder.email);

        if (!isEmailValid(fragmentBinder.etEmail.getText().toString())) {
            fragmentBinder.email.setError("Неправильный email");
            fragmentBinder.email.setErrorEnabled(true);
        }
    }

    private void checkNotEmptyFields(TextInputLayout fields) {
        if (fields.getEditText().getText().toString().length() == 0) {
            fields.setError("Пустое поле");
            fields.setErrorEnabled(true);
        } else {
            fields.setErrorEnabled(false);
        }
    }

    boolean isEmailValid(CharSequence email) {
        if (fragmentBinder.etEmail.getText().toString().length() == 0) {
            return true;
        }
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void saveProfileInfo() {
        saveLocal();
        Profile profile = new Profile();
        profile.name = fragmentBinder.etName.getText().toString();
        profile.surname = fragmentBinder.etSurname.getText().toString();
        profile.email = fragmentBinder.etEmail.getText().toString().trim();
        profile.age = fragmentBinder.etAge.getText().toString().trim();
        profile.illness = fragmentBinder.etIllness.getText().toString().trim();
        profile.blood = fragmentBinder.spinner.getSelectedItem().toString();

        presenter.changeProfile(getActivity(), accesstoken, id, profile);
        AppUtils.hideKeyBoard(getActivity());
    }

    private void saveLocal() {
        SettingsService.set(getActivity(), Constants.NAME, fragmentBinder.etName.getText().toString());
        SettingsService.set(getActivity(), Constants.SURNAME, fragmentBinder.etSurname.getText().toString());
        SettingsService.set(getActivity(), Constants.PHONE, fragmentBinder.etPhone.getText().toString());
        SettingsService.set(getActivity(), Constants.EMAIL, fragmentBinder.etEmail.getText().toString().trim());
        SettingsService.set(getActivity(), Constants.AGE, fragmentBinder.etAge.getText().toString().trim());
        SettingsService.set(getActivity(), Constants.ILLNESS, fragmentBinder.etIllness.getText().toString().trim());
        SettingsService.set(getActivity(), Constants.BLOOD, fragmentBinder.spinner.getSelectedItem().toString());
    }

    @Override
    public void showError(String error) {
        AppUtils.showErrorConnectDialog(getActivity(), error);
    }

    @Override
    public void successSaveProfile(Object obj) {
        Snackbar.make(fragmentBinder.getRoot(), "Данные сохранены", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void successGetProfile(Object profile) {
        checkfields = true;
        if (profile != null && profile instanceof Map) {
            Map<String, Object> prof = (Map<String, Object>) profile;
            if (prof.get("first_name") == null || prof.get("first_name").equals("")) {
                fragmentBinder.etName.setText(SettingsService.get(getActivity(), Constants.NAME));
            } else {
                fragmentBinder.etName.setText(prof.get("first_name").toString());
            }

            if (prof.get("last_name") == null || prof.get("last_name").equals("")) {
                fragmentBinder.etSurname.setText(SettingsService.get(getActivity(), Constants.SURNAME));
            } else {
                fragmentBinder.etSurname.setText(prof.get("last_name").toString());
            }

            if (prof.get("email") == null || prof.get("email").equals("")) {
                fragmentBinder.etEmail.setText(SettingsService.get(getActivity(), Constants.EMAIL));
            } else {
                fragmentBinder.etEmail.setText(prof.get("email").toString());
            }

            if (prof.get(Constants.AGE) == null || prof.get(Constants.AGE).equals("")) {
                fragmentBinder.etAge.setText(SettingsService.get(getActivity(), Constants.AGE));
            } else {
                fragmentBinder.etAge.setText(prof.get(Constants.AGE).toString().substring(0, prof.get(Constants.AGE).toString().lastIndexOf(".")));
            }

            if (prof.get("chronic_disease") == null || prof.get("chronic_disease").equals("")) {
                fragmentBinder.etIllness.setText(SettingsService.get(getActivity(), Constants.ILLNESS));
            } else {
                fragmentBinder.etIllness.setText(prof.get("chronic_disease").toString());
            }

            if (prof.get(Constants.BLOOD) == null || prof.get(Constants.BLOOD).equals("")) {
                selectSpinnerItem(SettingsService.get(getActivity(), Constants.BLOOD));
            } else {
                selectSpinnerItem(prof.get(Constants.BLOOD).toString());
            }
        } else {
            showError("Нет подключения к интернет");
        }
    }

    private void selectSpinnerItem(String text) {
        if (text.contains("Первая положительная")) {
            fragmentBinder.spinner.setSelection(1);
        } else if (text.contains("Первая отрицательная")) {
            fragmentBinder.spinner.setSelection(2);
        } else if (text.toString().contains("Вторая положительная")) {
            fragmentBinder.spinner.setSelection(3);
        } else if (text.contains("Вторая отрицательная")) {
            fragmentBinder.spinner.setSelection(4);
        } else if (text.contains("Третья положительная")) {
            fragmentBinder.spinner.setSelection(5);
        } else if (text.contains("Третья отрицательная")) {
            fragmentBinder.spinner.setSelection(6);
        } else if (text.contains("Четвертая положительная")) {
            fragmentBinder.spinner.setSelection(7);
        } else if (text.contains("Четвертая отрицательная")) {
            fragmentBinder.spinner.setSelection(8);
        } else {
            fragmentBinder.spinner.setSelection(0);
        }
    }

    @Override
    public int getLayout() {
        return R.layout.f_profile;
    }

    @Override
    public boolean checkAccessNetwork() {
        return AppUtils.checkAccessNetwork(getActivity());
    }

    @Override
    public void showLoadingProcess() {
        progressDialog.show();
    }

    @Override
    public void hideLoadingProcess() {
        progressDialog.hide();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        fragmentBinder.age.getEditText().setText(null);
        String month;
        String day;
        if (i1 + 1 >= 10) {
            month = String.valueOf(i1 + 1);
        } else {
            month = "0" + (i1 + 1);
        }

        if (i2 >= 10) {
            day = String.valueOf(i2);
        } else {
            day = "0" + i2;
        }
        fragmentBinder.age.getEditText().setText(day + "." + month + "." + i);
    }
}
