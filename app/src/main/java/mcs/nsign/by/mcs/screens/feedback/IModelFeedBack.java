package mcs.nsign.by.mcs.screens.feedback;

import mcs.nsign.by.mcs.utils.OnServerCallback;

/**
 * Created by mac1 on 25.08.16.
 */
public interface IModelFeedBack extends OnServerCallback {

    void sendMessage(String id, String message);
}
