package mcs.nsign.by.mcs.screens.info.download;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.screens.info.download.model.Download;

/**
 * Created by Bakht on 02.10.2016.
 */

public class DownloadFragment extends DialogFragment {

    public static final String MESSAGE_PROGRESS = "message_progress";
    public static final String ERROR_LOAD = "error_load";
    private ProgressBar progressBar;
    private TextView textProgress;
    private TextView count;
    private boolean savedInstantstate = false;
    private boolean downloadData = false;

    private DownloadListener downloadListener;

    public interface DownloadListener {
        void finish();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver();
        startDownload();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setCancelable(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.f_download, null);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        textProgress = (TextView) view.findViewById(R.id.progress_text);
        count = (TextView) view.findViewById(R.id.count);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle("Загрузка данных")
                .setView(view);
        return adb.create();
    }

    private void registerReceiver() {
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MESSAGE_PROGRESS);
        intentFilter.addAction(ERROR_LOAD);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MESSAGE_PROGRESS)) {
                Download download = intent.getParcelableExtra("download");
                progressBar.setProgress(download.getProgress());
                count.setText("Загружено данных " + download.getNumber() + " из " + download.getTotalNumber());
                if (download.getProgress() > 100) {
                    textProgress.setText("Загружено " + 100 + "/100 %");
                } else {
                    textProgress.setText("Загружено " + download.getProgress() + "/100 %");
                }
            } else if (intent.getAction().equals(ERROR_LOAD)) {
                if (intent.getBooleanExtra("flag", false)) {
                    Toast.makeText(getActivity(), "Ошибка загрузки данных", Toast.LENGTH_LONG).show();
                } else {
                    downloadListener.finish();
                }
                downloadData = true;
                if (!savedInstantstate) {
                    dismiss();
                }
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        savedInstantstate = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (downloadData == true) {
            dismiss();
        } else {
            savedInstantstate = false;
        }
    }

    private void startDownload() {
        Intent intent = new Intent(getActivity(), DownloadService.class);
        getActivity().startService(intent);
    }

    public void setDownloadListener(DownloadListener downloadListener) {
        this.downloadListener = downloadListener;
    }
}
