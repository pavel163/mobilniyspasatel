package mcs.nsign.by.mcs.service.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.utils.ReferenceComparator;


/**
 * Created by Bakht on 08.09.2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static String DB_PATH = "/data/data/mcs.nsign.by.mcs/databases/";
    private static final String DATABASE_NAME = "mchs.db";
    public static final String ITEMS = "items";
    public static final String CONTENTS = "contents";
    public static final String INSTITUTIONS = "institutions";
    public static final String REFERENCE = "reference";
    public static final String TYPES = "types";

    private static final int DATABASE_VERSION = 1;
    private final Context context;

    private SQLiteDatabase sqLiteDatabase;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public void clearDB() {
        sqLiteDatabase = getWritableDatabase();
        if (isTableExist(ITEMS)) {
            sqLiteDatabase.delete(ITEMS, null, null);
        }
        if (isTableExist(CONTENTS)) {
            sqLiteDatabase.delete(CONTENTS, null, null);
        }
        if (isTableExist(INSTITUTIONS)) {
            sqLiteDatabase.delete(INSTITUTIONS, null, null);
        }
        if (isTableExist(REFERENCE)) {
            sqLiteDatabase.delete(REFERENCE, null, null);
        }
        if (isTableExist(TYPES)) {
            sqLiteDatabase.delete(TYPES, null, null);
        }
    }

    public List<Map<String, Object>> getTypesItems() {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(TYPES, null, null, null, null, null, null);
        return createTypesResult(cursor);
    }

    public List<Map<String, Object>> getParentItems(String type) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(ITEMS, null, "parent = ? and type = ?", new String[]{"0", type}, null, null, null);
        return createItemResult(cursor, type);
    }

    public List<Map<String, Object>> getChildsItems(int parentId) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(ITEMS, null, "parent = ?", new String[]{String.valueOf(parentId)}, null, null, null);
        return createItemResult(cursor, "");
    }

    public Map<String, Object> getChildsItem(String id) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(ITEMS, null, "_id = ?", new String[]{id}, null, null, null);
        List<Map<String, Object>> list = createItemResult(cursor, "");
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public List<Map<String, Object>> getInfoToName(String text, String type) {
        String[] words = text.trim().split(" ");
        String where1 = "";
        for (int i = 0; i < words.length; i++) {
            where1 += "name LIKE ? and ";
            words[i] = "%" + words[i] + "%";
        }
        where1 += " content = 1";

        if (!type.equals("")) {
            where1 += " and type = " + type;
        }

        Cursor cursor;
        cursor = sqLiteDatabase.query(ITEMS, null, where1, words, null, null, null);
        sqLiteDatabase = getWritableDatabase();
        return createItemResult(cursor, "");
    }

    public List<Map<String, Object>> getContent(String parentId) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(CONTENTS, null, "item = ?", new String[]{parentId}, null, null, "sortby");
        return createContentResult(cursor);
    }

    public List<Map<String, Object>> getReference() {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(REFERENCE, null, null, null, null, null, null);
        return createReferenceResult(cursor);
    }

    public List<Map<String, Object>> getInstitutions(String parentId) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = createCursorNew(parentId);
        ReferenceComparator referenceComparator = new ReferenceComparator();
        List<Map<String, Object>> result = createInstitutionsResult(cursor);
        Collections.sort(result, referenceComparator);

        if (result.size() > 100) {
            result = result.subList(0, 100);
        }
        return result;
    }

    private Cursor createCursorNew(String parentId) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(INSTITUTIONS);
        qb.appendWhere("reference = ?");
        Cursor cursor = qb.query(sqLiteDatabase, null, "(" + Constants.LATITUDE + " BETWEEN ? AND ?) and " +
                        "(" + Constants.LONGITUDE + " BETWEEN ? AND ?)",
                new String[]{parentId, String.valueOf(Constants.latitude - 1), String.valueOf(Constants.latitude + 1)
                        , String.valueOf(Constants.longitude - 1), String.valueOf(Constants.longitude + 1)}, null, null, null);

        return cursor;
    }

    public Map<String, Object> getInstitution(String id) {
        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(INSTITUTIONS, null, "_id = ?", new String[]{id}, null, null, null);
        List<Map<String, Object>> result = createInstitutionsResult(cursor);
        return result.get(0);
    }

    public List<Map<String, Object>> getInstitutionsToName(String name) {
        String[] words = name.trim().split(" ");
        String where1 = "";
        for (int i = 0; i < words.length; i++) {
            if (i != 0) {
                where1 += " and ";
            }
            where1 += "name LIKE ?";
            words[i] = "%" + words[i] + "%";
        }

        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(INSTITUTIONS, null, where1, words, null, null, null);
        ReferenceComparator referenceComparator = new ReferenceComparator();
        List<Map<String, Object>> result = createInstitutionsResult(cursor);
        Collections.sort(result, referenceComparator);

        if (result.size() > 100) {
            result = result.subList(0, 100);
        }
        return result;
    }

    public List<Map<String, Object>> getInstitutionsToNameAndType(String name, String type) {
        String[] words = name.trim().split(" ");
        String where1 = "";
        for (int i = 0; i < words.length; i++) {
            where1 += "name LIKE ? and ";
            words[i] = "%" + words[i] + "%";
        }

        where1 += "reference = " + type;

        sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query(INSTITUTIONS, null, where1, words, null, null, null);
        ReferenceComparator referenceComparator = new ReferenceComparator();
        List<Map<String, Object>> result = createInstitutionsResult(cursor);
        Collections.sort(result, referenceComparator);

        if (result.size() > 100) {
            result = result.subList(0, 100);
        }
        return result;
    }

    private List<Map<String, Object>> createTypesResult(Cursor cursor) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(Constants.ID);
            int name = cursor.getColumnIndex(Constants.NAME);
            int description = cursor.getColumnIndex(Constants.DESCRIPTION);
            int icon = cursor.getColumnIndex(Constants.ICON);
            do {
                Map<String, Object> item = new HashMap<>();
                item.put(Constants.ID, cursor.getInt(id));
                item.put(Constants.NAME, cursor.getString(name));
                item.put(Constants.DESCRIPTION, cursor.getString(description));
                item.put(Constants.ICON, cursor.getString(icon));
                item.put(Constants.TABLE, TYPES);
                result.add(item);
            } while (cursor.moveToNext());
        } else
            Log.d("cursor", "0 rows");
        cursor.close();
        return result;
    }

    private List<Map<String, Object>> createReferenceResult(Cursor cursor) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(Constants.ID);
            int name = cursor.getColumnIndex(Constants.NAME);
            int description = cursor.getColumnIndex(Constants.DESCRIPTION);
            int icon = cursor.getColumnIndex(Constants.ICON);
            int color = cursor.getColumnIndex(Constants.COLOR);

            do {
                Map<String, Object> item = new HashMap<>();
                item.put(Constants.ID, cursor.getInt(id));
                item.put(Constants.NAME, cursor.getString(name));
                item.put(Constants.DESCRIPTION, cursor.getString(description));
                item.put(Constants.ICON, cursor.getString(icon));
                item.put(Constants.COLOR, cursor.getString(color));
                item.put(Constants.TABLE, REFERENCE);
                result.add(item);
            } while (cursor.moveToNext());
        } else
            Log.d("cursor", "0 rows");
        cursor.close();
        return result;
    }

    private List<Map<String, Object>> createInstitutionsResult(Cursor cursor) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(Constants.ID);
            int reference = cursor.getColumnIndex(Constants.REFERENCE);
            int name = cursor.getColumnIndex(Constants.NAME);
            int description = cursor.getColumnIndex(Constants.DESCRIPTION);
            int latitude = cursor.getColumnIndex(Constants.LATITUDE);
            int longitude = cursor.getColumnIndex(Constants.LONGITUDE);
            int address = cursor.getColumnIndex(Constants.ADDRESS);
            int phone = cursor.getColumnIndex(Constants.PHONE);
            int fax = cursor.getColumnIndex(Constants.FAX);
            int icon = cursor.getColumnIndex(Constants.ICON);
            int showIcon = cursor.getColumnIndex(Constants.SHOW_ICON);

            do {
                Map<String, Object> item = new HashMap<>();
                item.put(Constants.ID, cursor.getInt(id));
                item.put(Constants.REFERENCE, cursor.getInt(reference));
                item.put(Constants.NAME, cursor.getString(name));
                item.put(Constants.DESCRIPTION, cursor.getString(description));
                item.put(Constants.LATITUDE, cursor.getString(latitude));
                item.put(Constants.LONGITUDE, cursor.getString(longitude));
                item.put(Constants.ADDRESS, cursor.getString(address));
                item.put(Constants.PHONE, cursor.getString(phone));
                item.put(Constants.FAX, cursor.getString(fax));
                item.put(Constants.ICON, cursor.getString(icon));
                item.put(Constants.SHOW_ICON, cursor.getInt(showIcon));
                item.put(Constants.TABLE, INSTITUTIONS);
                result.add(item);
            } while (cursor.moveToNext());
        } else
            Log.d("cursor", "0 rows");
        cursor.close();
        return result;
    }

    private List<Map<String, Object>> createItemResult(Cursor cursor, String typeTable) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(Constants.ID);
            int name = cursor.getColumnIndex(Constants.NAME);
            int type = cursor.getColumnIndex(Constants.TYPE);
            int parent = cursor.getColumnIndex(Constants.PARENT);
            int showIcon = cursor.getColumnIndex(Constants.SHOW_ICON);
            int icon = cursor.getColumnIndex(Constants.ICON);
            int content = cursor.getColumnIndex(Constants.CONTENT);

            do {
                Map<String, Object> item = new HashMap<>();
                item.put(Constants.ID, cursor.getInt(id));
                item.put(Constants.NAME, cursor.getString(name));
                item.put(Constants.ICON, cursor.getString(icon));
                item.put(Constants.TYPE, cursor.getInt(type));
                item.put(Constants.PARENT, cursor.getInt(parent));
                item.put(Constants.SHOW_ICON, cursor.getInt(showIcon));
                item.put(Constants.CONTENT, cursor.getInt(content));
                item.put(Constants.TABLE, ITEMS);
                item.put(Constants.PARENT_TABLE, typeTable);
                result.add(item);
            } while (cursor.moveToNext());
        } else
            Log.d("cursor", "0 rows");
        cursor.close();
        return result;
    }

    private List<Map<String, Object>> createContentResult(Cursor cursor) {
        List<Map<String, Object>> result = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int id = cursor.getColumnIndex(Constants.ID);
            int item = cursor.getColumnIndex(Constants.ITEM);
            int order = cursor.getColumnIndex(Constants.SORT);
            int title = cursor.getColumnIndex(Constants.TITLE);
            int text = cursor.getColumnIndex(Constants.TEXT);
            int image = cursor.getColumnIndex(Constants.IMAGE);
            int largeImage = cursor.getColumnIndex(Constants.LARGE_IMAGE);
            int alt = cursor.getColumnIndex(Constants.ALT);
            int pattern = cursor.getColumnIndex(Constants.PATTERN);

            do {
                Map<String, Object> content = new HashMap<>();
                content.put(Constants.ID, cursor.getInt(id));
                content.put(Constants.ITEM, cursor.getInt(item));
                content.put(Constants.SORT, cursor.getInt(order));
                content.put(Constants.TITLE, cursor.getString(title));
                content.put(Constants.TEXT, cursor.getString(text));
                content.put(Constants.LARGE_IMAGE, cursor.getString(largeImage));
                content.put(Constants.IMAGE, cursor.getString(image));
                content.put(Constants.ALT, cursor.getString(alt));
                content.put(Constants.PATTERN, cursor.getInt(pattern));
                content.put(Constants.TABLE, CONTENTS);
                result.add(content);
            } while (cursor.moveToNext());
        } else
            Log.d("cursor", "0 rows");
        cursor.close();
        return result;
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {
            //ничего не делать - база уже есть
        } else {
            //вызывая этот метод создаем пустую базу, позже она будет перезаписана
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DATABASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            //база еще не существует
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException {
        //Открываем локальную БД как входящий поток
        InputStream myInput = context.getAssets().open(DATABASE_NAME);

        //Путь ко вновь созданной БД
        String outFileName = DB_PATH + DATABASE_NAME;

        //Открываем пустую базу данных как исходящий поток
        OutputStream myOutput = new FileOutputStream(outFileName);

        //перемещаем байты из входящего файла в исходящий
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //закрываем потоки
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String myPath = DB_PATH + DATABASE_NAME;
        sqLiteDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (sqLiteDatabase != null)
            sqLiteDatabase.close();
        super.close();
    }

    public void putReference(List<Map<String, Object>> references) {
        sqLiteDatabase.beginTransaction();
        ContentValues cv = new ContentValues();
        for (Map<String, Object> map : references) {
            cv.put(Constants.ID, Integer.parseInt(map.get("id").toString()));
            cv.put(Constants.NAME, map.get(Constants.TITLE).toString());
            if (map.get(Constants.COLOR) != null) {
                cv.put(Constants.COLOR, map.get(Constants.COLOR).toString());
            }

            if (map.get(Constants.ICON) != null) {
                cv.put(Constants.ICON, map.get(Constants.ICON).toString());
            }
            if (map.get(Constants.DESCRIPTION) != null) {
                cv.put(Constants.DESCRIPTION, map.get(Constants.DESCRIPTION).toString());
            }
            sqLiteDatabase.insert(REFERENCE, null, cv);
            cv.clear();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void putInstitutions(List<Map<String, Object>> references) {
        sqLiteDatabase.beginTransaction();
        ContentValues cv = new ContentValues();
        for (Map<String, Object> map : references) {
            if (map.get(Constants.DELETED).toString().equals("1")) {
                sqLiteDatabase.delete(INSTITUTIONS, "_id = ?", new String[]{map.get("id").toString()});
                continue;
            }
            cv.put(Constants.ID, Integer.parseInt(map.get("id").toString()));
            cv.put(Constants.REFERENCE, Integer.parseInt(map.get("type_id").toString()));
            cv.put(Constants.NAME, map.get(Constants.TITLE).toString());
            if (map.get(Constants.COLOR) != null) {
                cv.put(Constants.COLOR, map.get(Constants.COLOR).toString());
            }

            if (map.get(Constants.LATITUDE) != null) {
                cv.put(Constants.LATITUDE, map.get(Constants.LATITUDE).toString());
            }

            if (map.get(Constants.LONGITUDE) != null) {
                cv.put(Constants.LONGITUDE, map.get(Constants.LONGITUDE).toString());
            }

            if (map.get(Constants.FAX) != null) {
                cv.put(Constants.FAX, map.get(Constants.FAX).toString());
            }

            if (map.get(Constants.PHONE) != null) {
                cv.put(Constants.PHONE, map.get(Constants.PHONE).toString());
            }

            if (map.get(Constants.FAX) != null) {
                cv.put(Constants.FAX, map.get(Constants.FAX).toString());
            }

            if (map.get(Constants.EMAIL) != null) {
                cv.put(Constants.EMAIL, map.get(Constants.EMAIL).toString());
            }

            if (map.get(Constants.ADDRESS) != null) {
                cv.put(Constants.ADDRESS, map.get(Constants.ADDRESS).toString());
            }

            if (map.get(Constants.ICON) != null) {
                cv.put(Constants.ICON, map.get(Constants.ICON).toString());
            }

            if (map.get(Constants.DESCRIPTION) != null) {
                cv.put(Constants.DESCRIPTION, map.get(Constants.DESCRIPTION).toString());
            }

            if (map.get("show_this_icon") != null) {
                cv.put(Constants.SHOW_ICON, Integer.parseInt(map.get("show_this_icon").toString()));
            }

            sqLiteDatabase.insert(INSTITUTIONS, null, cv);
            cv.clear();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
        references.clear();

    }

    public void putHelpTypes(List<Map<String, Object>> references) {
        sqLiteDatabase.beginTransaction();
        ContentValues cv = new ContentValues();
        for (Map<String, Object> map : references) {
            if (map.get(Constants.DELETED).toString().equals("1")) {
                sqLiteDatabase.delete(TYPES, "_id = ?", new String[]{map.get("id").toString()});
                continue;
            }
            cv.put(Constants.ID, Integer.parseInt(map.get("id").toString()));
            cv.put(Constants.NAME, map.get(Constants.NAME).toString());
            if (map.get(Constants.COLOR) != null) {
                cv.put(Constants.COLOR, map.get("second_color").toString());
            }

            if (map.get(Constants.ICON) != null) {
                cv.put(Constants.ICON, map.get(Constants.ICON).toString());
            }
            if (map.get(Constants.DESCRIPTION) != null) {
                cv.put(Constants.DESCRIPTION, map.get(Constants.DESCRIPTION).toString());
            }
            sqLiteDatabase.insert(TYPES, null, cv);
            cv.clear();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void putHelpItems(List<Map<String, Object>> references) {
        sqLiteDatabase.beginTransaction();
        ContentValues cv = new ContentValues();
        for (Map<String, Object> map : references) {
            if (map.get(Constants.DELETED).toString().equals("1")) {
                sqLiteDatabase.delete(ITEMS, "_id = ?", new String[]{map.get("id").toString()});
                continue;
            }
            cv.put(Constants.ID, Integer.parseInt(map.get("id").toString()));
            cv.put(Constants.NAME, map.get(Constants.NAME).toString());
            cv.put(Constants.PARENT, Integer.parseInt(map.get("parent_id").toString()));
            cv.put(Constants.CONTENT, Integer.parseInt(map.get(Constants.CONTENT).toString()));
            cv.put(Constants.TYPE, Integer.parseInt(map.get("help_type_id").toString()));
            cv.put(Constants.SHOW_ICON, Integer.parseInt(map.get("show_this_icon").toString()));
            if (map.get(Constants.ICON) != null) {
                cv.put(Constants.ICON, map.get(Constants.ICON).toString());
            }
            if (map.get(Constants.DESCRIPTION) != null) {
                cv.put(Constants.DESCRIPTION, map.get(Constants.DESCRIPTION).toString());
            }
            sqLiteDatabase.insert(ITEMS, null, cv);
            cv.clear();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    public void putHelpContent(List<Map<String, Object>> references) {
        sqLiteDatabase.beginTransaction();
        ContentValues cv = new ContentValues();
        for (Map<String, Object> map : references) {
            if (map.get(Constants.DELETED).toString().equals("1")) {
                sqLiteDatabase.delete(CONTENTS, "_id = ?", new String[]{map.get("id").toString()});
                continue;
            }
            cv.put(Constants.ID, Integer.parseInt(map.get("id").toString()));
            cv.put(Constants.ITEM, Integer.parseInt(map.get("help_menu_id").toString()));
            cv.put(Constants.PATTERN, Integer.parseInt(map.get(Constants.PATTERN).toString()));
            cv.put(Constants.TITLE, map.get(Constants.TITLE).toString());
            cv.put(Constants.TEXT, map.get(Constants.TEXT).toString());

            if (map.get(Constants.ALT) != null) {
                cv.put(Constants.ALT, map.get(Constants.ALT).toString());
            }

            if (map.get(Constants.ORDER) != null) {
                cv.put(Constants.SORT, Integer.parseInt(map.get(Constants.ORDER).toString()));
            }

            if (map.get(Constants.IMAGE) != null) {
                cv.put(Constants.IMAGE, map.get(Constants.IMAGE).toString());
            }
            if (map.get(Constants.LARGE_IMAGE) != null) {
                cv.put(Constants.LARGE_IMAGE, map.get("large_image").toString());
            }
            if (map.get(Constants.DESCRIPTION) != null) {
                cv.put(Constants.DESCRIPTION, map.get(Constants.DESCRIPTION).toString());
            }
            sqLiteDatabase.insert(CONTENTS, null, cv);
            cv.clear();
        }
        sqLiteDatabase.setTransactionSuccessful();
        sqLiteDatabase.endTransaction();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public String getParentIcon(String id) {
        sqLiteDatabase = getWritableDatabase();
        String icon = null;
        Cursor cursor = sqLiteDatabase.query(TYPES, null, "_id = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            icon = cursor.getString(cursor.getColumnIndex(Constants.ICON));
        }
        cursor.close();
        return icon;
    }

    public String getReferenceIcon(String id) {
        sqLiteDatabase = getWritableDatabase();
        String icon = null;
        Cursor cursor = sqLiteDatabase.query(REFERENCE, null, "_id = ?", new String[]{id}, null, null, null);
        if (cursor.moveToFirst()) {
            icon = cursor.getString(cursor.getColumnIndex(Constants.ICON));
        }
        cursor.close();
        return icon;
    }

    private boolean isTableExist(String tableName) {
        Cursor cursor = sqLiteDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }
}