package mcs.nsign.by.mcs.service.http;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

/**
 * Created by Bakht on 11.09.2016.
 */
public interface HttpApi {

    @DELETE()
    Call<Object> deleteContact(
            @Url String url
    );

    @GET("users/{id}")
    Call<Object> getContacts(@Path("id") String id, @Query("expand") String expand, @Query("access-token") String accesstoken);

    @GET()
    Call<Object> getSos(@Url String url);

    @GET("sms-statuses/{id}")
    Call<Object> getSmsStatus(@Path("id") String id, @Query("access-token") String accesstoken);

    @FormUrlEncoded
    @POST("user-contacts")
    Call<Object> postContact(
            @Field("user_id") String user_id,
            @Field("name") String name,
            @Field("last_name") String surname,
            @Field("phone") String phone,
            @Field("access-token") String accesstoken
    );

    @FormUrlEncoded
    @POST("user-registers")
    Call<Object> postPhone(@Field("phone") String phone,
                           @Field("imei") String imei,
                           @Field("first_name") String name,
                           @Field("last_name") String surname,
                           @Field("os") String os,
                           @Field("push_token") String pushToken);

    @FormUrlEncoded
    @PUT()
    Call<Object> saveProfile(@Url String url,
                             @Field("first_name") String firstname,
                             @Field("last_name") String lastName,
                             @Field("email") String email,
                             @Field("blood") String blood,
                             @Field("birthday") String age,
                             @Field("chronic_disease") String illness);

    @GET("users/view-profile/{id}")
    Call<Object> getProfile(@Path("id") String id, @Query("access-token") String accesstoken);

    @FormUrlEncoded
    @PUT()
    Call<Object> putChangeContact(
            @Url String url,
            @Field("name") String name,
            @Field("last_name") String surname,
            @Field("phone") String phone
    );

    @FormUrlEncoded
    @PUT("user-registers/{id}")
    Call<Object> putPass(@Path("id") String id, @Field("verify_code") String pass);

    @GET("user-registers/{id}")
    Call<Object> repeatPostPhone(@Path("id") String id, @Query("phone") String phone);

    @FormUrlEncoded
    @PUT("feedback/send/{id}")
    Call<Object> sendMessage(@Path("id") String id, @Field("message") String message);

    @GET("guide/default/detail-update")
    Call<Object> checkUpdateDB(@Query("access-token") String accessToken, @QueryMap Map<String, String> dataUpdates);

    @FormUrlEncoded
    @POST("user-routes")
    Call<Object> sendMyCurrentLocation(@Field("access-token") String tokent, @Field("sos_id") String sosId,
                                       @Field("latitude") String latitude, @Field("longitude") String longitude);
}
