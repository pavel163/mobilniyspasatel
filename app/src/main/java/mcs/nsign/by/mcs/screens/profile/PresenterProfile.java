package mcs.nsign.by.mcs.screens.profile;

import android.content.Context;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.model.Profile;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by mac1 on 18.08.16.
 */
public class PresenterProfile implements IPresenterProfile, ServerObserver<Object, String> {

    public IViewProfile view;
    public IModelProfile model;

    public PresenterProfile(IViewProfile view) {
        this.view = view;
        this.model = new ModelProfile();
        this.model.setServerObserver(this);
    }

    @Override
    public void changeProfile(Context context, String accesstoken, String id, Profile profile) {
        view.showLoadingProcess();
        if (view.checkAccessNetwork()) {
            model.changeProfile(accesstoken, id, profile);
        } else {
            failedExecute(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    @Override
    public void getProfile(Context context, String accesstoken, String id) {
        view.showLoadingProcess();
        if (view.checkAccessNetwork()) {
            model.getProfile(accesstoken, id);
        } else {
            failedExecute(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    @Override
    public void successExecuteObject(Object obj) {
        view.hideLoadingProcess();
        view.successSaveProfile(obj);
    }

    @Override
    public void successExecuteObject2(Object obj) {
        view.hideLoadingProcess();
        view.successGetProfile(obj);
    }

    @Override
    public void failedExecute(String err) {
        view.hideLoadingProcess();
        view.showError(err);
    }
}
