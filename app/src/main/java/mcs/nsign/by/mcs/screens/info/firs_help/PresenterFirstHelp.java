package mcs.nsign.by.mcs.screens.info.firs_help;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 10.09.2016.
 */
public class PresenterFirstHelp implements IPresenterFirstHelp {

    private IViewFirstHelp view;
    private IModelFirstHelp modelFirstHelp;

    public PresenterFirstHelp(IViewFirstHelp view, IModelFirstHelp modelFirstHelp) {
        this.view = view;
        this.modelFirstHelp = modelFirstHelp;
    }

    @Override
    public void initExpandList() {
        List<Map<String, Object>> items = modelFirstHelp.getExpandList();
        if (items.size() != 0) {
            view.initExpandList(items);
        } else {
            view.showEmptyMessage("Нет данных по данному разделу");
        }
    }

    @Override
    public void unSubscribe() {

    }

    @Override
    public void initSearchList() {
        view.initSearchList(modelFirstHelp.getSearchList());
    }

    @Override
    public void initMenu() {

    }

    @Override
    public void search(String text, String type) {
        if (text.length() >= 3) {
            modelFirstHelp.loadNewSearchList(text, type);
            view.setVisibleSearchList();
        } else {
            view.setVisibleMenu();
        }
        view.notifySearchList();
    }
}
