package mcs.nsign.by.mcs.screens.info.review;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FInfoSearchListBinding;
import mcs.nsign.by.mcs.screens.info.adapter.ReviewAdapter;

/**
 * Created by mac1 on 09.09.16.
 */
public class ReviewFragment extends BaseBindingFragment<FInfoSearchListBinding> implements IViewReview{

    protected IModelReview modelReview;
    protected IPresenterReview presenterReview;

    @Override
    protected void afterBinding() {
        super.afterBinding();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getActivity().getIntent().getStringExtra(Constants.NAME));
        fragmentBinder.recycler.setBackgroundColor(Color.WHITE);

        modelReview = new ModelReview();
        presenterReview = new PresenterReview(modelReview, this);
        presenterReview.initList(getActivity().getIntent().getStringExtra(Constants.ID));
    }

    @Override
    public int getLayout() {
        return R.layout.f_info_search_list;
    }

    @Override
    public void initList(List<Map<String, Object>> list) {
        ReviewAdapter adapter = new ReviewAdapter(getActivity(), list);
        fragmentBinder.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentBinder.recycler.setAdapter(adapter);
    }
}
