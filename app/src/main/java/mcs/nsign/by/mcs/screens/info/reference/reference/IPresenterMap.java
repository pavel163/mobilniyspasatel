package mcs.nsign.by.mcs.screens.info.reference.reference;

/**
 * Created by Bakht on 18.09.2016.
 */
public interface IPresenterMap {
    void drawMarkers(String parentId);

    void unSubscribe();
}
