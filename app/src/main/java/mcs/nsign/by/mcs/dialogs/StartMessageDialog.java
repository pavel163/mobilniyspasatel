package mcs.nsign.by.mcs.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.service.SettingsService;

/**
 * Created by Ergashev on 30.10.2016.
 */

public class StartMessageDialog extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Уважаемый пользователь!");
        alertDialogBuilder.setMessage(getResources().getString(R.string.start_message));
        alertDialogBuilder.setPositiveButton("Принимаю", (dialog, which) ->
                SettingsService.set(getContext(), "start_message", "1"));
        return alertDialogBuilder.create();
    }
}
