package mcs.nsign.by.mcs.screens.info.reference;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 14.09.2016.
 */
public class ModelReference implements IModelReference {

    protected List<Map<String, Object>> listSearch;
    private List<Map<String, Object>> referenceList;

    public ModelReference() {
        listSearch = new ArrayList<>();
        referenceList = MCSApp.dbHelper.getReference();
    }

    @Override
    public List<Map<String, Object>> getReferenceList() {
        return referenceList;
    }

    @Override
    public List<Map<String, Object>> getSearchList() {
        return listSearch;
    }

    @Override
    public List<Map<String, Object>> getExpandList() {
        return null;
    }

    @Override
    public Map<String, Object> getSearchItem(int position) {
        return null;
    }

    @Override
    public Map<String, Object> getItem(int position) {
        return null;
    }

    @Override
    public void loadNewSearchList(String text, String type) {
        List<Map<String, Object>> result = MCSApp.dbHelper.getInstitutionsToName(text);

        for (Map<String, Object> map : result) {
            map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getReferenceIcon(map.get(Constants.REFERENCE).toString()));
        }

        listSearch.clear();
        listSearch.addAll(result);
    }
}
