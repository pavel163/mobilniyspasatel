package mcs.nsign.by.mcs.screens.info.review;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;

/**
 * Created by Bakht on 11.09.2016.
 */
public class ModelReview implements IModelReview {

    protected List<Map<String, Object>> list;

    public ModelReview(){
        list = new ArrayList<>();
    }

    @Override
    public List<Map<String, Object>> getInfo(String parentId) {
        list.clear();
        list.addAll(MCSApp.dbHelper.getContent(parentId));
        return list;
    }
}
