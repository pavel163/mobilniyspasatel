package mcs.nsign.by.mcs.screens.profile;

import mcs.nsign.by.mcs.utils.NetworkCallback;
import mcs.nsign.by.mcs.utils.OnProgressLoadListener;

/**
 * Created by mac1 on 18.08.16.
 */
public interface IViewProfile extends NetworkCallback, OnProgressLoadListener {

    void showError(String error);

    void successSaveProfile(Object obj);

    void successGetProfile(Object profile);
}
