package mcs.nsign.by.mcs.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mac1 on 07.10.16.
 */

public class MyISReader extends InputStreamReader {

    private int readBytes = 0;

    public MyISReader(InputStream in) {
        super(in);
    }

    @Override
    public int read(char[] cbuf, int offset, int length) throws IOException {
        readBytes += length;
        return super.read(cbuf, offset, length);
    }

    public int getReadBytes() {
        return readBytes;
    }

    public void flushReadBytes() {
        readBytes = 0;
    }
}
