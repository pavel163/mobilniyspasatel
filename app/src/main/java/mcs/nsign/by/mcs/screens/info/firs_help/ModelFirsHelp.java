package mcs.nsign.by.mcs.screens.info.firs_help;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 10.09.2016.
 */
public class ModelFirsHelp implements IModelFirstHelp {

    protected List<Map<String, Object>> listSearch;
    private List<Map<String, Object>> expandList;

    public ModelFirsHelp(String id, String type) {
        listSearch = new ArrayList<>();
        if (id == null || id.equals("")) {
            expandList = MCSApp.dbHelper.getParentItems(type);
        } else {
            expandList = MCSApp.dbHelper.getChildsItems(Integer.parseInt(id));
        }
    }

    @Override
    public List<Map<String, Object>> getSearchList() {
        return listSearch;
    }

    @Override
    public List<Map<String, Object>> getExpandList() {
        return expandList;
    }

    @Override
    public Map<String, Object> getSearchItem(int position) {
        return listSearch.get(position);
    }

    @Override
    public Map<String, Object> getItem(int position) {
        return expandList.get(position);
    }

    @Override
    public void loadNewSearchList(String text, String type) {
        List<Map<String, Object>> resultInfo = MCSApp.dbHelper.getInfoToName(text, type);

        for (Map<String, Object> map : resultInfo) {
            Object showIcon = map.get(Constants.SHOW_ICON);
            if (showIcon != null && (int) showIcon == 0) {
                map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getParentIcon(map.get(Constants.TYPE).toString()));
            }
        }
        listSearch.clear();
        listSearch.addAll(resultInfo);
    }
}
