package mcs.nsign.by.mcs.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by mac1 on 31.08.16.
 */
public class PhonesDialogFragment extends DialogFragment {

    private OnPhoneListener listener;

    public interface OnPhoneListener {
        void choosePhone(String phone);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnPhoneListener) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String[] phones = getArguments().getStringArray("phones");
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Выберите номер");
        dialog.setSingleChoiceItems(phones, 0, (dialogInterface, i) -> {
            listener.choosePhone(phones[i]);
            dismiss();
        });
        return dialog.create();
    }
}
