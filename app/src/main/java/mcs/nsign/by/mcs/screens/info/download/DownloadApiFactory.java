package mcs.nsign.by.mcs.screens.info.download;

import mcs.nsign.by.mcs.constants.Constants;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by Bakht on 02.10.2016.
 */

public class DownloadApiFactory {

    private DownLoadApi downLoadApi;

    public DownloadApiFactory(DownLoadApi downLoadApi) {
        this.downLoadApi = downLoadApi;
    }

    public Call<ResponseBody> getApiMethod(String name, String token, String dataUpdate, String start) {
        if (name.equals(Constants.SIZE_OBJECT)) {
            return downLoadApi.loadReference(token, dataUpdate, start);
        } else if (name.equals(Constants.SIZE_OBJECT_TYPE)) {
            return downLoadApi.loadReferenceType(token, dataUpdate, start);
        } else if (name.equals(Constants.SIZE_HELP_CONTENT)) {
            return downLoadApi.loadHelpContent(token, dataUpdate, start);
        } else if (name.equals(Constants.SIZE_HELP_MENU)) {
            return downLoadApi.loadHelpItems(token, dataUpdate, start);
        } else if (name.equals(Constants.SIZE_HELP_TYPE)) {
            return downLoadApi.loadHelpType(token, dataUpdate, start);
        } else return null;
    }
}
