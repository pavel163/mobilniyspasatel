package mcs.nsign.by.mcs.screens.signal;

import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by Ruslan on 22.11.2015.
 */

public class ModelSignal implements IModelSignal {

    public RetrofitClient client;

    public ModelSignal() {
        client = new RetrofitClient();
    }

    @Override
    public void getSos(String token, String id, double lan, double lon, String test) {
        client.getSos(token, id, lan, lon, test);
    }

    @Override
    public void sendMyCurrentLocation(String token, String sosId, String latitude, String longitude) {
        client.sendMyCurrentLocation(token, sosId, latitude, longitude);
    }

    public void getSmsStatus(String token, String id){
        client.getSmsStatus(id, token);
    }

    public void setServerObserver(ServerObserver serverObserver) {
        client.setServerObserver(serverObserver);
    }

    @Override
    public void setServerCallback(ServerCallback serverCallback) {
        client.setServerCallback(serverCallback);
    }
}
