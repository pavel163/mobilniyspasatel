package mcs.nsign.by.mcs.screens.login.code;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IPresenterCode {

    void tryToEnter(String code, String id);

    void handleVerifiedResponse(int verified);

    void repeatRequestCode(String id, String phone);

    void startTimer();
}
