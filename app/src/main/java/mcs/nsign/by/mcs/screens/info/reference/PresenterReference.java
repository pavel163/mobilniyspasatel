package mcs.nsign.by.mcs.screens.info.reference;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.screens.info.firs_help.IPresenterFirstHelp;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 14.09.2016.
 */
public class PresenterReference implements IPresenterFirstHelp {

    private IViewReference view;
    private IModelReference modelReference;
    private Subscription referenceSubscription;
    private Subscription searchSubscription;

    public PresenterReference(IViewReference view, IModelReference modelReference) {
        this.view = view;
        this.modelReference = modelReference;
    }

    @Override
    public void initExpandList() {
        Observable<List<Map<String, Object>>> observable = Observable.create((Observable.OnSubscribe<List<Map<String, Object>>>) subscriber -> {
            subscriber.onNext(modelReference.getReferenceList());
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        referenceSubscription = observable.subscribe(o -> view.initReferenceAdapter(o));
    }

    @Override
    public void unSubscribe() {
        if (referenceSubscription != null && !referenceSubscription.isUnsubscribed()) {
            referenceSubscription.unsubscribe();
        }
        if (searchSubscription != null && !searchSubscription.isUnsubscribed()) {
            searchSubscription.unsubscribe();
        }
    }

    @Override
    public void initSearchList() {
        view.initSearchList(modelReference.getSearchList());
    }

    @Override
    public void initMenu() {

    }

    @Override
    public void search(String text, String type) {
        if (text.length() >= 3) {
            Observable<Integer> observable = Observable.create((Observable.OnSubscribe<Integer>) subscriber -> {
                modelReference.loadNewSearchList(text, type);
                subscriber.onNext(1);
            }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

            searchSubscription = observable.subscribe(o -> {
                view.setVisibleSearchList();
                view.notifySearchList();
            });
        } else {
            view.setVisibleMenu();
            view.notifySearchList();
        }
    }
}
