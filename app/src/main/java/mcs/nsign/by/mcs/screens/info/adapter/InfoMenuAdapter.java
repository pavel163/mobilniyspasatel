package mcs.nsign.by.mcs.screens.info.adapter;

import android.content.Context;
import android.util.Base64;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.adapter.holder.SearchViewHolder;
import mcs.nsign.by.mcs.service.databases.DBHelper;

/**
 * Created by mac1 on 04.10.16.
 */

public class InfoMenuAdapter extends SearchListAdapter {

    public InfoMenuAdapter(Context context, List<Map<String, Object>> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        Map<String, Object> map = list.get(position);
        (holder).name.setText(map.get(Constants.NAME).toString());
        if (position == list.size() - 1 && map.get(DBHelper.REFERENCE) != null) {
            Glide.with(holder.imageView.getContext())
                    .load(R.drawable.reference)
                    .into((holder).imageView);
        } else {
            byte[] icon = null;
            Object showIcon = map.get(Constants.SHOW_ICON);
            if (showIcon != null && (int) showIcon == 0 && map.get(Constants.PARENT_ICON) != null) {
                icon = Base64.decode(map.get(Constants.PARENT_ICON).toString(), Base64.DEFAULT);
            } else if (map.get(Constants.ICON) != null) {
                icon = Base64.decode(map.get(Constants.ICON).toString(), Base64.DEFAULT);
            }

            Glide.with((holder).imageView.getContext())
                    .load(icon)
                    .into((holder).imageView);
        }
    }
}
