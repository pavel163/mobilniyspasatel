package mcs.nsign.by.mcs.screens.login.login;

import mcs.nsign.by.mcs.model.RegisterModel;
import mcs.nsign.by.mcs.utils.OnServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IModelLogin extends OnServerCallback {

    void makeRequest(RegisterModel registerModel);

}
