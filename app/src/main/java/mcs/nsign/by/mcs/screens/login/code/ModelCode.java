package mcs.nsign.by.mcs.screens.login.code;

import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by Ruslan on 22.11.2015.
 */

public class ModelCode implements IModelCode {

    public RetrofitClient client;

    public ModelCode() {
        client = new RetrofitClient();
    }

    @Override
    public void makeRequestCode(String pass, String id){
        client.putPass(pass, id);
    }

    @Override
    public void repeatRequestCode(String userId, String phone){
        client.repeatRequestCode(userId, phone);
    }

    @Override
    public void setServerObserver(ServerObserver serverObserver) {
        client.setServerObserver(serverObserver);
    }

    @Override
    public void setServerCallback(ServerCallback serverCallback) {
        client.setServerCallback(serverCallback);

    }
}
