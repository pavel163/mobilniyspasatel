package mcs.nsign.by.mcs.screens.info.reference;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.screens.info.firs_help.IModelFirstHelp;

/**
 * Created by Bakht on 14.09.2016.
 */
public interface IModelReference extends IModelFirstHelp {
    List<Map<String, Object>> getReferenceList();
}
