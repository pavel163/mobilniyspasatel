package mcs.nsign.by.mcs.screens.info.reference;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.ebr163.core.util.ItemClickSupport;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FReferenceBinding;
import mcs.nsign.by.mcs.screens.info.adapter.SearchListAdapter;
import mcs.nsign.by.mcs.screens.info.adapter.holder.SearchViewHolder;
import mcs.nsign.by.mcs.screens.info.firs_help.IPresenterFirstHelp;
import mcs.nsign.by.mcs.screens.info.reference.reference.ReferenceVPActivity;
import mcs.nsign.by.mcs.ui.recyclerview.DividerItemDecoration;
import mcs.nsign.by.mcs.utils.FragmentFactory;

/**
 * Created by mac1 on 13.09.16.
 */
public class ReferenceCategoryFragment extends BaseBindingFragment<FReferenceBinding> implements IViewReference, SearchView.OnQueryTextListener {

    protected SearchListAdapter searchListAdapter;
    protected SearchListAdapter referenceAdapter;

    protected IModelReference modelReference;
    protected IPresenterFirstHelp presenterReference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void afterBinding() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Справочник организаций");

        modelReference = new ModelReference();
        presenterReference = new PresenterReference(this, modelReference);

        presenterReference.initExpandList();
        presenterReference.initSearchList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Ключевое слово");
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= 3 && fragmentBinder.searchList.getVisibility() == View.GONE) {
            fragmentBinder.progress.setVisibility(View.VISIBLE);
            fragmentBinder.referenceList.setVisibility(View.GONE);
        }
        presenterReference.search(newText, "");
        return true;
    }

    @Override
    public void onStop() {
        super.onStop();
        presenterReference.unSubscribe();
    }

    @Override
    public int getLayout() {
        return R.layout.f_reference;
    }

    @Override
    public void setVisibleMenu() {
        fragmentBinder.progress.setVisibility(View.GONE);
        fragmentBinder.searchList.setVisibility(View.GONE);
        fragmentBinder.referenceList.setVisibility(View.VISIBLE);
    }

    @Override
    public void initMenuItems(List<Map<String, Object>> items) {

    }

    @Override
    public void setVisibleSearchList() {
        fragmentBinder.progress.setVisibility(View.GONE);
        fragmentBinder.searchList.setVisibility(View.VISIBLE);
        fragmentBinder.referenceList.setVisibility(View.GONE);
    }

    @Override
    public void notifySearchList() {
        fragmentBinder.searchList.getAdapter().notifyDataSetChanged();
    }

    protected RecyclerView.Adapter<SearchViewHolder> getSearchAdapter(List<Map<String, Object>> data) {
        return new SearchReferenceAdapter(getActivity(), data);
    }

    protected RecyclerView.Adapter<SearchViewHolder> getReferenceAdapter(List<Map<String, Object>> data) {
        return new SearchListAdapter(getActivity(), data);
    }

    @Override
    public boolean checkConnect() {
        return false;
    }

    @Override
    public void showError(String error) {

    }

    @Override
    public void initSearchList(List<Map<String, Object>> data) {
        searchListAdapter = (SearchListAdapter) getSearchAdapter(data);
        fragmentBinder.searchList.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentBinder.searchList.setAdapter(searchListAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.searchList.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.searchList).setOnItemClickListener((recyclerView1, i, view1) -> {
            clickItemSearch(i, data);
        });
    }

    @Override
    public void initReferenceAdapter(List<Map<String, Object>> data) {
        fragmentBinder.progress.setVisibility(View.GONE);
        referenceAdapter = (SearchListAdapter) getReferenceAdapter(data);
        fragmentBinder.referenceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentBinder.referenceList.setAdapter(referenceAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.referenceList.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.referenceList).setOnItemClickListener((recyclerView1, i, view1) -> {
            clickItemReference(i, data);
        });
    }

    protected void clickItemSearch(int i, List<Map<String, Object>> data){
        Intent intent = new Intent(getActivity(), ActivityWithFragment.class);
        Map<String, Object> map = data.get(i);
        intent.putExtra(Constants.ID, map.get(Constants.ID).toString());
        intent.putExtra(Constants.NAME, map.get(Constants.NAME).toString());
        intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REFERENCE_REVIEW);
        startActivity(intent);
    }

    protected void clickItemReference(int i, List<Map<String, Object>> data){
        Intent intent = new Intent(getActivity(), ReferenceVPActivity.class);
        Map<String, Object> map = data.get(i);
        intent.putExtra(Constants.ID, map.get(Constants.ID).toString());
        intent.putExtra(Constants.NAME, map.get(Constants.NAME).toString());
        startActivity(intent);
    }
}
