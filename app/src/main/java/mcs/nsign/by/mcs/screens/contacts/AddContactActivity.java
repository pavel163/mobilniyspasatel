package mcs.nsign.by.mcs.screens.contacts;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.ActivityAddContactBinding;
import mcs.nsign.by.mcs.dialogs.PhonesDialogFragment;
import mcs.nsign.by.mcs.model.ContactsObject;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.ui.CustomEditText;
import mcs.nsign.by.mcs.utils.ContactUtil;
import mcs.nsign.by.mcs.utils.ServerObserverFour;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.ViewUtils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class AddContactActivity extends AppCompatActivity implements IViewContacts, ServerObserverFour<Object, String>, PhonesDialogFragment.OnPhoneListener {

    private ActivityAddContactBinding binding;
    private PresenterContacts presenter;
    private ModelContacts model;
    private ContactsObject contactsObject;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contactsObject = (ContactsObject) getIntent().getSerializableExtra("contact");
        initUI();

        RetrofitClient client = new RetrofitClient();
        client.setServerObserverFour(this);
        model = new ModelContacts(client);
        presenter = new PresenterContacts(this, this, model);
    }

    private void initUI() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_contact);
        binding.etName.getEditText().addTextChangedListener(getTextWatcher());
        AppUtils.getPhoneMask().installOn(binding.etPhone.getEditText());
        binding.etPhone.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkFields();
                if (binding.etPhone.getEditText().getText().toString().length() != 16) {
                    binding.etPhone.setError("Номер короткий");
                    binding.etPhone.setErrorEnabled(true);
                } else {
                    binding.etPhone.setErrorEnabled(false);
                }
            }
        });

        if (contactsObject == null) {
            binding.appBar.toolbar.setTitle("Добавить контакт");
        } else {
            binding.appBar.toolbar.setTitle("Редактировать контакт");
            binding.etName.getEditText().setText(contactsObject.getName());
            binding.etPhone.getEditText().setText(contactsObject.getPhone().substring(1));
            binding.etSurname.getEditText().setText(contactsObject.getLastName());
        }

        setSupportActionBar(binding.appBar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((CustomEditText) binding.etPhone.getEditText()).setDrawableClickListener(target -> AddContactActivityPermissionsDispatcher.openContactsWithCheck(AddContactActivity.this));

        binding.btnSave.setOnClickListener(view -> saveContact());

        progress = new ProgressDialog(this);
        progress.setMessage("Сохранение...");
    }

    private TextWatcher getTextWatcher(){
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkFields();
                if (binding.etName.getEditText().getText().toString().length() == 0){
                    binding.etName.setError("Пустое поле");
                    binding.etName.setErrorEnabled(true);
                } else {
                    binding.etName.setErrorEnabled(false);
                }
            }
        };
    }

    private void checkFields(){
        if (binding.etName.getEditText().getText().toString().length() != 0 && binding.etPhone.getEditText().getText().toString().length() == 16) {
            ViewUtils.setClickableStateButton(binding.btnSave);
        } else {
            ViewUtils.setUnClickableStateButton(binding.btnSave);
        }
    }
    private void saveContact() {
        progress.show();
        String id = ((MCSApp) getApplicationContext()).getValue("id");
        String accesstoken = ((MCSApp) getApplicationContext()).getValue("access_token");
        if (!checkSameSelf()) {
            progress.hide();
            Toast.makeText(AddContactActivity.this, "Нельзя добавить себя в список контактов", Toast.LENGTH_SHORT).show();
            return;
        }
        if (contactsObject == null) {
            presenter.addContact(id, binding.etName.getEditText().getText().toString(),  binding.etSurname.getEditText().getText().toString(),
                    binding.etPhone.getEditText().getText().toString(), accesstoken, (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE),
                    getIntent().getIntExtra("countContacts", 0));
        } else {
            String id_int = String.valueOf((int) Double.parseDouble(contactsObject.getId()));
            presenter.changeContact(id_int, binding.etName.getEditText().getText().toString(), binding.etSurname.getEditText().getText().toString(), binding.etPhone.getEditText().getText().toString(), accesstoken, (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        }
    }

    private boolean checkSameSelf() {
        String phone;
        phone = binding.etPhone.getEditText().getText().toString();
        phone = getStringClear(phone);

        if (SettingsService.get(this, Constants.PHONE).contains(phone)) {
            return false;
        } else {
            return true;
        }
    }

    private String getStringClear(String phone) {
        if (phone.startsWith("+7")) {
            phone = phone.replaceFirst("\\+7", "");
        } else if (phone.startsWith("7")) {
            phone = phone.replaceFirst("7", "");
        } else if (phone.startsWith("8")) {
            phone = phone.replaceFirst("8", "");
        }
        return phone;
    }

    @NeedsPermission(Manifest.permission.READ_CONTACTS)
    protected void openContacts() {
        startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), 111);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AddContactActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
            Uri uriContact = data.getData();
            Map<String, String> contact = new HashMap<>();
            String[] c = ContactUtil.getContactName(this, uriContact).split(" ");
            contact.put(Constants.NAME, c[0]);
            if (c.length == 2) {
                contact.put(Constants.SURNAME, c[1]);
            } else if (c.length == 3) {
                contact.put(Constants.SURNAME, c[2]);
            }

            List<String> phones = ContactUtil.getContactNumber(this, uriContact);
            if (phones.size() == 1){
                binding.etPhone.getEditText().setText(getStringClear(phones.get(0)));
            } else {
                DialogFragment dialogFragment = new PhonesDialogFragment();
                Bundle args = new Bundle();
                args.putStringArray("phones", phones.toArray(new String[phones.size()]));
                dialogFragment.setArguments(args);
                dialogFragment.show(getSupportFragmentManager(), "dialog");
            }
            binding.etName.getEditText().setText(contact.get(Constants.NAME));
            binding.etSurname.getEditText().setText(contact.get(Constants.SURNAME));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showToast(String error) {
        progress.hide();
    }

    @Override
    public void showErrorConnection(String error) {
        progress.hide();
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDialog() {

    }

    @Override
    public void successExecuteObject(Object obj) {
        progress.hide();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void successExecuteObject_Two(Object obj) {

    }

    @Override
    public void successExecuteObject_Three(Object obj) {
        progress.hide();
        Toast.makeText(AddContactActivity.this, "Контакт обновлен", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void successExecuteObject_Four(Object obj) {

    }

    @Override
    public void failedExecute(String err) {
    }

    @Override
    public void failedExecute_Two(String err) {

    }

    @Override
    public void failedExecute_Three(String err) {

    }

    @Override
    public void failedExecute_Four(String err) {

    }

    @Override
    public void choosePhone(String phone) {
        binding.etPhone.getEditText().setText(getStringClear(phone));
    }
}
