package mcs.nsign.by.mcs.screens.info.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.adapter.holder.BaseViewHolder;
import mcs.nsign.by.mcs.screens.info.adapter.holder.DopViewHolder;
import mcs.nsign.by.mcs.screens.info.adapter.holder.OtherViewHolder;
import mcs.nsign.by.mcs.screens.info.adapter.holder.TextAndImageHolder;
import mcs.nsign.by.mcs.screens.info.adapter.holder.TextViewHolder;

/**
 * Created by Bakht on 11.09.2016.
 */
public class ReviewAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final static int TEXT_AND_IMAGE = 4;
    private final static int TITLE = 1;
    private final static int TEXT = 2;
    private final static int IMAGE = 3;
    private final static int DOP = 5;
    private final static int OTHER = 6;

    private List<Map<String, Object>> list;
    private LayoutInflater mInflator;

    public ReviewAdapter(Context context, List<Map<String, Object>> list) {
        this.list = list;
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TEXT_AND_IMAGE) {
            View view = mInflator.inflate(R.layout.i_text_image, parent, false);
            return new TextAndImageHolder(view);
        } else if (viewType == TEXT) {
            View view = mInflator.inflate(R.layout.i_text, parent, false);
            return new TextViewHolder(view);
        } else if (viewType == TITLE) {
            View view = mInflator.inflate(R.layout.i_title, parent, false);
            return new TextViewHolder(view);
        } else if (viewType == IMAGE) {
            View view = mInflator.inflate(R.layout.i_text_image, parent, false);
            return new TextAndImageHolder(view);
        } else if (viewType == DOP) {
            View view = mInflator.inflate(R.layout.i_dop, parent, false);
            return new DopViewHolder(view);
        } else {
            View view = mInflator.inflate(R.layout.i_other, parent, false);
            return new OtherViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (int) list.get(position).get(Constants.PATTERN);
    }
}
