package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import com.google.maps.android.PolyUtil;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by Bakht on 01.10.2016.
 */
public class DirectPresenter implements IDirectPresenter, ServerCallback<Object> {

    private IDirectModel directModel;
    private IDirectView directView;

    public DirectPresenter(IDirectView directView) {
        directModel = new DirectModel(this);
        this.directView = directView;
    }

    @Override
    public void createDirect(String type, double endLatitude, double endLongitude) {
        if (directView.checkConnect()) {
            directView.showProgress();
            directModel.getDirectFromServer(type, endLatitude, endLongitude);
        } else {
            directView.error("Нет доступа к сети");
        }
    }

    @Override
    public void success(Object response) {
        directView.hideProgress();
        Map<String, Object> map = (Map<String, Object>) response;
        Map<String, Object> routes = (Map<String, Object>) ((List<Object>) map.get("routes")).get(0);
        String poli = ((Map) routes.get("overview_polyline")).get("points").toString();
        directView.drawDirect(PolyUtil.decode(poli));
    }

    @Override
    public void error(String message) {
        directView.hideProgress();
        directView.error(message);
    }
}
