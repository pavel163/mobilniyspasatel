package mcs.nsign.by.mcs.screens.info.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.screens.info.adapter.holder.ReferencesViewHolder;

/**
 * Created by mac1 on 21.09.16.
 */
public class ReferencesAdapter extends SearchListAdapter {

    public ReferencesAdapter(Context context, List<Map<String, Object>> list) {
        super(context, list);
    }

    @Override
    public ReferencesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflator.inflate(R.layout.i_reference, parent, false);
        return new ReferencesViewHolder(view);
    }
}
