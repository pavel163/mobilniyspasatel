package mcs.nsign.by.mcs.screens.contacts;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import mcs.nsign.by.mcs.R;

public class PresenterContacts implements IPresenterContacts {

    public IViewContacts view;
    public ModelContacts model;
    public Context context;

    public PresenterContacts(Context context, IViewContacts view, ModelContacts model) {
        this.model = model;
        this.view = view;
        this.context = context;
    }

    public void addContact(String user_id, String name, String surname, String phone, String accesstoken, ConnectivityManager connectivityManager, int countOfContacts) {
        phone = phone.replaceAll(" ", "");
        phone = phone.replaceFirst("\\+7", "7");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (name == null || name.equals("")) {
            view.showToast(context.getResources().getString(R.string.ERROR_EMPTY_NAME));
            return;
        }

        if (phone == null || phone.equals("")) {
            view.showToast(context.getResources().getString(R.string.ERROR_EMPTY_PHONE));
            return;
        }

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            if (countOfContacts > 2){
                view.showToast(context.getResources().getString(R.string.ERROR_MORE_THAN_THREE));
                return;
            } else {
                model.postContact(user_id, name, surname, phone, accesstoken);
            }
        } else {
            view.showErrorConnection(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    public void getAllContacts(String accesstoken, String id){
        view.showProgressDialog();
        model.getAllContacts(accesstoken, id);
    }

    @Override
    public void changeContact(String id, String name, String surname, String phone, String accesstoken, ConnectivityManager connectivityManager) {
        phone = phone.replaceAll(" ", "");
        phone = phone.replaceFirst("\\+7", "7");

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (name == null || name.equals("")) {
            view.showToast(context.getResources().getString(R.string.ERROR_EMPTY_NAME));
            return;
        }

        if (phone == null || phone.equals("")) {
            view.showToast(context.getResources().getString(R.string.ERROR_EMPTY_PHONE));
            return;
        }

        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            model.changeContact(id, name, surname, phone, accesstoken);
        } else {
            view.showErrorConnection(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    @Override
    public void deleteContact(String id, String accesstoken, ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            model.deleteContact(id, accesstoken);
        } else {
            view.showErrorConnection(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }
}
