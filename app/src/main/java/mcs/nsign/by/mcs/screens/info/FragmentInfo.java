package mcs.nsign.by.mcs.screens.info;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.ebr163.core.util.ItemClickSupport;
import com.flurry.android.FlurryAgent;
import com.yandex.metrica.YandexMetrica;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FInfoBinding;
import mcs.nsign.by.mcs.screens.info.adapter.InfoMenuAdapter;
import mcs.nsign.by.mcs.screens.info.adapter.SearchListAdapter;
import mcs.nsign.by.mcs.screens.info.download.DownloadFragment;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.service.databases.DBHelper;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.ui.recyclerview.DividerItemDecoration;
import mcs.nsign.by.mcs.utils.FragmentFactory;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.UpdateUtils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by Bakht on 07.09.2016.
 */
@RuntimePermissions
public class FragmentInfo extends BaseBindingFragment<FInfoBinding> implements IViewInfo, DownloadFragment.DownloadListener, SearchView.OnQueryTextListener, LocationListener {

    private IModelInfo modelInfo;
    private IPresenterInfo presenterInfo;
    private SearchListAdapter adapter;
    private ProgressDialog progressDialog;
    private LocationManager locationManager;
    private SearchListAdapter itemsAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    protected void afterBinding() {
        super.afterBinding();
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Информация");
        toolbar.setBackgroundColor(Color.parseColor("#e5e8e9"));

        RetrofitClient client = new RetrofitClient();
        modelInfo = new ModelInfo(client);
        presenterInfo = new PresenterInfo(this, modelInfo);
        presenterInfo.initMenu();
        presenterInfo.initSearchList();

        if (SettingsService.get(getActivity(), "update").equals("1")) {
            createUpdateDialog();
        }

        AppUtils.sendScreenNameStatistic("info_screen", "Информация");
    }

    private void checkPosition() {
        if (Constants.longitude == 0 && Constants.latitude == 0) {
            tryToShowGPSDialog();
        } else {
            AppUtils.startActivityFromFragment(this, FragmentFactory.REFERENCE_LIST, null);
        }
    }

    private void tryToShowGPSDialog() {
        if (hasGPSDevice(getActivity())) {
            String provider = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (!provider.contains("gps")) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Включить GPS?")
                        .setPositiveButton(getResources().getString(R.string.OK),
                                (dialog, which) -> {
                                    Intent gpsOptionsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivityForResult(gpsOptionsIntent, 1);
                                })
                        .setNegativeButton("Нет", (dialog, which) -> {
                        }).show();
            } else {
                FragmentInfoPermissionsDispatcher.createLocationManagerWithCheck(FragmentInfo.this);
            }
        }
    }

    private void createUpdateDialog() {
        String sizeUpdates = UpdateUtils.getUpdateSize(getActivity());
        if (sizeUpdates.equals("0")) {
            return;
        }
        if (checkConnect()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Доступны новые данные. Размер " + UpdateUtils.getUpdateSize(getActivity()) + ". Обновить?")
                    .setPositiveButton(getResources().getString(R.string.OK),
                            (dialog, which) -> {
                                DownloadFragment dialogFragment = new DownloadFragment();
                                dialogFragment.show(getChildFragmentManager(), "download");
                                dialogFragment.setDownloadListener(this);
                            })
                    .setNegativeButton("Нет", (dialog, which) -> {
                    }).show();
        }
    }

    public boolean hasGPSDevice(Context context) {
        LocationManager mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null) return false;
        List<String> providers = mgr.getAllProviders();
        if (providers == null) return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void createLocationManager() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
        }
        progressDialog.setMessage("Определение координат...");
        progressDialog.show();
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            tryToShowGPSDialog();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        FragmentInfoPermissionsDispatcher.onRequestPermissionsResult(FragmentInfo.this, requestCode, grantResults);
    }

    @Override
    public int getLayout() {
        return R.layout.f_info;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setQueryHint("Ключевое слово");
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        presenterInfo.search(newText, "");
        return true;
    }

    @Override
    public void setVisibleMenu() {
        fragmentBinder.menu.menu.setVisibility(View.VISIBLE);
        fragmentBinder.list.recycler.setVisibility(View.GONE);
    }

    @Override
    public void initMenuItems(List<Map<String, Object>> items) {
        if (itemsAdapter == null) {
            itemsAdapter = new InfoMenuAdapter(getActivity(), items);
        } else {
            itemsAdapter.setData(items);
        }
        fragmentBinder.menu.menu.setAdapter(itemsAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.menu.menu.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.menu.menu).setOnItemClickListener((recyclerView1, i, view1) -> {
            if (i != items.size() - 1) {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.NAME, items.get(i).get(Constants.NAME).toString());
                params.put(Constants.TYPE, items.get(i).get(Constants.ID).toString());
                params.put(Constants.PARENT_ICON, items.get(i).get(Constants.ICON).toString());
                AppUtils.startActivityFromFragment(this, FragmentFactory.FIRST_HELP, params);
            } else {
                checkPosition();
            }
        });
    }

    @Override
    public void setVisibleSearchList() {
        fragmentBinder.list.recycler.setVisibility(View.VISIBLE);
        fragmentBinder.menu.menu.setVisibility(View.GONE);
    }

    @Override
    public void notifySearchList() {
        fragmentBinder.list.recycler.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void initSearchList(List<Map<String, Object>> data) {
        if (adapter == null) {
            adapter = new SearchListAdapter(getActivity(), modelInfo.getDefualtInfo());
        }
        fragmentBinder.list.recycler.setAdapter(adapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.list.recycler.addItemDecoration(new DividerItemDecoration(divider));
        ItemClickSupport.addTo(fragmentBinder.list.recycler).setOnItemClickListener((recyclerView1, i, view1) -> {
            Intent intent = new Intent(getActivity(), ActivityWithFragment.class);
            Map<String, Object> map = modelInfo.getInfo(i);
            intent.putExtra(Constants.ID, map.get(Constants.ID).toString());
            intent.putExtra(Constants.NAME, map.get(Constants.NAME).toString());
            if (map.get(Constants.TABLE).equals(DBHelper.ITEMS)) {
                intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REVIEW);
            } else if (map.get(Constants.TABLE).equals(DBHelper.INSTITUTIONS)) {
                intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REFERENCE_REVIEW);
            }
            startActivity(intent);
        });
    }

    @Override
    public boolean checkConnect() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    @Override
    public void showError(String error) {
        Log.e("error", error);
    }

    @Override
    public void onLocationChanged(Location location) {
        Constants.latitude = location.getLatitude();
        Constants.longitude = location.getLongitude();
        progressDialog.hide();
        AppUtils.startActivityFromFragment(this, FragmentFactory.REFERENCE_LIST, null);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void finish() {
        SettingsService.set(getActivity(), Constants.START, "0");
        presenterInfo.initMenu();
    }


    @Override
    public void onResume() {
        super.onResume();
        FlurryAgent.logEvent("Информация");
        YandexMetrica.reportEvent("Информация");
    }

    @Override
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(getActivity());
    }
}
