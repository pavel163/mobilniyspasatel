package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.view.View;
import android.widget.TextView;

import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by mac1 on 21.09.16.
 */
public class ReferencesViewHolder extends SearchViewHolder {

    public TextView distance;
    public TextView address;

    public ReferencesViewHolder(View itemView) {
        super(itemView);
        distance = (TextView) itemView.findViewById(R.id.distance);
        address = (TextView) itemView.findViewById(R.id.address);
    }

    @Override
    public void bind(Map<String, Object> map) {
        super.bind(map);
        address.setText(map.get(Constants.ADDRESS).toString());
        if (map.get(Constants.DISTANCE) != null && !map.get(Constants.DISTANCE).equals("")) {
            distance.setText(map.get(Constants.DISTANCE).toString());
        } else {
            distance.setText("Дистанция не определена");
        }
    }
}
