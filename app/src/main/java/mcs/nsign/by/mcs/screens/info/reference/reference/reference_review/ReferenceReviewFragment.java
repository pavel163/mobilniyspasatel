package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FReviewReferenceBinding;
import mcs.nsign.by.mcs.utils.utils.MapUtils;

/**
 * Created by mac1 on 14.09.16.
 */
public class ReferenceReviewFragment extends BaseBindingFragment<FReviewReferenceBinding> {

    @Override
    protected void afterBinding() {
        super.afterBinding();
        Map<String, Object> map = MCSApp.dbHelper.getInstitution(getActivity().getIntent().getStringExtra(Constants.ID));
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(map.get(Constants.NAME).toString());

        fragmentBinder.name.setText(map.get(Constants.NAME).toString());
        fragmentBinder.address.setText(map.get(Constants.ADDRESS).toString());
        if (map.get(Constants.FAX) != null && !map.get(Constants.FAX).equals("")) {
            fragmentBinder.fax.setText(map.get(Constants.FAX).toString());
        } else {
            fragmentBinder.containerFax.setVisibility(View.GONE);
        }

        if (map.get(Constants.PHONE) != null && !map.get(Constants.PHONE).equals("")) {
            fragmentBinder.phone.setText(map.get(Constants.PHONE).toString());
        } else {
            fragmentBinder.containerPhone.setVisibility(View.GONE);
        }

        if (map.get(Constants.EMAIL) != null && !map.get(Constants.EMAIL).equals("")) {
            fragmentBinder.email.setText(map.get(Constants.EMAIL).toString());
        } else {
            fragmentBinder.containerEmail.setVisibility(View.GONE);
        }

        fragmentBinder.distance.setText("Расстояние до объекта: " + MapUtils.getDestantion(map));

        fragmentBinder.direct.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), MapDirectActivity.class);
            intent.putExtra(Constants.LATITUDE, map.get(Constants.LATITUDE).toString());
            intent.putExtra(Constants.LONGITUDE, map.get(Constants.LONGITUDE).toString());
            startActivity(intent);
        });
    }

    @Override
    public int getLayout() {
        return R.layout.f_review_reference;
    }
}
