package mcs.nsign.by.mcs.screens.feedback;

import mcs.nsign.by.mcs.utils.NetworkCallback;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by mac1 on 25.08.16.
 */
public interface IViewFeedBack extends NetworkCallback, ServerCallback {

    void showErrorConnection(String error);
}
