package mcs.nsign.by.mcs.screens.contacts;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IModelContacts {

    void postContact(String user_id, String name, String surname, String phone, String accesstoken);

    void getAllContacts(String accesstoken, String id);

    void changeContact(String id, String name, String surname, String phone, String accesstoken);

    void deleteContact(String id, String accesstoken);
}
