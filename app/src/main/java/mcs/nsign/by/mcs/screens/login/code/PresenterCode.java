package mcs.nsign.by.mcs.screens.login.code;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Ruslan on 22.11.2015.
 */
public class PresenterCode implements IPresenterCode, ServerCallback<Map<String, Object>>, ServerObserver<Object, Object> {

    private IViewCode view;
    private IModelCode model;
    private Observable timerObservable;

    public PresenterCode(IViewCode view) {
        this.model = new ModelCode();
        this.model.setServerCallback(this);
        this.model.setServerObserver(this);
        this.view = view;
        createObservableTimer();
    }

    private void createObservableTimer() {
        timerObservable = Observable.create((Observable.OnSubscribe<Integer>) subscriber -> {
            for (int i = 1; i <= 60; i++) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                    subscriber.onNext(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            subscriber.onCompleted();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void tryToEnter(String code, String id) {
        if (view.checkAccessNetwork()) {
            model.makeRequestCode(code, id);
        } else {
            view.showErrorConnection("Проверьте соединение с интернетом");
        }
    }

    @Override
    public void repeatRequestCode(String id, String phone) {
        model.repeatRequestCode(id, phone);
    }

    @Override
    public void handleVerifiedResponse(int verified) {
        if (verified == 1) {
            view.successAutorization();
            return;
        }

        if (verified == 0) {
            view.error("Пароль подтверждения неверен");
            return;
        }
    }

    @Override
    public void startTimer() {
        timerObservable.subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {
                view.completeTimer();
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(Integer integer) {
                view.showTimerProgress(integer);
            }
        });
    }

    @Override
    public void success(Map<String, Object> response) {
        view.success(response);
    }

    @Override
    public void error(String message) {
        view.error(message);
    }

    @Override
    public void successExecuteObject(Object obj) {
    }

    @Override
    public void successExecuteObject2(Object obj) {

    }

    @Override
    public void failedExecute(Object err) {

    }
}
