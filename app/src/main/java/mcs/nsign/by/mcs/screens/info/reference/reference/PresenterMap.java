package mcs.nsign.by.mcs.screens.info.reference.reference;

import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Bakht on 18.09.2016.
 */
public class PresenterMap implements IPresenterMap {

    private IModelMap modelMap;
    private IViewMap viewMap;
    private Subscription mapSubscription;

    public PresenterMap(IViewMap viewMap, IModelMap modelMap) {
        this.viewMap = viewMap;
        this.modelMap = modelMap;
    }

    @Override
    public void drawMarkers(String parentId) {
        Observable<List<Map<String, Object>>> observable = Observable.create((Observable.OnSubscribe<List<Map<String, Object>>>) subscriber -> {
            subscriber.onNext(modelMap.getMarkers(parentId));
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

        mapSubscription = observable.subscribe(o -> {
                    viewMap.drawMarkers(o);
                });
    }

    @Override
    public void unSubscribe() {
        if (mapSubscription != null && !mapSubscription.isUnsubscribed()) {
            mapSubscription.unsubscribe();
        }
    }
}
