package mcs.nsign.by.mcs.screens.info;

import java.util.Map;

/**
 * Created by mac1 on 16.09.16.
 */
public interface Loader {

    void checkUpdate(String accessToken, Map<String, String> dataUpdates);
    void loadReferenceType(String token, String lastDataUpdate);
    void loadReference(String token, String lastDataUpdate);
    void loadHelpType(String token, String lastDataUpdate);
    void loadHelpItems(String token, String lastDataUpdate);
    void loadHelpContent(String token, String lastDataUpdate);
}
