package mcs.nsign.by.mcs.screens.contacts;

import mcs.nsign.by.mcs.model.ContactsObject;

/**
 * Created by Bakht on 15.08.2016.
 */
public interface EventCallBack {
    void delete(ContactsObject contactsObject);
    void change(ContactsObject contactsObject);
}
