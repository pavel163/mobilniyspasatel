package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by mac1 on 21.09.16.
 */
public class SearchViewHolder extends RecyclerView.ViewHolder{

    public TextView name;
    public ImageView imageView;

    public SearchViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);
        imageView = (ImageView) itemView.findViewById(R.id.image);
    }

    public void bind(Map<String, Object> map) {
        name.setText(map.get(Constants.NAME).toString());
        byte[] icon = null;
        Object showIcon = map.get(Constants.SHOW_ICON);
        if (showIcon != null && (int) showIcon == 0 && map.get(Constants.PARENT_ICON) != null) {
            icon = Base64.decode(map.get(Constants.PARENT_ICON).toString(), Base64.DEFAULT);
        } else if (map.get(Constants.ICON) != null) {
            icon = Base64.decode(map.get(Constants.ICON).toString(), Base64.DEFAULT);
        }

        if (imageView != null) {
            Glide.with(imageView.getContext())
                    .load(icon)
                    .into(imageView);
        }
    }
}
