package mcs.nsign.by.mcs.screens.info.review;


/**
 * Created by Bakht on 11.09.2016.
 */
public class PresenterReview implements IPresenterReview {

    private final IModelReview modelReview;
    private final IViewReview view;

    public PresenterReview(IModelReview modelReview, IViewReview view){
        this.modelReview = modelReview;
        this.view = view;
    }

    @Override
    public void initList(String parentId) {
        view.initList(modelReview.getInfo(parentId));
    }
}
