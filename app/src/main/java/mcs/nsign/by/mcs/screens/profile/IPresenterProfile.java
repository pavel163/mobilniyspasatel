package mcs.nsign.by.mcs.screens.profile;

import android.content.Context;

import mcs.nsign.by.mcs.model.Profile;

/**
 * Created by mac1 on 18.08.16.
 */
public interface IPresenterProfile {

    void changeProfile(Context context, String accesstoken, String id, Profile profile);

    void getProfile(Context context, String accesstoken, String id);
}
