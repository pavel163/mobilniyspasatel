package mcs.nsign.by.mcs.screens.info.download;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import constants.WebSettings;
import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.download.model.Download;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.MyISReader;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.UpdateUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

public class DownloadService extends IntentService {

    private String accessToken;
    private List<Map<String, String>> references;
    private Retrofit retrofit;
    private DownloadApiFactory downloadApiFactory;
    private int totalReferenceSize;
    private String referenceName;

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private Gson gson;

    @Override
    protected void onHandleIntent(Intent intent) {
        references = UpdateUtils.getReferencesSize(getApplicationContext());
        totalReferenceSize = references.size();

        GsonBuilder builder = new GsonBuilder();
        gson = builder.create();

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_download)
                .setContentTitle("Загрузка")
                .setContentText("Пожалуйста подождите")
                .setAutoCancel(true);
        notificationManager.notify(0, notificationBuilder.build());

        accessToken = ((MCSApp) getApplication()).getValue(Constants.TOKEN);

        retrofit = new Retrofit.Builder()
                .baseUrl(WebSettings.URL)
                .client(WebSettings.getDownloadHttpClient())
                .build();

        DownLoadApi downLoadApi = retrofit.create(DownLoadApi.class);
        downloadApiFactory = new DownloadApiFactory(downLoadApi);
        initDownload();
    }

    private void initDownload() {
        for (int i = 0; i < references.size(); i++) {
            Map<String, String> reference = references.get(i);
            referenceName = reference.get(Constants.NAME);
            Call<ResponseBody> request = downloadApiFactory.getApiMethod(referenceName, accessToken, reference.get(Constants.DATA_UPDATE),
                    SettingsService.get(getApplicationContext(), Constants.START));
            try {
                downloadFile(request.execute().body(), Long.parseLong(reference.get("size").toString()), i);
            } catch (IOException e) {
                e.printStackTrace();
                closeDialog(true);
                break;
            }
        }
        closeDialog(false);
    }

    private void closeDialog(boolean flag) {
        Intent intent = new Intent(DownloadFragment.ERROR_LOAD);
        intent.putExtra("flag", flag);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
        notificationManager.cancel(0);
    }

    private void downloadFile(ResponseBody body, long fileSize, int number) throws IOException {
        Download download = new Download();
        download.setNumber(number);
        download.setTotalNumber(totalReferenceSize);
        if (fileSize != 0) {
            if (body == null) {
                return;
            }
            InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
            MyISReader myISReader = new MyISReader(bis);
            readFromJson(myISReader, fileSize, number);
            onDownloadComplete(number);
            bis.close();
        } else {
            download.setProgress(100);
            sendNotification(download);
            onDownloadComplete(number);
        }
    }

    private void readFromJson(MyISReader myISReader, long fileSize, int number) throws IOException {
        JsonReader reader = new JsonReader(myISReader);
        Download download = new Download();
        download.setNumber(number);
        download.setTotalNumber(totalReferenceSize);
        List<Map<String, Object>> data = new ArrayList<>();
        reader.beginObject();
        while (reader.hasNext()) {
            if (!"items".equals(reader.nextName())) {
                reader.skipValue();
                continue;
            }
            reader.beginArray();
            if (reader.hasNext()) {
                while (reader.hasNext()) {
                    Map<String, Object> map = gson.fromJson(reader, Map.class);
                    data.add(map);
                    if (data.size() == 100) {
                        saveInDB(data);
                        int progress = (int) ((myISReader.getReadBytes() * 100) / fileSize);
                        download.setProgress(progress);
                        sendNotification(download);
                        data.clear();
                    }
                }
                if (data.size() != 0) {
                    saveInDB(data);
                    data.clear();
                }
            }
            reader.endArray();
        }
        reader.close();
    }

    private void saveInDB(List<Map<String, Object>> data) {
        if (referenceName.equals(Constants.SIZE_OBJECT)) {
            MCSApp.dbHelper.putInstitutions(data);
            SettingsService.set(getApplicationContext(), Constants.LOAD_REFERENCE, AppUtils.getCurrentData());
        } else if (referenceName.equals(Constants.SIZE_OBJECT_TYPE)) {
            MCSApp.dbHelper.putReference(data);
            SettingsService.set(getApplicationContext(), Constants.LOAD_REFERENCE_TYPE, AppUtils.getCurrentData());
        } else if (referenceName.equals(Constants.SIZE_HELP_CONTENT)) {
            MCSApp.dbHelper.putHelpContent(data);
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_CONTENT, AppUtils.getCurrentData());
        } else if (referenceName.equals(Constants.SIZE_HELP_MENU)) {
            MCSApp.dbHelper.putHelpItems(data);
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_ITEMS, AppUtils.getCurrentData());
        } else if (referenceName.equals(Constants.SIZE_HELP_TYPE)) {
            MCSApp.dbHelper.putHelpTypes(data);
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_TYPE, AppUtils.getCurrentData());
        }
    }

    private void sendNotification(Download download) {
        sendIntent(download);
        notificationBuilder.setProgress(100, download.getProgress(), false);
        if (download.getProgress() > 100) {
            notificationBuilder.setContentText("Загружено " + 100 + "/100 %");
        } else {
            notificationBuilder.setContentText("Загружено " + download.getProgress() + "/100 %");
        }
        notificationManager.notify(0, notificationBuilder.build());
    }


    private void sendIntent(Download download) {
        Intent intent = new Intent(DownloadFragment.MESSAGE_PROGRESS);
        intent.putExtra("download", download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete(int number) {
        Download download = new Download();
        download.setProgress(100);
        if (number == totalReferenceSize - 1) {
            download.setNumber(totalReferenceSize);
            notificationBuilder.setContentText("Загружено " + totalReferenceSize + "/" + totalReferenceSize);
            SettingsService.set(getApplicationContext(), "update", "0");
            UpdateUtils.resetSizesUpdate(getApplicationContext());
        } else {
            notificationBuilder.setContentText("Загружено " + number + "/" + totalReferenceSize);
            download.setNumber(number);
        }
        download.setTotalNumber(totalReferenceSize);
        sendIntent(download);

        notificationManager.cancel(0);
        notificationBuilder.setProgress(0, 0, false);
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }
}
