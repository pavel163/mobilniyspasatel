package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

/**
 * Created by Bakht on 01.10.2016.
 */
public interface IDirectModel {
    void getDirectFromServer(String type, double latitude, double longitude);
}
