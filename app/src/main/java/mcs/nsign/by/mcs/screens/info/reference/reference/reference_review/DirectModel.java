package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.utils.ServerCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bakht on 01.10.2016.
 */
public class DirectModel implements IDirectModel {

    private final String url = "https://maps.googleapis.com/";
    private DirectApi directApi;
    private ServerCallback<Object> serverCallback;

    public DirectModel(ServerCallback<Object> serverCallback) {
        this.serverCallback = serverCallback;

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        directApi = retrofit.create(DirectApi.class);
    }

    @Override
    public void getDirectFromServer(String type, double latitude, double longitude) {
        Call<Object> result = directApi.getDirect(String.valueOf(Constants.latitude) + "," + String.valueOf(Constants.longitude),
                String.valueOf(latitude) + "," + String.valueOf(longitude),
                "AIzaSyB6ZXn8S6m-q6c-o0arqjVu3Cco8sYYd2M", type);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverCallback.success(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("Не удалось получить точки маршрута");
            }
        });
    }
}
