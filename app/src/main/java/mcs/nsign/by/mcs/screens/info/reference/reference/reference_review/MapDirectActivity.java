package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;

import com.ebr163.core.newApi.base.activity.BaseBindingFragmentActivity;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.AMapDirectBinding;

public class MapDirectActivity extends BaseBindingFragmentActivity<AMapDirectBinding> implements TabLayout.OnTabSelectedListener {

    private MapDirectFragment mapDirectFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinder.appBar.toolbar.setTitle("На автомобиле");

        TabLayout.Tab tab = activityBinder.appBar.tabs.newTab().setIcon(R.drawable.car);
        tab.getIcon().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        activityBinder.appBar.tabs.addTab(tab, 0);
        activityBinder.appBar.tabs.addTab(activityBinder.appBar.tabs.newTab().setIcon(R.drawable.walk), 1);
        activityBinder.appBar.tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        activityBinder.appBar.tabs.addOnTabSelectedListener(this);
        activityBinder.appBar.tabs.setSelectedTabIndicatorColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    @Override
    protected void initFragment() {
        mapDirectFragment = (MapDirectFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    }

    @Override
    public int getLayout() {
        return R.layout.a_map_direct;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        tab.getIcon().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
        switch (tab.getPosition()) {
            case 0:
                mapDirectFragment.changeTypeTravel(Constants.DRIVING);
                activityBinder.appBar.toolbar.setTitle("На автомобиле");
                break;
            case 1:
                mapDirectFragment.changeTypeTravel(Constants.WALKING);
                activityBinder.appBar.toolbar.setTitle("Пешком");
                break;
            default:
                mapDirectFragment.changeTypeTravel(Constants.DRIVING);
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        tab.getIcon().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }
}
