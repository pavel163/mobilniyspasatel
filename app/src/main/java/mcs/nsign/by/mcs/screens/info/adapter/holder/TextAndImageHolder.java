package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 11.09.2016.
 */
public class TextAndImageHolder extends BaseViewHolder<Map<String, Object>> {

    private TextView title;
    private ImageView image;
    private TextView text;

    public TextAndImageHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.title);
        image = (ImageView) itemView.findViewById(R.id.image);
        text = (TextView) itemView.findViewById(R.id.text);
    }

    @Override
    public void bind(Map<String, Object> map) {
        if (map.get(Constants.TITLE) != null) {
            title.setText(map.get(Constants.TITLE).toString().trim());
        }

        if (map.get(Constants.TEXT) != null) {
            text.setText(map.get(Constants.TEXT).toString().trim());
        }

        if (map.get(Constants.IMAGE) != null) {
            image.setImageDrawable(null);
            Glide.with(image.getContext())
                    .load(Base64.decode(map.get(Constants.IMAGE).toString(), Base64.DEFAULT))
                    .fitCenter()
                    .into(image);
        } else {
            image.setImageDrawable(null);
        }
    }
}
