package mcs.nsign.by.mcs.screens.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.databases.DBHelper;
import mcs.nsign.by.mcs.service.http.RetrofitClient;

/**
 * Created by Bakht on 10.09.2016.
 */
public class ModelInfo implements IModelInfo {

    private List<Map<String, Object>> list;
    private RetrofitClient retrofitClient;

    public ModelInfo(RetrofitClient retrofitClient) {
        list = new ArrayList<>();
        this.retrofitClient = retrofitClient;
    }

    @Override
    public List<Map<String, Object>> getDefualtInfo() {
        return list;
    }

    @Override
    public List<Map<String, Object>> getMenuItems() {
        List<Map<String, Object>> items = MCSApp.dbHelper.getTypesItems();
        Map<String, Object> reference = new HashMap<>();
        reference.put(Constants.NAME, "Справочник организаций");
        reference.put(Constants.TABLE, DBHelper.REFERENCE);
        reference.put(DBHelper.REFERENCE, DBHelper.REFERENCE);
        items.add(reference);
        return items;
    }

    @Override
    public void getInfo(String text) {
        List<Map<String, Object>> resultInfo = MCSApp.dbHelper.getInfoToName(text, "");

        for (Map<String, Object> map : resultInfo) {
            Object showIcon = map.get(Constants.SHOW_ICON);
            if (showIcon != null && (int) showIcon == 0) {
                map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getParentIcon(map.get(Constants.TYPE).toString()));
            }
        }

        List<Map<String, Object>> resultReference = MCSApp.dbHelper.getInstitutionsToName(text);
        for (Map<String, Object> map : resultReference) {
            map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getReferenceIcon(map.get(Constants.REFERENCE).toString()));
        }

        list.clear();
        list.addAll(resultInfo);
        list.addAll(resultReference);
    }

    @Override
    public Map<String, Object> getInfo(int position) {
        return list.get(position);
    }
}
