package mcs.nsign.by.mcs.dialogs;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Bakht on 14.08.2016.
 */
public class RatingDialogFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Нам важно Ваше мнение");
        alertDialogBuilder.setMessage("Оцените наше приложение и оставьте свой отзыв в Google Play");
        alertDialogBuilder.setPositiveButton("OK", (dialog, which) -> addCommentInPlayMarket());
        alertDialogBuilder.setNegativeButton("Отмена", (dialog, which) -> dialog.dismiss());
        return alertDialogBuilder.create();
    }

    private void addCommentInPlayMarket(){
        final String appPackageName = getActivity().getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
