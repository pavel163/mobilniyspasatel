package mcs.nsign.by.mcs.screens.info.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.adapter.holder.ReferencesViewHolder;
import mcs.nsign.by.mcs.screens.info.adapter.holder.SearchViewHolder;
import mcs.nsign.by.mcs.service.databases.DBHelper;

/**
 * Created by Bakht on 10.09.2016.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchViewHolder> {

    protected List<Map<String, Object>> list;
    protected LayoutInflater mInflator;

    public SearchListAdapter(Context context, List<Map<String, Object>> list) {
        this.list = list;
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = mInflator.inflate(R.layout.i_reference, parent, false);
            return new ReferencesViewHolder(view);
        } else {
            View view = mInflator.inflate(R.layout.i_expand, parent, false);
            return new SearchViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        Map<String, Object> map = list.get(position);
        holder.bind(map);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position).get(Constants.TABLE).equals(DBHelper.INSTITUTIONS)) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setData(List<Map<String, Object>> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }
}
