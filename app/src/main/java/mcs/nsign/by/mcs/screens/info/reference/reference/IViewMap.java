package mcs.nsign.by.mcs.screens.info.reference.reference;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 18.09.2016.
 */
public interface IViewMap {

   void drawMarkers(List<Map<String, Object>> markers);
}
