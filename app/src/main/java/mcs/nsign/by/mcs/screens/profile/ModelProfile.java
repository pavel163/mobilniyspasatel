package mcs.nsign.by.mcs.screens.profile;

import mcs.nsign.by.mcs.model.Profile;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by mac1 on 18.08.16.
 */
public class ModelProfile implements IModelProfile {

    public RetrofitClient client;

    public ModelProfile() {
        client = new RetrofitClient();
    }

    @Override
    public void changeProfile(String accesstoken, String id, Profile profile) {
        client.changeProfile(accesstoken, id, profile);
    }

    @Override
    public void getProfile(String accesstoken, String id) {
        client.getProfile(accesstoken, id);
    }

    @Override
    public void setServerObserver(ServerObserver serverObserver) {
        client.setServerObserver(serverObserver);
    }
}
