package mcs.nsign.by.mcs.screens.signal;

import android.content.Context;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IPresenterSignal {

    void getSos(Context context, String token, String id, double lan, double lon, String test);

    void getSmsStatus(Context context, String token, String id);

    void sendMyCurrentLocation(String token, String sosId, String latitude, String longitude);
}
