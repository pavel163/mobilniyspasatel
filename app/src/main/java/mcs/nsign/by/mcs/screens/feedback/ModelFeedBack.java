package mcs.nsign.by.mcs.screens.feedback;

import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by mac1 on 25.08.16.
 */
public class ModelFeedBack implements IModelFeedBack {

    public RetrofitClient client;

    public ModelFeedBack() {
        client = new RetrofitClient();
    }

    @Override
    public void sendMessage(String id, String message) {
        client.sendMessage(id, message);
    }

    @Override
    public void setServerCallback(ServerCallback serverCallback) {
        client.setServerCallback(serverCallback);
    }
}
