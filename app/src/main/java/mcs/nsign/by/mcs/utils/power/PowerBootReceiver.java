package mcs.nsign.by.mcs.utils.power;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PowerBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, PowerService.class));
    }
}