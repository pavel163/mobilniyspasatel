package mcs.nsign.by.mcs.screens.info.reference.reference;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.ebr163.core.util.ItemClickSupport;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FMapBinding;
import mcs.nsign.by.mcs.screens.info.adapter.MapReferenceAdapter;
import mcs.nsign.by.mcs.screens.info.reference.reference.cluster.CustomClusterRender;
import mcs.nsign.by.mcs.screens.info.reference.reference.cluster.MapClusterItem;
import mcs.nsign.by.mcs.ui.recyclerview.DividerItemDecoration;
import mcs.nsign.by.mcs.utils.FragmentFactory;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

public class MapReferenceFragment extends BaseBindingFragment<FMapBinding> implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, IViewMap, ClusterManager.OnClusterClickListener<MapClusterItem>, ClusterManager.OnClusterItemClickListener<MapClusterItem> {

    private GoogleMap googleMap;
    private IPresenterMap presenterMap;
    private Map<String, Map<String, Object>> mapObjects;
    private BottomSheetBehavior bottomSheetBehavior;
    private ClusterManager<MapClusterItem> clusterManager;
    private MapReferenceAdapter referencesAdapter;
    private List<String> clusterItems = new ArrayList<>();
    private Map<String, List<Map<String, Object>>> clusterObjects = new HashMap<>();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override
    protected void afterBinding() {
        super.afterBinding();
        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        bottomSheetBehavior = BottomSheetBehavior.from(fragmentBinder.layoutBottomSheet);
        fragmentBinder.fab.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            fragmentBinder.fab.setVisibility(View.INVISIBLE);
        });
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    fragmentBinder.fab.setVisibility(View.VISIBLE);
                } else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    fragmentBinder.fab.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        referencesAdapter = new MapReferenceAdapter(getActivity(), new ArrayList<>());
        fragmentBinder.recycler.setAdapter(referencesAdapter);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_sample);
        fragmentBinder.recycler.addItemDecoration(new DividerItemDecoration(divider));

        ItemClickSupport.addTo(fragmentBinder.recycler).setOnItemClickListener((recyclerView, i, view) -> {
            Intent intent = new Intent(getActivity(), ActivityWithFragment.class);
            intent.putExtra(Constants.ID, referencesAdapter.getItem(i).get(Constants.ID).toString());
            intent.putExtra(Constants.NAME, referencesAdapter.getItem(i).get(Constants.NAME).toString());
            intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REFERENCE_REVIEW);
            startActivity(intent);
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapObjects = new HashMap<>();
        this.googleMap = googleMap;
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.latitude, Constants.longitude), 10));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.googleMap.setMyLocationEnabled(true);

        clusterManager = new ClusterManager<>(getActivity(), this.googleMap);
        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setRenderer(new CustomClusterRender(getContext(), this.googleMap, clusterManager));
        this.googleMap.setOnMarkerClickListener(clusterManager);
        this.googleMap.setOnCameraIdleListener(clusterManager);

        IModelMap modelMap = new MapModel();
        presenterMap = new PresenterMap(this, modelMap);
        presenterMap.drawMarkers(getActivity().getIntent().getStringExtra(Constants.ID));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterMap.unSubscribe();
    }

    @Override
    public void drawMarkers(List<Map<String, Object>> markers) {
        for (int i = 0; i < markers.size(); i++) {
            Map<String, Object> map = markers.get(i);
            if (map.get(Constants.LATITUDE) == null || map.get(Constants.LONGITUDE) == null) {
                continue;
            }

            LatLng latLng = new LatLng(Double.parseDouble(map.get(Constants.LATITUDE).toString()),
                    Double.parseDouble(map.get(Constants.LONGITUDE).toString()));

            Bitmap bitmap;
            if ((int) map.get(Constants.SHOW_ICON) == 0 || map.get(Constants.ICON) == null || map.get(Constants.ICON).equals("")) {
                bitmap = AppUtils.createBitmapFromBase64(map.get(Constants.PARENT_ICON).toString());
            } else {
                bitmap = AppUtils.createBitmapFromBase64(map.get(Constants.ICON).toString());
            }
            float density = getActivity().getResources().getDisplayMetrics().density;
            bitmap = Bitmap.createScaledBitmap(bitmap, (int) (getResources().getInteger(R.integer.pin_size) * density), (int) (getResources().getInteger(R.integer.pin_size) * density), false);

            if (clusterItems.contains(latLng.toString())) {
                clusterManager.addItem(new MapClusterItem(latLng.latitude + 0.00000001, latLng.longitude, bitmap));
                continue;
            } else {
                clusterManager.addItem(new MapClusterItem(latLng, bitmap));
            }

            boolean isAddCluster = false;
            for (int j = i + 1; j < markers.size(); j++) {
                Map<String, Object> mapCluster = markers.get(j);
                LatLng latLngCluster = new LatLng(Double.parseDouble(mapCluster.get(Constants.LATITUDE).toString()), Double.parseDouble(mapCluster.get(Constants.LONGITUDE).toString()));

                if (latLng.toString().equals(latLngCluster.toString())) {
                    putInCluster(mapCluster);
                    isAddCluster = true;
                }
            }

            if (isAddCluster) {
                putInCluster(map);
                clusterItems.add(latLng.toString());
            } else {
                mapObjects.put(latLng.toString(), map);
            }
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.latitude, Constants.longitude), 12));
    }

    private void putInCluster(Map<String, Object> mapCluster) {
        LatLng latLngCluster = new LatLng(Double.parseDouble(mapCluster.get(Constants.LATITUDE).toString()), Double.parseDouble(mapCluster.get(Constants.LONGITUDE).toString()));
        List<Map<String, Object>> clusters = clusterObjects.get(latLngCluster.toString());
        if (clusters == null) {
            clusters = new ArrayList<>();
            clusterObjects.put(latLngCluster.toString(), clusters);
        }

        clusters.add(mapCluster);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (mapObjects.get(marker.getPosition().toString()) != null) {
            list.add(mapObjects.get(marker.getPosition().toString()));
            referencesAdapter.addData(list);
        } else if (clusterObjects.get(marker.getPosition().toString()) != null) {
            list.addAll(clusterObjects.get(marker.getPosition().toString()));
            referencesAdapter.addData(list);
        }
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        return false;
    }

    @Override
    public int getLayout() {
        return R.layout.f_map;
    }

    @Override
    public boolean onClusterClick(Cluster<MapClusterItem> cluster) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MapClusterItem marker : cluster.getItems()) {
            if (mapObjects.get(marker.getPosition().toString()) != null) {
                list.add(mapObjects.get(marker.getPosition().toString()));
            } else if (clusterObjects.get(marker.getPosition().toString()) != null) {
                list.addAll(clusterObjects.get(marker.getPosition().toString()));
            }
        }
        referencesAdapter.addData(list);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        return false;
    }

    @Override
    public boolean onClusterItemClick(MapClusterItem mapClusterItem) {
        List<Map<String, Object>> list = new ArrayList<>();
        if (mapObjects.get(mapClusterItem.getPosition().toString()) != null) {
            list.add(mapObjects.get(mapClusterItem.getPosition().toString()));
        }

        referencesAdapter.addData(list);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        return false;
    }
}
