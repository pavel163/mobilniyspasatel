package mcs.nsign.by.mcs.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.utils.UpdateUtils;

public class UpdateService extends Service implements ServerCallback<Map<String, Object>> {

    public UpdateService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        RetrofitClient client = new RetrofitClient();
        client.setServerCallback(this);
        String token = ((MCSApp) getApplication()).getValue(Constants.TOKEN);
        client.checkUpdateService(token, UpdateUtils.getDataUpdateTable(getApplication()));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void success(Map<String, Object> response) {
        if (!(response instanceof Map)) {
            return;
        }

        if (response == null) {
            SettingsService.set(getApplicationContext(), "update", "1");
        } else {
            long sizeObj = (long) ((double) response.get("obj_size"));
            long sizeObjType = (long) ((double) response.get("obj_type_size"));
            long sizeHelpType = (long) ((double) response.get("help_type_size"));
            long sizeHelpTypeMenu = (long) ((double) response.get("help_type_menu"));
            long sizeHelpTypeContent = (long) ((double) response.get("help_type_content"));

            if (sizeObj > 0) {
                SettingsService.set(getApplicationContext(), "update", "1");
            } else if (sizeObjType > 0) {
                SettingsService.set(getApplicationContext(), "update", "1");
            } else if (sizeHelpType > 0) {
                SettingsService.set(getApplicationContext(), "update", "1");
            } else if (sizeHelpTypeMenu > 0) {
                SettingsService.set(getApplicationContext(), "update", "1");
            } else if (sizeHelpTypeContent > 0) {
                SettingsService.set(getApplicationContext(), "update", "1");
            }

            SettingsService.set(getApplicationContext(), Constants.SIZE_OBJECT, String.valueOf(sizeObj));
            SettingsService.set(getApplicationContext(), Constants.SIZE_OBJECT_TYPE, String.valueOf(sizeObjType));
            SettingsService.set(getApplicationContext(), Constants.SIZE_HELP_TYPE, String.valueOf(sizeHelpType));
            SettingsService.set(getApplicationContext(), Constants.SIZE_HELP_MENU, String.valueOf(sizeHelpTypeMenu));
            SettingsService.set(getApplicationContext(), Constants.SIZE_HELP_CONTENT, String.valueOf(sizeHelpTypeContent));
        }
        stopSelf();
    }

    @Override
    public void error(String message) {

    }
}
