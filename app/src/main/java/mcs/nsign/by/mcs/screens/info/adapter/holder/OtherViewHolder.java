package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.utils.FragmentFactory;

/**
 * Created by Bakht on 25.09.2016.
 */
public class OtherViewHolder extends TextViewHolder {

    public OtherViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void bind(Map<String, Object> data) {
        Map<String, Object> map = MCSApp.dbHelper.getChildsItem(data.get(Constants.TEXT).toString());
        if (map == null){
            return;
        }
        ((TextView) itemView).setText(map.get(Constants.NAME).toString().trim());
        itemView.setOnClickListener(view -> {
            Intent intent = new Intent(itemView.getContext(), ActivityWithFragment.class);
            intent.putExtra(Constants.ID, map.get(Constants.ID).toString());
            intent.putExtra(Constants.NAME, map.get(Constants.NAME).toString());
            intent.putExtra(ActivityWithFragment.IDENTIFICATOR, FragmentFactory.REVIEW);
            itemView.getContext().startActivity(intent);
        });
    }
}
