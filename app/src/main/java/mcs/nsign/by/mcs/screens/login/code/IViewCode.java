package mcs.nsign.by.mcs.screens.login.code;

import mcs.nsign.by.mcs.utils.NetworkCallback;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IViewCode<T> extends NetworkCallback, ServerCallback<T> {

    void showErrorConnection(String error);

    void successAutorization();

    void showTimerProgress(int time);

    void startTimerTask();

    void completeTimer();
}
