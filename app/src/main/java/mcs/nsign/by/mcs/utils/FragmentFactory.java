package mcs.nsign.by.mcs.utils;

import android.support.v4.app.Fragment;

import mcs.nsign.by.mcs.screens.info.firs_help.FirstHelpFragment;
import mcs.nsign.by.mcs.screens.info.reference.ReferenceCategoryFragment;
import mcs.nsign.by.mcs.screens.info.reference.reference.reference_review.MapDirectFragment;
import mcs.nsign.by.mcs.screens.info.reference.reference.reference_review.ReferenceReviewFragment;
import mcs.nsign.by.mcs.screens.info.review.ReviewFragment;

/**
 * Created by mac1 on 14.09.16.
 */
public class FragmentFactory {

    public static final String REFERENCE_LIST = "reference_list";
    public static final String REFERENCE = "reference";
    public static final String REFERENCE_REVIEW = "reference_review";
    public static final String REVIEW = "review";
    public static final String FIRST_HELP = "first_help";
    public static final String DIRECT = "direct";

    public static Fragment createFragment(String identificator) {
        Fragment fragment = null;
        if (identificator.equals(REFERENCE_LIST)){
            fragment = new ReferenceCategoryFragment();
        } else if (identificator.equals(REFERENCE_REVIEW)){
            fragment = new ReferenceReviewFragment();
        } else if (identificator.equals(FIRST_HELP)){
            fragment = new FirstHelpFragment();
        } else if (identificator.equals(REVIEW)){
            fragment = new ReviewFragment();
        } else if (identificator.equals(DIRECT)) {
            fragment = new MapDirectFragment();
        }
        return fragment;
    }
}
