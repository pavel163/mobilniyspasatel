package mcs.nsign.by.mcs.utils;

public interface ServerObserver<Object, String> {

    void successExecuteObject(Object obj);

    void successExecuteObject2(Object obj);

    void failedExecute(String err);
}