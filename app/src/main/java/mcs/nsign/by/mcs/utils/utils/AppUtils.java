package mcs.nsign.by.mcs.utils.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.signal.FragmentSignal;
import mcs.nsign.by.mcs.service.SettingsService;
import ru.tinkoff.decoro.MaskDescriptor;
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser;
import ru.tinkoff.decoro.watchers.FormatWatcher;
import ru.tinkoff.decoro.watchers.FormatWatcherImpl;

/**
 * Created by Bakht on 15.08.2016.
 */
public class AppUtils {

    public static void hideKeyBoard(Activity activity) {
        if (activity == null){
            return;
        }
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showkeyBoard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static String getCurrentData() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));

        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static void startActivityFromFragment(Fragment fragment, String identifier, Map<String, String> params) {
        Intent intent = new Intent(fragment.getActivity(), ActivityWithFragment.class);
        intent.putExtra(ActivityWithFragment.IDENTIFICATOR, identifier);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                intent.putExtra(entry.getKey(), entry.getValue());
            }
        }
        fragment.startActivity(intent);
    }

    public static void startActivityFromActivity(Activity activity, String identifier, Map<String, String> params) {
        Intent intent = new Intent(activity, ActivityWithFragment.class);
        intent.putExtra(ActivityWithFragment.IDENTIFICATOR, identifier);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                intent.putExtra(entry.getKey(), entry.getValue());
            }
        }
        activity.startActivity(intent);
    }

    public static boolean checkAccessNetwork(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static void showErrorConnectDialog(Activity activity, String error) {
        new AlertDialog.Builder(activity)
                .setTitle(error)
                .setPositiveButton(activity.getResources().getString(R.string.OK), (dialog, which) -> {
                    dialog.dismiss();
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }

    public static boolean checkTimePeriod(Context context) {
        if (SettingsService.get(context, Constants.TIME).equals("")) {
            return true;
        }
        return System.currentTimeMillis() - Long.parseLong(SettingsService.get(context, Constants.TIME)) >= FragmentSignal.SIGNAL_TASK;
    }

    public static boolean onGPS(Context context) {
        final LocationManager mgr = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null) return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null) return false;
        String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        return provider.contains("gps");
    }

    public static void sendStatistic(String eventType, Map<String, String> params) {
        Bundle bundle = new Bundle();
        for (Map.Entry<String, String> param : params.entrySet()) {
            bundle.putString(param.getKey(), param.getValue());
        }
        MCSApp.firebaseAnalytics.logEvent(eventType, bundle);
    }

    public static void sendScreenNameStatistic(String type, String screenName) {
        Map<String, String> params = new HashMap<>();
        params.put(FirebaseAnalytics.Param.ITEM_NAME, screenName);
        AppUtils.sendStatistic(type, params);
    }

    public static void sendCustomEventStatistic(String type, String eventName, String eventType) {
        Map<String, String> params = new HashMap<>();
        params.put(type, eventName);
        AppUtils.sendStatistic(eventType, params);
    }

    public static Bitmap createBitmapFromBase64(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public static FormatWatcher getPhoneMask() {
        FormatWatcher formatWatcher = new FormatWatcherImpl(
                new UnderscoreDigitSlotsParser(),
                MaskDescriptor.ofRawMask("+7 ___ ___ __ __", true)
        );
        formatWatcher.getMask().setHideHardcodedHead(false);
        return formatWatcher;
    }
}
