package mcs.nsign.by.mcs.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.login.NameLoginActivity;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

/**
 * Created by Bakht on 08.10.2016.
 */

public class SosWidget extends AppWidgetProvider {

    private final static String ACTION_CHANGE_BUTTON = "mchs.send.sos.ACTION_BUTTON";
    public final static String WIDGET_SOS = "widget_sos";
    public final static String WIDGET_BLOCK = "widget_block";

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        SettingsService.set(context, "widgetID", String.valueOf(appWidgetIds[0]));
        updateWidget(context, appWidgetManager, appWidgetIds[0], "Нажмите быстро \nSOS\n3 раза");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        if (intent.getAction().equalsIgnoreCase(ACTION_CHANGE_BUTTON)) {
            buttonClick(context, intent);
        } else if (intent.getAction().equalsIgnoreCase(SosService.ACTION_CHANGE_SOS) || intent.getAction().equalsIgnoreCase(WidgetReceiver.ACTION_RESET_SOS)) {
            if (mAppWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
                updateWidget(context, AppWidgetManager.getInstance(context), mAppWidgetId, extras.getString(Constants.NAME));
            }
        }
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        SettingsService.set(context, "widgetID", "");
        SettingsService.set(context, WIDGET_BLOCK, "0");
    }

    void updateWidget(Context context, AppWidgetManager appWidgetManager, int widgetID, String message) {
        RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.widget);
        widgetView.setTextViewText(R.id.btnSos, message);
        Intent sosIntent = new Intent(context, SosWidget.class);
        sosIntent.setAction(ACTION_CHANGE_BUTTON);
        sosIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, widgetID, sosIntent, 0);
        widgetView.setOnClickPendingIntent(R.id.btnSos, pIntent);
        appWidgetManager.updateAppWidget(widgetID, widgetView);
    }

    private void buttonClick(Context context, Intent intent) {
        if (((MCSApp) context.getApplicationContext()).getValue(Constants.TOKEN).equals("")) {
            Intent newIntent = new Intent(context, NameLoginActivity.class);
            newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);
            return;
        }

        if (SettingsService.get(context, WIDGET_BLOCK).equals("1") || !AppUtils.checkTimePeriod(context)) {
            return;
        }
        int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        if (mAppWidgetId != AppWidgetManager.INVALID_APPWIDGET_ID) {
            int count;
            String sCount = SettingsService.get(context, WIDGET_SOS);
            if (sCount.equals("")) {
                count = 0;
            } else {
                count = Integer.parseInt(sCount);
            }

            if (SettingsService.get(context, WidgetReceiver.WIDGET_RESET).equals("0") || SettingsService.get(context, WidgetReceiver.WIDGET_RESET).equals("")) {
                count = 0;
                SettingsService.set(context, WidgetReceiver.WIDGET_RESET, "1");
                AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
                Intent alarmIntent = new Intent(context, WidgetReceiver.class);
                alarmIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                PendingIntent alarmPendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
                alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() + 3, alarmPendingIntent);
            }

            if (count + 1 >= 3) {
                updateWidget(context, AppWidgetManager.getInstance(context), mAppWidgetId, "Отправка сигнала SOS");
                SettingsService.set(context, WIDGET_SOS, "");
                Intent service = new Intent(context, SosService.class);
                SettingsService.set(context, AppWidgetManager.EXTRA_APPWIDGET_ID, String.valueOf(mAppWidgetId));
                context.startService(service);
            } else {
                SettingsService.set(context, WIDGET_SOS, String.valueOf(count + 1));
                updateWidget(context, AppWidgetManager.getInstance(context), mAppWidgetId, String.valueOf(count + 1));
            }
        }
    }
}
