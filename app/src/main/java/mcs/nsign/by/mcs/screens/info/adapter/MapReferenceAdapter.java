package mcs.nsign.by.mcs.screens.info.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.screens.info.adapter.holder.MapReferenceHolder;

/**
 * Created by Bakht on 01.10.2016.
 */
public class MapReferenceAdapter extends RecyclerView.Adapter<MapReferenceHolder> {

    protected List<Map<String, Object>> list;
    protected LayoutInflater mInflator;

    public MapReferenceAdapter(Context context, List<Map<String, Object>> list) {
        this.list = list;
        mInflator = LayoutInflater.from(context);
    }

    @Override
    public MapReferenceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflator.inflate(R.layout.i_map_reference, parent, false);
        return new MapReferenceHolder(view);
    }

    @Override
    public void onBindViewHolder(MapReferenceHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Map<String, Object>> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    public Map<String, Object> getItem(int position) {
        return list.get(position);
    }
}
