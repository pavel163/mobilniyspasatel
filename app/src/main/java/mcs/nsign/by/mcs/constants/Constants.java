package mcs.nsign.by.mcs.constants;

public class Constants {

    public static double latitude = 0;
    public static double longitude = 0;

    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String AGE = "age";
    public static final String BLOOD = "blood";
    public static final String ILLNESS = "illness";
    public static final String IMEI = "imei";
    public static final String TOKEN = "access_token";
    public static final String DATA_UPDATE = "data_update";
    public static final String TABLE = "table";
    public static final String VERIFIED = "verified";
    public static final String DELETED = "deleted";

    public static final String TIME = "time";
    public static final String MESSAGE = "message";
    public static final String SOS_ID = "sos_id";

    public static final String CONTACT_1 = "contact1";
    public static final String CONTACT_2 = "contact2";
    public static final String CONTACT_3 = "contact3";
    public static final String STATUS = "status";

    public static final String FIREBASE_NOTIFICATION_TOKEN = "fcm_token";

    public static String getStatus(Double number) {
        String status;
        switch (number.intValue()) {
            case 0:
                status = "Доставляется";
                break;
            case 1:
                status = "Доставлено";
                break;
            case 2:
                status = "Не доставлено";
                break;
            case 3:
                status = "Отказ в передаче";
                break;
            case 4:
                status = "Просрочено";
                break;
            case 5:
                status = "Удалено";
                break;
            case 6:
            default:
                status = "Неизвестный статус";
                break;
        }
        return status;
    }

    public static final String DEMO = "demo";
    public static final String START = "start";

    public static final String ID = "_id";
    public static final String REFERENCE = "reference";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ADDRESS = "address";
    public static final String FAX = "fax";
    public static final String TYPE = "type";
    public static final String ICON = "icon";
    public static final String COLOR = "color";
    public static final String SHOW_ICON = "showicon";
    public static final String PARENT = "parent";
    public static final String ITEM = "item";
    public static final String ORDER = "position";
    public static final String SORT = "sortby";
    public static final String TITLE = "title";
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String ALT = "alt";
    public static final String PATTERN = "pattern";
    public static final String LARGE_IMAGE = "limage";
    public static final String DESCRIPTION = "description";
    public static final String CONTENT = "content";
    public static final String DISTANCE = "distance";
    public static final String PARENT_ICON = "parent_icon";
    public static final String PARENT_TABLE = "parent_table";

    public static final String LOAD_REFERENCE_TYPE = "loadReferenceType";
    public static final String LOAD_REFERENCE = "loadReference";
    public static final String LOAD_HELP_TYPE = "loadHelpType";
    public static final String LOAD_HELP_ITEMS = "loadHelpItems";
    public static final String LOAD_HELP_CONTENT = "loadHelpContent";
    public static final String UPDATE_TYPE = "updateType";

    public static final String SIZE_OBJECT = "size_object";
    public static final String SIZE_OBJECT_TYPE = "size_object_type";
    public static final String SIZE_HELP_TYPE = "size_help_type";
    public static final String SIZE_HELP_MENU = "size_help_menu";
    public static final String SIZE_HELP_CONTENT = "size_help_content";

    //direct
    public static final String DRIVING = "driving";
    public static final String WALKING = "walking";

    //statistic
    public static final String SCREEN = "screen";
}
