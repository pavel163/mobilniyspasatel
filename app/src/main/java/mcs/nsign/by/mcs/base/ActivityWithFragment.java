package mcs.nsign.by.mcs.base;

import android.support.v4.app.Fragment;

import com.ebr163.core.newApi.base.activity.BaseBindingFragmentActivity;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.databinding.ActivityWithFBinding;
import mcs.nsign.by.mcs.utils.FragmentFactory;

public class ActivityWithFragment extends BaseBindingFragmentActivity<ActivityWithFBinding> {

    public static final String IDENTIFICATOR = "identificator";

    @Override
    protected void initFragment() {
        Fragment defaultFragment = FragmentFactory.createFragment(getIntent().getStringExtra(IDENTIFICATOR));
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, defaultFragment, "expand")
                .commit();
    }
}
