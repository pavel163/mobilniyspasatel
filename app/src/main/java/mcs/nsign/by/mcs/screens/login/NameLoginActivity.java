package mcs.nsign.by.mcs.screens.login;

import android.content.Intent;

import com.flurry.android.FlurryAgent;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.base.ActivityWithFragment;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.MainActivity;
import mcs.nsign.by.mcs.screens.login.login.FragmentLogin;

public class NameLoginActivity extends ActivityWithFragment {

    private FragmentLogin mFragmentLogin;

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void initFragment() {
        checkUserAlreadyAutorized();
    }

    private void checkUserAlreadyAutorized() {
        String verified = ((MCSApp) getApplicationContext()).getValue(Constants.VERIFIED);
        String accessToken = ((MCSApp) getApplicationContext()).getValue(Constants.TOKEN);
        if (verified.equals("1") || verified.equals("1.0") && accessToken != null && !accessToken.equals("")) {
            successAutorization();
        } else {
            setFragmentLogin();
        }
    }

    private void setFragmentLogin() {
        mFragmentLogin = new FragmentLogin();
        getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragment_container, mFragmentLogin, "expand")
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    public void successAutorization() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
