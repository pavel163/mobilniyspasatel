package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Bakht on 01.10.2016.
 */
public interface DirectApi {
    @GET("maps/api/directions/json")
    Call<Object> getDirect(
            @Query("origin") String position,
            @Query("destination") String destination,
            @Query("key") String key,
            @Query("mode") String mode);
}
