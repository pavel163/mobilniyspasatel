package mcs.nsign.by.mcs.utils.utils;

import android.support.v4.content.ContextCompat;
import android.widget.Button;

import mcs.nsign.by.mcs.R;

/**
 * Created by mac1 on 06.10.16.
 */

public class ViewUtils {

    public static void setClickableStateButton(Button button) {
        button.setEnabled(true);
        button.setAlpha(1);
        button.setBackgroundColor(ContextCompat.getColor(button.getContext(), android.R.color.holo_orange_dark));
    }

    public static void setUnClickableStateButton(Button button) {
        button.setEnabled(false);
        button.setAlpha(0.5f);
        button.setBackgroundColor(ContextCompat.getColor(button.getContext(), R.color.colorPrimaryDark));
    }
}
