package mcs.nsign.by.mcs.screens.login.code;

import mcs.nsign.by.mcs.utils.OnServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IModelCode extends OnServerCallback {

    void makeRequestCode(String name, String id);

    void repeatRequestCode(String userId, String phone);

    void setServerObserver(ServerObserver serverObserver);
}
