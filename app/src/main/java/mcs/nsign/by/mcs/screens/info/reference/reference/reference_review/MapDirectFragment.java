package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

/**
 * Created by Bakht on 01.10.2016.
 */
public class MapDirectFragment extends SupportMapFragment implements OnMapReadyCallback, IDirectView {

    private GoogleMap googleMap;
    private IDirectPresenter directPresenter;
    private double endLat;
    private double endLng;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getMapAsync(this);
        directPresenter = new DirectPresenter(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.latitude, Constants.longitude), 12));
        endLat = Double.parseDouble(getActivity().getIntent().getStringExtra(Constants.LATITUDE));
        endLng = Double.parseDouble(getActivity().getIntent().getStringExtra(Constants.LONGITUDE));

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Построение маршрута...");

        directPresenter.createDirect(Constants.DRIVING, endLat, endLng);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        this.googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void drawDirect(List<LatLng> points) {
        googleMap.clear();

        PolylineOptions line = new PolylineOptions();
        line.width(8f).color(Color.RED);
        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        for (int i = 0; i < points.size(); i++) {
            if (i == points.size() - 1) {
                MarkerOptions endMarkerOptions = new MarkerOptions()
                        .position(points.get(i));
                googleMap.addMarker(endMarkerOptions);
            }
            line.add(points.get(i));
            latLngBuilder.include(points.get(i));
        }
        googleMap.addPolyline(line);
        int size = getResources().getDisplayMetrics().widthPixels;
        LatLngBounds latLngBounds = latLngBuilder.build();
        CameraUpdate track = CameraUpdateFactory.newLatLngBounds(latLngBounds, size, size, 25);
        googleMap.animateCamera(track);
    }

    @Override
    public void error(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.hide();
    }

    @Override
    public boolean checkConnect() {
        return AppUtils.checkAccessNetwork(getActivity());
    }

    public void changeTypeTravel(String type) {
        directPresenter.createDirect(type, endLat, endLng);
    }
}
