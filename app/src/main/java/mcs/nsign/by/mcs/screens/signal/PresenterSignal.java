package mcs.nsign.by.mcs.screens.signal;

import android.content.Context;
import android.util.Log;

import java.util.Map;

import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by Ruslan on 22.11.2015.
 */
public class PresenterSignal implements IPresenterSignal, ServerObserver<Object, String>, ServerCallback<Map<String, Object>> {

    public IViewSignal view;
    public ModelSignal model;

    public PresenterSignal(IViewSignal view) {
        this.model = new ModelSignal();
        this.view = view;
        this.model.setServerObserver(this);
        this.model.setServerCallback(this);
    }

    @Override
    public void getSos(Context context, String token, String id, double lan, double lon, String test) {
        if (view.checkAccessNetwork()) {
            model.getSos(token, id, lan, lon, test);
        } else {
            view.showErrorConnection("1");
        }
    }

    @Override
    public void getSmsStatus(Context context, String token, String id) {
        if (view.checkAccessNetwork()) {
            model.getSmsStatus(token, id);
        } else {
            view.showErrorConnection("0");
        }
    }

    @Override
    public void sendMyCurrentLocation(String token, String sosId, String latitude, String longitude) {
        model.sendMyCurrentLocation(token, sosId, latitude, longitude);
    }

    @Override
    public void successExecuteObject(Object obj) {
        view.sendSosTrue(obj);
    }

    @Override
    public void successExecuteObject2(Object obj) {
        view.sendSmsStatusTrue(obj);
    }

    @Override
    public void failedExecute(String err) {
        if (err.equals("sms status error")) {
            return;
        }
        view.errorSend(err);
    }

    @Override
    public void success(Map<String, Object> response) {
        Log.d("asdas", response.toString());
    }

    @Override
    public void error(String message) {
        Log.d("asdas", message);
    }
}
