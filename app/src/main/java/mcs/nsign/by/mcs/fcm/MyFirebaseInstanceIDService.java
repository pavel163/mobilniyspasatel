package mcs.nsign.by.mcs.fcm;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.SettingsService;

/**
 * Created by mac1 on 19.10.16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    public static final String NEW_TOKEN = "FIREBASE_TOKEN";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        Log.d("fcm_token", token);
        SettingsService.set(getApplicationContext(), Constants.FIREBASE_NOTIFICATION_TOKEN, token);
        Intent intent = new Intent(NEW_TOKEN);
        getApplicationContext().sendBroadcast(intent);
    }
}
