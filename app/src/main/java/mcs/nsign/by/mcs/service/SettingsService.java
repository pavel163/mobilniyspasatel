package mcs.nsign.by.mcs.service;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Bakht on 18.03.2016.
 */
public class SettingsService {

    private static final String SETTINGS = "settings";
    private static Context ctx;

    public static void init(Context context){
        ctx = context;
    }

    public static void set(Context context, String key, String value){
        SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String get(Context context, String key){
        if (context != null){
            SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
            return preferences.getString(key, "");
        } else {
            return "";
        }
    }

    public static String getDataTime(Context context, String key){
        if (context != null){
            SharedPreferences preferences = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
            return preferences.getString(key, "2015-09-12 13:59:06");
        } else {
            return "2015-09-12 13:59:06";
        }
    }
}
