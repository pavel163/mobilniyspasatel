package mcs.nsign.by.mcs.screens.info.firs_help;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IModelFirstHelp {

    List<Map<String, Object>> getSearchList();

    List<Map<String, Object>> getExpandList();
    Map<String, Object> getSearchItem(int position);

    Map<String, Object> getItem(int position);

    void loadNewSearchList(String text, String type);
}

