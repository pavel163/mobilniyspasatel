package mcs.nsign.by.mcs.screens.info;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IModelInfo {

    List<Map<String, Object>> getDefualtInfo();

    List<Map<String, Object>> getMenuItems();
    void getInfo(String text);
    Map<String, Object> getInfo(int position);
}
