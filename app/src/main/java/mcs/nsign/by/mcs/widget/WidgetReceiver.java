package mcs.nsign.by.mcs.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.WidgetUtils;

public class WidgetReceiver extends BroadcastReceiver {

    public static final String WIDGET_RESET = "widget_reset";
    public final static String ACTION_RESET_SOS = "mchs.send.sos.ACTION_RESET";


    public WidgetReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        SettingsService.set(context, WIDGET_RESET, "0");
        if (SettingsService.get(context, SosWidget.WIDGET_BLOCK).equals("1")) {
            return;
        }

        WidgetUtils.sendResetWidget(context, intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, 0));
    }
}
