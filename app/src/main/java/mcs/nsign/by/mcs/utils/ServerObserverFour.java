package mcs.nsign.by.mcs.utils;

public interface ServerObserverFour<Object, String> {

    void successExecuteObject(Object obj);

    void successExecuteObject_Two(Object obj);

    void successExecuteObject_Three(Object obj);

    void successExecuteObject_Four(Object obj);

    void failedExecute(String err);

    void failedExecute_Two(String err);

    void failedExecute_Three(String err);

    void failedExecute_Four(String err);
}