package mcs.nsign.by.mcs.screens.login.code;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.flurry.android.FlurryAgent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.yandex.metrica.YandexMetrica;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FCodeBinding;
import mcs.nsign.by.mcs.screens.MainActivity;
import mcs.nsign.by.mcs.screens.feedback.FeedBackActivity;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;


public class FragmentCode extends BaseBindingFragment<FCodeBinding> implements IViewCode<Map<String, Object>> {

    private IPresenterCode presenter;

    @Override
    protected void afterBinding() {
        super.afterBinding();
        initToolbar();
        initPassFields();
        fragmentBinder.repeat.setOnClickListener(v -> {
            presenter.repeatRequestCode(SettingsService.get(getActivity(), Constants.ID), ((MCSApp) getActivity().getApplicationContext()).getValue(Constants.PHONE));
            startTimerTask();
            fragmentBinder.repeat.setAlpha(0.7f);
            fragmentBinder.repeat.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        });
        presenter = new PresenterCode(this);
        startTimerTask();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Регистрация");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initPassFields() {
        fragmentBinder.pass1.requestFocus();
        fragmentBinder.pass1.setCursorVisible(true);
        AppUtils.showkeyBoard(getActivity());

        fragmentBinder.pass1.addTextChangedListener(getTextWatcher(() -> checkFillFields(), () -> {
            if (changeFocus(fragmentBinder.pass1, fragmentBinder.pass2)) {
                hideError();
            }
        }));

        fragmentBinder.pass2.addTextChangedListener(getTextWatcher(() -> checkFillFields(), () -> changeFocus(fragmentBinder.pass2, fragmentBinder.pass3)));
        fragmentBinder.pass3.addTextChangedListener(getTextWatcher(() -> checkFillFields(), () -> changeFocus(fragmentBinder.pass3, fragmentBinder.pass4)));
        fragmentBinder.pass4.addTextChangedListener(getTextWatcher(() -> checkFillFields(), () -> {
        }));

        fragmentBinder.callback.setOnClickListener(v -> startActivity(new Intent(getActivity(), FeedBackActivity.class)));
    }

    private boolean changeFocus(EditText editText1, EditText editText2) {
        if (editText1.getText().length() == 1) {
            editText1.clearFocus();
            editText2.requestFocus();
            editText2.setCursorVisible(true);
            return true;
        } else {
            return false;
        }
    }

    private void checkCode() {
        hideError();
        presenter.tryToEnter(getCode(), SettingsService.get(getActivity(), Constants.ID));
    }

    private String getCode() {
        return fragmentBinder.pass1.getText().toString() + fragmentBinder.pass2.getText().toString() + fragmentBinder.pass3.getText().toString() + fragmentBinder.pass4.getText().toString();
    }

    private void clearFields() {
        fragmentBinder.pass1.setText(null);
        fragmentBinder.pass2.setText(null);
        fragmentBinder.pass3.setText(null);
        fragmentBinder.pass4.setText(null);
    }

    private void hideError() {
        fragmentBinder.error.setVisibility(View.INVISIBLE);
        fragmentBinder.repeat.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        fragmentBinder.errorImg.setVisibility(View.INVISIBLE);
    }

    private TextWatcher getTextWatcher(Runnable afterTextChangeRunnable, Runnable textChangeRunnable) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                textChangeRunnable.run();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                afterTextChangeRunnable.run();
            }
        };
    }

    private void checkFillFields() {
        if (fragmentBinder.pass1.getText().toString().length() == 1 && fragmentBinder.pass2.getText().toString().length() == 1 &&
                fragmentBinder.pass3.getText().toString().length() == 1 && fragmentBinder.pass4.getText().toString().length() == 1) {
            checkCode();
        }
    }

    @Override
    public void showErrorConnection(String error) {
        new AlertDialog.Builder(getActivity())
                .setTitle(error)
                .setPositiveButton(getResources().getString(R.string.OK),
                        (dialog, which) -> {
                            clearFields();
                            fragmentBinder.pass4.clearFocus();
                            fragmentBinder.pass1.requestFocus();
                            fragmentBinder.pass1.setCursorVisible(true);
                            dialog.dismiss();
                        })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .show();
    }

    public void handleResponse(Map<String, Object> obj) {
        AppUtils.hideKeyBoard(getActivity());
        presenter.handleVerifiedResponse((int) ((double) ((Map) obj).get(Constants.VERIFIED)));
        ((MCSApp) getActivity().getApplicationContext()).saveValue(Constants.TOKEN, ((Map) obj).get(Constants.TOKEN).toString());
        ((MCSApp) getActivity().getApplicationContext()).saveValue(Constants.VERIFIED, ((Map) obj).get(Constants.VERIFIED).toString());
    }

    public void successAutorization() {
        ((MCSApp) getActivity().getApplicationContext()).saveValue(Constants.VERIFIED, "1");
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void showTimerProgress(int time) {
        fragmentBinder.time.setText("Новый запрос доступен через " + (60 - time) + " секунд");
    }

    @Override
    public void startTimerTask() {
        fragmentBinder.repeat.setEnabled(false);
        presenter.startTimer();
    }

    @Override
    public void completeTimer() {
        fragmentBinder.repeat.setEnabled(true);
        fragmentBinder.repeat.setTextColor(Color.RED);
        fragmentBinder.repeat.setAlpha(1);
    }

    @Override
    public int getLayout() {
        return R.layout.f_code;
    }

    @Override
    public void success(Map<String, Object> response) {
        AppUtils.sendCustomEventStatistic(FirebaseAnalytics.Param.SIGN_UP_METHOD, "Успешная регистрация", FirebaseAnalytics.Event.SIGN_UP);
        FlurryAgent.logEvent("Успешная регистрация");
        YandexMetrica.reportEvent("Успешная регистрация");
        handleResponse(response);
    }

    @Override
    public void error(String message) {
        fragmentBinder.error.setVisibility(View.VISIBLE);
        fragmentBinder.repeat.setTextColor(Color.RED);
        fragmentBinder.errorImg.setVisibility(View.VISIBLE);
        clearFields();
        fragmentBinder.pass4.clearFocus();
        fragmentBinder.pass1.requestFocus();
        fragmentBinder.pass1.setCursorVisible(true);
    }

    @Override
    public boolean checkAccessNetwork() {
        return AppUtils.checkAccessNetwork(getActivity());
    }
}
