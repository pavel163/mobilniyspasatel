package mcs.nsign.by.mcs.screens.info.reference;

import android.content.Context;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.screens.info.adapter.SearchListAdapter;
import mcs.nsign.by.mcs.screens.info.adapter.holder.SearchViewHolder;

/**
 * Created by mac1 on 04.10.16.
 */

public class SearchReferenceAdapter extends SearchListAdapter {

    public SearchReferenceAdapter(Context context, List<Map<String, Object>> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(SearchViewHolder holder, int position) {
        Map<String, Object> map = list.get(position);
        holder.bind(map);
    }

}
