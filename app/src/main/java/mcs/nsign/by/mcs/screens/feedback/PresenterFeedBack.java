package mcs.nsign.by.mcs.screens.feedback;

import android.content.Context;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by mac1 on 25.08.16.
 */
public class PresenterFeedBack implements IPresenterFeedBack, ServerCallback {

    public IViewFeedBack view;
    public IModelFeedBack model;

    public PresenterFeedBack(IViewFeedBack view) {
        this.model = new ModelFeedBack();
        this.view = view;
        this.model.setServerCallback(this);
    }

    @Override
    public void sendMessage(Context context, String id, String message) {
        if (view.checkAccessNetwork()) {
            model.sendMessage(id, message);
        } else {
            view.showErrorConnection(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    @Override
    public void success(Object response) {
        view.error("");
    }

    @Override
    public void error(String message) {
        view.error(message);
    }
}
