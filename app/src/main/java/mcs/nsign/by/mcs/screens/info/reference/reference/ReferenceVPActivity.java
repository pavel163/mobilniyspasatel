package mcs.nsign.by.mcs.screens.info.reference.reference;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.ebr163.core.adapter.TabsAdapter;
import com.ebr163.core.newApi.base.activity.BaseBindingActivity;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.AReferenceBinding;

public class ReferenceVPActivity extends BaseBindingActivity<AReferenceBinding> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityBinder.appBar.toolbar.setTitle(getIntent().getStringExtra(Constants.NAME));
        initTabs();
    }

    private void initTabs() {
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager());
        tabsAdapter.addFragment(new ReferencesFragment(), "СПИСКОМ");
        tabsAdapter.addFragment(new MapReferenceFragment(), "НА КАРТЕ");
        activityBinder.viewPager.setAdapter(tabsAdapter);
        activityBinder.appBar.tabs.setupWithViewPager(activityBinder.viewPager);
    }

    @Override
    public int getLayout() {
        return R.layout.a_reference;
    }

}
