package mcs.nsign.by.mcs.screens.login.login;

import mcs.nsign.by.mcs.model.RegisterModel;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */

public class ModelLogin implements IModelLogin{

    private RetrofitClient client;

    public ModelLogin() {
        client = new RetrofitClient();
    }

    @Override
    public void makeRequest(RegisterModel registerModel) {
        client.postPhone(registerModel);
    }

    @Override
    public void setServerCallback(ServerCallback serverCallback) {
        client.setServerCallback(serverCallback);
    }
}
