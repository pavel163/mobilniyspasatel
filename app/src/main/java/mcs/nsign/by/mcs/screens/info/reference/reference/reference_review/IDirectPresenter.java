package mcs.nsign.by.mcs.screens.info.reference.reference.reference_review;

/**
 * Created by Bakht on 01.10.2016.
 */
public interface IDirectPresenter {
    void createDirect(String type, double endLatitude, double endLongitude);
}
