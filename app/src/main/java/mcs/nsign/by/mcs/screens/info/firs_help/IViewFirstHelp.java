package mcs.nsign.by.mcs.screens.info.firs_help;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.screens.info.IViewInfo;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IViewFirstHelp extends IViewInfo {
    void initExpandList(List<Map<String, Object>> items);

    void showEmptyMessage(String message);

    void hideEmptyMessage();
}
