package mcs.nsign.by.mcs.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mac1 on 21.07.16.
 */
public class ContactUtil {

    public static List<String> getContactNumber(Context context, Uri uriContact) {
        List<String> result = new ArrayList<>();

        String contactNumber = null;
        String contactID = null;
        Cursor cursorID = context.getContentResolver().query(uriContact, new String[]{ContactsContract.Contacts._ID}, null, null, null);

        if (cursorID.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }
        cursorID.close();

        Cursor cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            result.add(contactNumber);
            while (cursorPhone.moveToNext()) {
                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                result.add(contactNumber);
            }
        }
        cursorPhone.close();

        for (int i = 0; i < result.size(); i++ ) {
            if (result.get(i) == null || result.get(i).equals("")) {
                result.set(i, "");
            }

            result.set(i, result.get(i).replaceAll(" ", ""));
            result.set(i, result.get(i).replaceAll("\\(", ""));
            result.set(i, result.get(i).replaceAll("\\)", ""));
            result.set(i, result.get(i).replaceAll("-", ""));

            if (result.get(i).startsWith("8")) {
                result.set(i,result.get(i).replaceFirst("8", "+7"));
            }
        }

        return result;
    }

    public static String getContactName(Context context, Uri uriContact) {
        String contactName = null;
        Cursor cursor = context.getContentResolver().query(uriContact, null, null, null, null);
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }
        cursor.close();
        return contactName;
    }
}
