package mcs.nsign.by.mcs;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.flurry.android.FlurryAgent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.yandex.metrica.YandexMetrica;

import java.io.IOException;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.service.databases.DBHelper;


public class MCSApp extends Application {

    public SharedPreferences sharedPreferences;
    public static DBHelper dbHelper;
    public static FirebaseAnalytics firebaseAnalytics;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MCSApp.this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        YandexMetrica.activate(getApplicationContext(), "1fd08020-b340-4a3c-8ed3-6f4e4323a0c0");
        YandexMetrica.enableActivityAutoTracking(this);

        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .build(this, "ND68ZJVJ2697X5VTWVHP");

        initDB(this);
        if (SettingsService.get(this, "last_update1").equals("")) {
            dbHelper.clearDB();
            SettingsService.set(this, "last_update1", "1");
            SettingsService.set(getApplicationContext(), Constants.LOAD_REFERENCE, "2015-09-12 13:59:06");
            SettingsService.set(getApplicationContext(), Constants.LOAD_REFERENCE_TYPE, "2015-09-12 13:59:06");
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_CONTENT, "2015-09-12 13:59:06");
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_ITEMS, "2015-09-12 13:59:06");
            SettingsService.set(getApplicationContext(), Constants.LOAD_HELP_TYPE, "2015-09-12 13:59:06");
        }
    }

    private static void initDB(Application application) {
        dbHelper = new DBHelper(application);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dbHelper.openDataBase();
    }

    public void saveValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getValue(String key) {
        return sharedPreferences.getString(key, "");
    }
}
