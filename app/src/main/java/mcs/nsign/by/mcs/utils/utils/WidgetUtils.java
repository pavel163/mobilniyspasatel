package mcs.nsign.by.mcs.utils.utils;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.widget.SosService;
import mcs.nsign.by.mcs.widget.SosWidget;
import mcs.nsign.by.mcs.widget.WidgetReceiver;

/**
 * Created by mac1 on 10.10.16.
 */

public class WidgetUtils {

    public static void sendResetWidget(Context context, int id) {
        Intent resenIntent = new Intent(context, SosWidget.class);
        resenIntent.setAction(WidgetReceiver.ACTION_RESET_SOS);
        resenIntent.putExtra(Constants.NAME, "Нажмите быстро \n" +
                "SOS\n" +
                "3 раза");
        resenIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
        context.sendBroadcast(resenIntent);
    }

    public static void sendMessageWidget(Context context, int id, String message) {
        Intent intent = new Intent(context, SosWidget.class);
        intent.setAction(SosService.ACTION_CHANGE_SOS);
        intent.putExtra(Constants.NAME, message);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
        context.sendBroadcast(intent);
    }

    public static void sendSuccesSignal(Context context, String message) {
        Intent intent = new Intent(SosService.ACTION_SUCCESS_SOS);
        intent.putExtra(Constants.MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static void sendSuccesStatus(Context context, String message) {
        Intent intent = new Intent(SosService.ACTION_SUCCESS_STATUS);
        intent.putExtra(Constants.MESSAGE, message);
        context.sendBroadcast(intent);
    }

    public static void sendErrorWidget(Context context, String message) {
        Intent intent = new Intent(SosService.ACTION_ERROR_STATUS);
        intent.putExtra(Constants.MESSAGE, message);
        context.sendBroadcast(intent);
    }
}
