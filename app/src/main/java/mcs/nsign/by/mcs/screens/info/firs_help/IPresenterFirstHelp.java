package mcs.nsign.by.mcs.screens.info.firs_help;

import mcs.nsign.by.mcs.screens.info.IPresenterInfo;

/**
 * Created by Bakht on 10.09.2016.
 */
public interface IPresenterFirstHelp extends IPresenterInfo {
    void initExpandList();

    void unSubscribe();
}
