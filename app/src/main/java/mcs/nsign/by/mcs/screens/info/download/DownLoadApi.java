package mcs.nsign.by.mcs.screens.info.download;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Created by Bakht on 02.10.2016.
 */

public interface DownLoadApi {

    @Streaming
    @GET("guide/types/index")
    Call<ResponseBody> loadReferenceType(@Query("access-token") String accessToken, @Query("db_date_obj_type") String data, @Query("start") String start);

    @Streaming
    @GET("guide/guides/index")
    Call<ResponseBody> loadReference(@Query("access-token") String accessToken, @Query("db_date_obj") String data, @Query("start") String start);

    @Streaming
    @GET("guide/help-types/index")
    Call<ResponseBody> loadHelpType(@Query("access-token") String accessToken, @Query("db_date_help_type") String data, @Query("start") String start);

    @Streaming
    @GET("guide/help-menus/index")
    Call<ResponseBody> loadHelpItems(@Query("access-token") String accessToken, @Query("db_date_help_menu") String data, @Query("start") String start);

    @Streaming
    @GET("guide/help-contents/index")
    Call<ResponseBody> loadHelpContent(@Query("access-token") String accessToken, @Query("db_date_help_content") String data, @Query("start") String start);
}
