package mcs.nsign.by.mcs.screens.info.reference.reference;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.screens.info.adapter.ReferencesAdapter;
import mcs.nsign.by.mcs.screens.info.adapter.holder.SearchViewHolder;
import mcs.nsign.by.mcs.screens.info.reference.PresenterReference;
import mcs.nsign.by.mcs.screens.info.reference.ReferenceCategoryFragment;
import mcs.nsign.by.mcs.ui.recyclerview.DividerItemDecoration;

/**
 * Created by mac1 on 14.09.16.
 */
public class ReferencesFragment extends ReferenceCategoryFragment {

    @Override
    protected void afterBinding() {
        modelReference = new ModelReferences(getActivity().getIntent().getStringExtra(Constants.ID));
        presenterReference = new PresenterReference(this, modelReference);

        presenterReference.initExpandList();
        presenterReference.initSearchList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterReference.unSubscribe();
    }

    @Override
    public void initReferenceAdapter(List<Map<String, Object>> data) {
        super.initReferenceAdapter(data);
        fragmentBinder.progress.setVisibility(View.GONE);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.referenceList.addItemDecoration(new DividerItemDecoration(divider));
    }

    @Override
    public void initSearchList(List<Map<String, Object>> data) {
        super.initSearchList(data);
        Drawable divider = ContextCompat.getDrawable(getActivity(), R.drawable.divider_layer);
        fragmentBinder.searchList.addItemDecoration(new DividerItemDecoration(divider));
    }

    @Override
    protected RecyclerView.Adapter<SearchViewHolder> getReferenceAdapter(List<Map<String, Object>> data) {
        return new ReferencesAdapter(getActivity(), data);
    }

    @Override
    protected RecyclerView.Adapter<SearchViewHolder> getSearchAdapter(List<Map<String, Object>> data) {
        return new ReferencesAdapter(getActivity(), data);
    }

    @Override
    protected void clickItemReference(int i, List<Map<String, Object>> data) {
        clickItemSearch(i, data);
    }
}
