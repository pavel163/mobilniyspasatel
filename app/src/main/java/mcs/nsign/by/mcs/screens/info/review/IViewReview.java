package mcs.nsign.by.mcs.screens.info.review;

import java.util.List;
import java.util.Map;

/**
 * Created by Bakht on 11.09.2016.
 */
public interface IViewReview {
    void initList(List<Map<String, Object>> list);
}
