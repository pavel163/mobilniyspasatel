package mcs.nsign.by.mcs.screens.info.reference.reference.cluster;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Bakht on 30.09.2016.
 */
public class MapClusterItem implements ClusterItem {

    private final LatLng position;
    private final Bitmap image;

    public MapClusterItem(double lat, double lng, Bitmap image) {
        position = new LatLng(lat, lng);
        this.image = image;
    }

    public MapClusterItem(LatLng position, Bitmap image) {
        this.position = position;
        this.image = image;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    public Bitmap getImage() {
        return image;
    }
}
