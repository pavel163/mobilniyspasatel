package mcs.nsign.by.mcs.screens.profile;

import mcs.nsign.by.mcs.model.Profile;
import mcs.nsign.by.mcs.utils.ServerObserver;

/**
 * Created by mac1 on 18.08.16.
 */
public interface IModelProfile {
    void changeProfile(String accesstoken, String id, Profile profile);
    void getProfile(String accesstoken, String id);

    void setServerObserver(ServerObserver serverObserver);
}
