package mcs.nsign.by.mcs.screens.info.reference;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.screens.info.IViewInfo;

/**
 * Created by Bakht on 14.09.2016.
 */
public interface IViewReference extends IViewInfo {
    void initReferenceAdapter(List<Map<String, Object>> data);
}
