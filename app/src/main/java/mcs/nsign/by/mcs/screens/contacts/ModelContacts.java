package mcs.nsign.by.mcs.screens.contacts;

import android.util.Log;

import mcs.nsign.by.mcs.service.http.RetrofitClient;

/**
 * Created by Ruslan on 22.11.2015.
 */

public class ModelContacts implements IModelContacts {

    public RetrofitClient client;

    public ModelContacts(RetrofitClient client){
        this.client = client;
    }

    public void postContact(String user_id, String name, String surname, String phone, String accesstoken){
        client.postContact(user_id, name, surname, phone, accesstoken);
    }

    @Override
    public void getAllContacts(String accesstoken, String id) {
        Log.d("rklogs", "Запрос получения всех контактов");
        client.getContacts(accesstoken, id);
    }

    @Override
    public void changeContact(String id, String name, String surname, String phone, String accesstoken) {
        client.putChangeContact(id, name, surname, phone, accesstoken);
    }

    @Override
    public void deleteContact(String id, String accesstoken) {
        client.deleteContact(id, accesstoken);
    }
}
