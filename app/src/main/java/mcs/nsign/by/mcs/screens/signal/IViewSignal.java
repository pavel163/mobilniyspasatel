package mcs.nsign.by.mcs.screens.signal;

import mcs.nsign.by.mcs.utils.NetworkCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IViewSignal extends NetworkCallback {

     void showErrorConnection(String error);

     void sendSosTrue(Object obj);

     void sendSmsStatusTrue(Object obj);

     void errorSend(String message);
}
