package mcs.nsign.by.mcs.screens.login.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.ebr163.core.newApi.base.fragment.BaseBindingFragment;
import com.google.gson.internal.LinkedTreeMap;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.databinding.FLoginBinding;
import mcs.nsign.by.mcs.fcm.MyFirebaseInstanceIDService;
import mcs.nsign.by.mcs.model.RegisterModel;
import mcs.nsign.by.mcs.screens.MainActivity;
import mcs.nsign.by.mcs.screens.login.code.FragmentCode;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;
import mcs.nsign.by.mcs.utils.utils.ViewUtils;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import rx.Observable;
import rx.subjects.PublishSubject;

@RuntimePermissions
public class FragmentLogin extends BaseBindingFragment<FLoginBinding> implements IViewLogin<Map<String, Object>> {

    private PresenterLogin presenter;
    private String imei = "imei";
    private BroadcastReceiver broadcastReceiver;
    PublishSubject<String> subject = PublishSubject.create();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                subject.onNext(SettingsService.get(getContext(), Constants.FIREBASE_NOTIFICATION_TOKEN));
            }
        };

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MyFirebaseInstanceIDService.NEW_TOKEN);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void afterBinding() {
        super.afterBinding();
        initToolbar();
        initPhoneEdt();
        fragmentBinder.btnLogin.setOnClickListener(v1 -> register());
        FragmentLoginPermissionsDispatcher.getImeiWithCheck(this);
        presenter = new PresenterLogin(getActivity(), this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Регистрация");
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void initPhoneEdt() {
        AppUtils.getPhoneMask().installOn(fragmentBinder.etPhone.getEditText());

        Observable<Boolean> shouldEnableButtons = Observable.combineLatest(
                RxTextView.afterTextChangeEvents(fragmentBinder.etPhone.getEditText()),
                RxTextView.afterTextChangeEvents(fragmentBinder.etName.getEditText()),
                RxTextView.afterTextChangeEvents(fragmentBinder.etSurname.getEditText()),
                (phone, name, surname) -> {
                    if (phone.editable().length() != 0 && name.editable().length() != 0 && surname.editable().length() != 0) {
                        return true;
                    } else {
                        return false;
                    }
                });
        shouldEnableButtons.subscribe(aBoolean -> {
            if (aBoolean) {
                ViewUtils.setClickableStateButton(fragmentBinder.btnLogin);
            } else {
                ViewUtils.setUnClickableStateButton(fragmentBinder.btnLogin);
            }
        });
    }

    private void register() {
        ViewUtils.setUnClickableStateButton(fragmentBinder.btnLogin);
        fragmentBinder.etPhone.setErrorEnabled(false);
        AppUtils.hideKeyBoard(getActivity());
        RegisterModel registerModel = new RegisterModel();
        registerModel.name = fragmentBinder.etName.getEditText().getText().toString();
        registerModel.surname = fragmentBinder.etSurname.getEditText().getText().toString();
        registerModel.imei = imei;
        registerModel.phone = fragmentBinder.etPhone.getEditText().getText().toString();
        //   registerModel.pushToken = SettingsService.get(getActivity(), Constants.FIREBASE_NOTIFICATION_TOKEN);
        presenter.tryToEnter(registerModel);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        FragmentLoginPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @SuppressLint("HardwareIds")
    @NeedsPermission(Manifest.permission.READ_PHONE_STATE)
    protected void getImei() {
        TelephonyManager mngr = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        imei = mngr.getDeviceId();
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentBinder.etPhone.getEditText().setText(SettingsService.get(getActivity(), Constants.PHONE));
        fragmentBinder.etName.getEditText().setText(SettingsService.get(getActivity(), Constants.NAME));
        fragmentBinder.etSurname.getEditText().setText(SettingsService.get(getActivity(), Constants.SURNAME));
    }

    @Override
    public void showErrorName(String error) {
        ViewUtils.setClickableStateButton(fragmentBinder.btnLogin);
        fragmentBinder.etPhone.setError(error);
        fragmentBinder.etPhone.setErrorEnabled(true);
    }

    @Override
    public void showErrorConnection(String error) {
        ViewUtils.setClickableStateButton(fragmentBinder.btnLogin);
        AppUtils.showErrorConnectDialog(getActivity(), error);
    }

    public void setFragmentCode() {
        FragmentCode mFragmentCode = new FragmentCode();
        getActivity().getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.fragment_container, mFragmentCode, "code")
                .commit();
    }

    public void handleResponse(Object obj) {
        if (obj instanceof LinkedTreeMap) {
            LinkedTreeMap mapResponse = (LinkedTreeMap) obj;
            String phone = (String) mapResponse.get("phone");
            String email = (String) mapResponse.get("email");
            double id = (double) mapResponse.get("id");
            double verified = (double) mapResponse.get("verified");

            SettingsService.set(getActivity(), Constants.NAME, fragmentBinder.etName.getEditText().getText().toString());
            SettingsService.set(getActivity(), Constants.SURNAME, fragmentBinder.etSurname.getEditText().getText().toString());
            SettingsService.set(getActivity(), Constants.PHONE, fragmentBinder.etPhone.getEditText().getText().toString());
            SettingsService.set(getActivity(), Constants.EMAIL, email);

            if (verified == 1 && (((MCSApp) getActivity().getApplicationContext()).getValue("access_token").length() > 5)) {
                ((MCSApp) getActivity().getApplicationContext()).saveValue("phone", phone);
                ((MCSApp) getActivity().getApplicationContext()).saveValue("verified", String.valueOf((int) verified));
                ((MCSApp) getActivity().getApplicationContext()).saveValue("id", String.valueOf((int) id));
                SettingsService.set(getActivity(), Constants.ID, String.valueOf((int) id));
                successAutorization();
            } else {
                setFragmentCode();
                ((MCSApp) getActivity().getApplicationContext()).saveValue("id", String.valueOf((int) id));
                ((MCSApp) getActivity().getApplicationContext()).saveValue("phone", phone);
                SettingsService.set(getActivity(), Constants.ID, String.valueOf((int) id));
            }
        } else {
            Log.e("Login", "некорректный ответ от сервера");
        }
    }

    public void successAutorization() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public int getLayout() {
        return R.layout.f_login;
    }

    @Override
    public void success(Map<String, Object> response) {
        SettingsService.set(getActivity(), Constants.START, "1");
        handleResponse(response);
    }

    @Override
    public void error(String message) {
        ViewUtils.setClickableStateButton(fragmentBinder.btnLogin);
    }

    @Override
    public boolean checkAccessNetwork() {
        return AppUtils.checkAccessNetwork(getActivity());
    }
}
