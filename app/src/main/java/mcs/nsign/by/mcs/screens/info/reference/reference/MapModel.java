package mcs.nsign.by.mcs.screens.info.reference.reference;

import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 18.09.2016.
 */
public class MapModel implements IModelMap {

    @Override
    public List<Map<String, Object>> getMarkers(String parentId) {
        List<Map<String, Object>> result = MCSApp.dbHelper.getInstitutions(parentId);
        ;

        for (Map<String, Object> map : result) {
            map.put(Constants.PARENT_ICON, MCSApp.dbHelper.getReferenceIcon(map.get(Constants.REFERENCE).toString()));
        }

        return result;
    }
}
