package mcs.nsign.by.mcs.screens.contacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.gson.internal.LinkedTreeMap;
import com.yandex.metrica.YandexMetrica;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.MCSApp;
import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.adapters.ContactsAdapter;
import mcs.nsign.by.mcs.databinding.FragmentContactsBinding;
import mcs.nsign.by.mcs.model.ContactsObject;
import mcs.nsign.by.mcs.service.http.RetrofitClient;
import mcs.nsign.by.mcs.utils.ServerObserverFour;
import mcs.nsign.by.mcs.utils.utils.AppUtils;


public class FragmentContacts extends Fragment implements IViewContacts, ServerObserverFour<Object, String>, EventCallBack {

    private PresenterContacts presenter;
    private ModelContacts model;
    private FragmentContactsBinding binding;
    private ContactsAdapter adapter;
    private String accesstoken, id;
    private ProgressDialog progressDialog;
    private List<ContactsObject> contListOfObject;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contacts, container, false);
        RetrofitClient client = new RetrofitClient();
        client.setServerObserverFour(this);
        AppUtils.sendScreenNameStatistic("contacts_screen", "Экстренные контакты");
        YandexMetrica.reportEvent("Экстренные контакты");

        model = new ModelContacts(client);
        presenter = new PresenterContacts(getActivity(), this, model);

        accesstoken = ((MCSApp) getActivity().getApplicationContext()).getValue("access_token");
        id = (((MCSApp) getActivity().getApplicationContext()).getValue("id"));

        binding.fullList.fab.setOnClickListener(v -> {
            if (contListOfObject.size() <= 2){
                startActivityForResult(getAddContactIntent(), 122);
            }else {
                Toast.makeText(getActivity().getApplicationContext(), "Нельзя добавить больше 3 контактов", Toast.LENGTH_SHORT).show();
            }
        });
        binding.emptyList.addButton.setOnClickListener(v -> startActivityForResult(getAddContactIntent(), 123));

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Загрузка...");
        progressDialog.show();

        presenter.getAllContacts(accesstoken, id);
        return binding.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123 && data != null){
            binding.emptyList.emptyList.setVisibility(View.GONE);
            binding.fullList.fullList.setVisibility(View.VISIBLE);
            presenter.getAllContacts(accesstoken, id);
        } else if (requestCode == 122 && data != null){
            presenter.getAllContacts(accesstoken, id);
        }
    }

    private Intent getAddContactIntent(){
        Intent intent = new Intent(getActivity(), AddContactActivity.class);
        if (contListOfObject != null) {
            intent.putExtra("countContacts", contListOfObject.size());
        } else {
            intent.putExtra("countContacts", 0);
        }
        return intent;
    }

    @Override
    public void showToast(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        FlurryAgent.logEvent("Экстренные контакты");
    }

    @Override
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(getActivity());
    }

    @Override
    public void showErrorConnection(String error) {
        AppUtils.showErrorConnectDialog(getActivity(), error);
    }

    @Override
    public void showProgressDialog() {
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    private void showDeleteDialog(String id) {
        new AlertDialog.Builder(getActivity())
                .setTitle("Подтвердите удаление контакта")
                .setPositiveButton(getResources().getString(R.string.OK),
                        (dialog, which) -> {
                            presenter.deleteContact(id, accesstoken, (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE));
                        })
                .setNegativeButton("Отмена", (dialog, which) -> {})
                .setCancelable(false)
                .show();
    }

    @Override
    public void successExecuteObject(Object obj) {
        LinkedTreeMap mapResponse = (LinkedTreeMap) obj;
        showToast("Контакт успешно добавлен: " + mapResponse.get("name") + ", " + mapResponse.get("phone"));
        presenter.getAllContacts(accesstoken, id);
    }

    @Override
    public void successExecuteObject_Two(Object obj) {
        if (obj instanceof Map) {
            LinkedTreeMap mapResponse = (LinkedTreeMap) obj;
            fillListContacts(mapResponse);
        } else {
            showErrorConnection("Нет подключения к интернет");
        }
    }

    @Override
    public void successExecuteObject_Three(Object obj) {
        showToast("Контакт успешно изменен");
        presenter.getAllContacts(accesstoken, id);
    }

    @Override
    public void successExecuteObject_Four(Object obj) {
        showToast("Контакт успешно удален");
        for (int i = 0; i <= contListOfObject.size(); i++) {
            // TODO: 17.08.2016 костыль. Подпер, но надо убрать
            if (contListOfObject.get(i).getId().equals(obj.toString()+".0")){
                contListOfObject.remove(i);
                break;
            }
        }
        adapter.notifyDataSetChanged();
        if (contListOfObject.size() == 0){
            binding.emptyList.emptyList.setVisibility(View.VISIBLE);
            binding.fullList.fullList.setVisibility(View.GONE);
        } else {
            binding.fullList.fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void failedExecute(String err) {
        showToast(err);
    }

    @Override
    public void failedExecute_Two(String err) {
        showToast("Проверьте соединение с интернетом");
    }

    @Override
    public void failedExecute_Three(String err) {
        showToast("Не удалось изменить контакт");
    }

    @Override
    public void failedExecute_Four(String err) {
        showToast("Не удалось удалить контакт");
    }

    public void fillListContacts(LinkedTreeMap map) {
        progressDialog.hide();
        if (map != null) {
            List<Map<String, Object>> contacts = (List<Map<String, Object>>) map.get("contacts");
            if (contacts == null || contacts.size() == 0) {
                binding.emptyList.emptyList.setVisibility(View.VISIBLE);
            } else {
                binding.fullList.fullList.setVisibility(View.VISIBLE);
            }

            contListOfObject = new ArrayList<>();
            for (Map<String, Object> contact : contacts) {
                ContactsObject contactsObject = new ContactsObject(
                        contact.get("id").toString(),
                        contact.get("user_id").toString(),
                        contact.get("name").toString(),
                        contact.get("phone").toString(),
                        contact.get("created_by").toString(),
                        contact.get("last_name").toString()
                );
                contListOfObject.add(contactsObject);
            }
            adapter = new ContactsAdapter(getActivity(), contListOfObject, this);
            binding.fullList.linList.setAdapter(adapter);
            if (contListOfObject.size() == 3){
                binding.fullList.fab.setVisibility(View.GONE);
            } else {
                binding.fullList.fab.setVisibility(View.VISIBLE);
            }
        } else {
            binding.emptyList.emptyList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void delete(ContactsObject contactsObject) {
        String id_int = String.valueOf((int) Double.parseDouble(contactsObject.getId()));
        showDeleteDialog(id_int);
    }

    @Override
    public void change(ContactsObject contactsObject) {
        Intent intent = new Intent(getContext(), AddContactActivity.class);
        intent.putExtra("contact", contactsObject);
        startActivityForResult(intent, 122);
    }
}
