package mcs.nsign.by.mcs.screens.login.login;

import android.content.Context;

import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.model.RegisterModel;
import mcs.nsign.by.mcs.utils.ServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public class PresenterLogin implements IPresenterLogin, ServerCallback<Map<String, Object>> {

    public IViewLogin view;
    public IModelLogin model;
    public Context context;

    public PresenterLogin(Context context, IViewLogin view) {
        model = new ModelLogin();
        this.view = view;
        this.context = context;
        model.setServerCallback(this);
    }

    public void tryToEnter(RegisterModel registerModel) {
        if (registerModel.phone.length() < 6) {
            view.showErrorName(context.getResources().getString(R.string.ERROR_LENGHT_PHONE));
            return;
        }

        if (view.checkAccessNetwork()) {
            registerModel.phone = "7" + registerModel.phone.replace("+7", "");
            registerModel.phone = registerModel.phone.replaceAll(" ", "");
            model.makeRequest(registerModel);
        } else {
            view.showErrorConnection(context.getResources().getString(R.string.ERROR_CONNECTION));
        }
    }

    @Override
    public void success(Map<String, Object> response) {
        view.success(response);
    }

    @Override
    public void error(String message) {
        view.error(message);
    }
}