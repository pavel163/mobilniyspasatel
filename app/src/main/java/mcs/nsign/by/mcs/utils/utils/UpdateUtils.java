package mcs.nsign.by.mcs.utils.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.service.SettingsService;

/**
 * Created by mac1 on 06.10.16.
 */

public class UpdateUtils {

    public static Map<String, String> getDataUpdateTable(Context context) {
        Map<String, String> dataUpdates = new HashMap<>();
        dataUpdates.put("db_date_obj_type", SettingsService.getDataTime(context, Constants.LOAD_REFERENCE_TYPE));
        dataUpdates.put("db_date_obj", SettingsService.getDataTime(context, Constants.LOAD_REFERENCE));
        dataUpdates.put("db_date_help_type", SettingsService.getDataTime(context, Constants.LOAD_HELP_TYPE));
        dataUpdates.put("db_date_help_menu", SettingsService.getDataTime(context, Constants.LOAD_HELP_ITEMS));
        dataUpdates.put("db_date_help_content", SettingsService.getDataTime(context, Constants.LOAD_HELP_CONTENT));
        dataUpdates.put(Constants.START, SettingsService.get(context, Constants.START));
        return dataUpdates;
    }

    public static Map<String, String> getDataUpdateTableTest(Context context) {
        Map<String, String> dataUpdates = new HashMap<>();
        dataUpdates.put("db_date_obj_type", "2015-09-12 13:59:06");
        dataUpdates.put("db_date_obj", "2015-09-12 13:59:06");
        dataUpdates.put("db_date_help_type", "2015-09-12 13:59:06");
        dataUpdates.put("db_date_help_menu", "2015-09-12 13:59:06");
        dataUpdates.put("db_date_help_content", "2015-09-12 13:59:06");
        return dataUpdates;
    }

    public static List<Map<String, String>> getReferencesSize(Context context) {
        List<Map<String, String>> references = new ArrayList<>();
        Map<String, String> map;

        if (!SettingsService.get(context, Constants.SIZE_OBJECT).equals("0") && !SettingsService.get(context, Constants.SIZE_OBJECT).equals("")) {
            map = new HashMap<>();
            map.put(Constants.NAME, Constants.SIZE_OBJECT);
            map.put("size", SettingsService.get(context, Constants.SIZE_OBJECT));
            map.put(Constants.DATA_UPDATE, SettingsService.getDataTime(context, Constants.LOAD_REFERENCE));
            references.add(map);
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_CONTENT).equals("0") && !SettingsService.get(context, Constants.SIZE_HELP_CONTENT).equals("")) {
            map = new HashMap<>();
            map.put(Constants.NAME, Constants.SIZE_HELP_CONTENT);
            map.put("size", SettingsService.get(context, Constants.SIZE_HELP_CONTENT));
            map.put(Constants.DATA_UPDATE, SettingsService.getDataTime(context, Constants.LOAD_HELP_CONTENT));
            references.add(map);
        }

        if (!SettingsService.get(context, Constants.SIZE_OBJECT_TYPE).equals("0") &&
                !SettingsService.get(context, Constants.SIZE_OBJECT_TYPE).equals("")) {
            map = new HashMap<>();
            map.put(Constants.NAME, Constants.SIZE_OBJECT_TYPE);
            map.put("size", SettingsService.get(context, Constants.SIZE_OBJECT_TYPE));
            map.put(Constants.DATA_UPDATE, SettingsService.getDataTime(context, Constants.LOAD_REFERENCE_TYPE));
            references.add(map);
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_MENU).equals("0") &&
                !SettingsService.get(context, Constants.SIZE_HELP_MENU).equals("")) {
            map = new HashMap<>();
            map.put(Constants.NAME, Constants.SIZE_HELP_MENU);
            map.put("size", SettingsService.get(context, Constants.SIZE_HELP_MENU));
            map.put(Constants.DATA_UPDATE, SettingsService.getDataTime(context, Constants.LOAD_HELP_ITEMS));
            references.add(map);
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_TYPE).equals("0") &&
                !SettingsService.get(context, Constants.SIZE_HELP_TYPE).equals("")) {
            map = new HashMap<>();
            map.put(Constants.NAME, Constants.SIZE_HELP_TYPE);
            map.put("size", SettingsService.get(context, Constants.SIZE_HELP_TYPE));
            map.put(Constants.DATA_UPDATE, SettingsService.getDataTime(context, Constants.LOAD_HELP_TYPE));
            references.add(map);
        }

        return references;
    }

    public static void resetSizesUpdate(Context context) {
        SettingsService.set(context, Constants.SIZE_OBJECT_TYPE, "0");
        SettingsService.set(context, Constants.SIZE_OBJECT, "0");
        SettingsService.set(context, Constants.SIZE_HELP_MENU, "0");
        SettingsService.set(context, Constants.SIZE_HELP_CONTENT, "0");
        SettingsService.set(context, Constants.SIZE_HELP_TYPE, "0");
    }

    public static String getUpdateSize(Context context) {
        int totalSize = 0;

        if (!SettingsService.get(context, Constants.SIZE_OBJECT_TYPE).equals("")) {
            totalSize += Integer.parseInt(SettingsService.get(context, Constants.SIZE_OBJECT_TYPE));
        }

        if (!SettingsService.get(context, Constants.SIZE_OBJECT).equals("")) {
            totalSize += Integer.parseInt(SettingsService.get(context, Constants.SIZE_OBJECT));
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_MENU).equals("")) {
            totalSize += Integer.parseInt(SettingsService.get(context, Constants.SIZE_HELP_MENU));
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_CONTENT).equals("")) {
            totalSize += Integer.parseInt(SettingsService.get(context, Constants.SIZE_HELP_CONTENT));
        }

        if (!SettingsService.get(context, Constants.SIZE_HELP_TYPE).equals("")) {
            totalSize += Integer.parseInt(SettingsService.get(context, Constants.SIZE_HELP_TYPE));
        }

        totalSize = totalSize / 1024;
        if (totalSize <= 100) {
            if (totalSize == 0) {
                return String.valueOf(10) + " кб";
            }
            return String.valueOf(totalSize) + " кб";
        }

        float total = (float) totalSize / 1000;

        return String.format("%.1f", total) + " мб";
    }
}
