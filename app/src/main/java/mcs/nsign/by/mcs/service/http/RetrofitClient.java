package mcs.nsign.by.mcs.service.http;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.Map;

import constants.WebSettings;
import mcs.nsign.by.mcs.model.Profile;
import mcs.nsign.by.mcs.model.RegisterModel;
import mcs.nsign.by.mcs.utils.ServerCallback;
import mcs.nsign.by.mcs.utils.ServerObserver;
import mcs.nsign.by.mcs.utils.ServerObserverFour;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private ServerObserver<Object, String> serverObserver;
    private ServerObserverFour<Object, String> serverObserver4;
    private HttpApi httpApi;
    private ServerCallback serverCallback;

    public RetrofitClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebSettings.URL)
                .client(WebSettings.getDownloadHttpClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        httpApi = retrofit.create(HttpApi.class);
    }

    public void postPhone(RegisterModel registerModel) {
        Call<Object> result = httpApi.postPhone(registerModel.phone, registerModel.imei,
                registerModel.name, registerModel.surname,
                registerModel.os, registerModel.pushToken);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (obj instanceof Map) {
                    serverCallback.success(obj);
                } else {
                    serverCallback.error("Нет доступа к интернет");
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("Нет доступа к интернет");
            }
        });
    }

    public void putPass(String pass, String id) {
        Call<Object> result = httpApi.putPass(id, pass);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (obj == null || obj instanceof List && ((List) obj).size() == 0) {
                    serverCallback.error("Неверный код");
                } else {
                    serverCallback.success(obj);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("");
            }
        });
    }

    public void repeatRequestCode(String id, String phone) {
        Call<Object> result = httpApi.repeatPostPhone(id, phone);
        load(result, "");
    }

    public void putChangeContact(String id, String name, String surname, String phone, String accesstoken) {
        String url = "user-contacts/" + id + "?access-token=" + accesstoken;
        Call<Object> result = httpApi.putChangeContact(url, name, surname, phone);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverObserver4.successExecuteObject_Three(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.d("rklogs", "onFailure putChangeContact_ " + t);
            }
        });
    }

    public void getSos(String accesstoken, String id, double latitude, double longitude, String test) {
        String url = "sos/" + id + "?latitude=" + latitude + "&longitude=" + longitude
                + "&access-token=" + accesstoken + "&test=" + test;
        Call<Object> result = httpApi.getSos(url);
        load(result, "Не удалось отправить сос");
    }

    public void getSmsStatus(String id, String accesstoken) {
        Call<Object> result = httpApi.getSmsStatus(id, accesstoken);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverObserver.successExecuteObject2(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver.failedExecute("sms status error");
            }
        });
    }

    public void deleteContact(String id, String accesstoken) {
        String url = "user-contacts/" + id + "?access-token=" + accesstoken;
        Call<Object> result = httpApi.deleteContact(url);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                serverObserver4.successExecuteObject_Four(id);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver4.failedExecute_Four("");
            }
        });
    }

    public void postContact(String user_id, String name, String surname, String phone, String accesstoken) {
        Call<Object> result = httpApi.postContact(user_id, name, surname, phone, accesstoken);
        loadFour(result, "Не удалось добавить контакт");
    }

    public void getContacts(String accesstoken, String id) {
        Call<Object> result = httpApi.getContacts(id, "contacts", accesstoken);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverObserver4.successExecuteObject_Two(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver4.failedExecute("");
            }
        });
    }

    public void changeProfile(String accesstoken, String id, Profile profile) {
        String url = "users/save-profile/" + id + "?access-token=" + accesstoken;
        Call<Object> result = httpApi.saveProfile(url, profile.name, profile.surname, profile.email, profile.blood, profile.age, profile.illness);
        load(result, "Не удалось обновить профиль");
    }

    public void getProfile(String accesstoken, String id) {
        Call<Object> result = httpApi.getProfile(id, accesstoken);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverObserver.successExecuteObject2(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver.failedExecute("Не удалось обновить профиль");
            }
        });
    }

    public void sendMessage(String id, String message) {
        Call<Object> result = httpApi.sendMessage(id, message);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverCallback.success(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("Не удалось отправить сообщение");
            }
        });
    }

    public void checkUpdateService(String accesstoken, Map<String, String> dataUpdate) {
        Call<Object> result = httpApi.checkUpdateDB(accesstoken, dataUpdate);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverCallback.success(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("Не удалось проверить обновление");
            }
        });
    }

    private void load(Call<Object> result, String errorMessage) {
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                if (obj == null) {
                    serverObserver.failedExecute("Плохое соединение");
                } else {
                    serverObserver.successExecuteObject(obj);
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver.failedExecute(errorMessage);
            }
        });
    }


    private void loadFour(Call<Object> result, String errorMessage) {
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverObserver4.successExecuteObject(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverObserver4.failedExecute(errorMessage);
            }
        });
    }

    public void sendMyCurrentLocation(String tokent, String sosId, String latitude, String longitude) {
        Call<Object> result = httpApi.sendMyCurrentLocation(tokent, sosId, latitude, longitude);
        result.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Object obj = null;
                try {
                    obj = response.body();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                serverCallback.success(obj);
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                serverCallback.error("Не удалось отправить местоположение");
            }
        });
    }

    public void setServerCallback(ServerCallback serverCallback) {
        this.serverCallback = serverCallback;
    }

    public void setServerObserver(ServerObserver<Object, String> serverObserver) {
        this.serverObserver = serverObserver;
    }

    public void setServerObserverFour(ServerObserverFour<Object, String> serverObserver4) {
        this.serverObserver4 = serverObserver4;
    }
}
