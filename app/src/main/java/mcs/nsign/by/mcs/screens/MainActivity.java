package mcs.nsign.by.mcs.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;
import mcs.nsign.by.mcs.dialogs.RatingDialogFragment;
import mcs.nsign.by.mcs.dialogs.StartMessageDialog;
import mcs.nsign.by.mcs.screens.info.FragmentInfo;
import mcs.nsign.by.mcs.screens.settings.FragmentSettings;
import mcs.nsign.by.mcs.screens.signal.FragmentSignal;
import mcs.nsign.by.mcs.service.SettingsService;
import mcs.nsign.by.mcs.service.UpdateService;
import mcs.nsign.by.mcs.utils.utils.AppUtils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentSignal mFragmentSignal;
    private FragmentSettings mFragmentSettings;
    private FragmentInfo fragmentInfo;
    private FragmentManager mFragmentTransaction;
    public NavigationView navigationView;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentTransaction = getSupportFragmentManager();
        setUIForDrawer();
        setSignalFragment();

        if (SettingsService.get(this, Constants.START).equals("")) {
            SettingsService.set(this, Constants.START, "0");
        }

        startService(new Intent(this, UpdateService.class));
        // startService(new Intent(this, PowerService.class));

        if (!SettingsService.get(this, "start_message").equals("1")) {
            DialogFragment startMessageDialog = new StartMessageDialog();
            startMessageDialog.show(getSupportFragmentManager(), "start_message");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this);
        String lat = SettingsService.get(this, Constants.LATITUDE);
        if (lat != null && !lat.equals("")) {
            Constants.latitude = Double.parseDouble(lat);
        } else {
            Constants.latitude = 0;
        }

        String lon = SettingsService.get(this, Constants.LONGITUDE);
        if (lon != null && !lon.equals("")) {
            Constants.longitude = Double.parseDouble(lon);
        } else {
            Constants.longitude = 0;
        }
    }

    public void setSignalFragment() {
        mFragmentSignal = new FragmentSignal();
        mFragmentTransaction.beginTransaction()
                .replace(R.id.fragmentContainer, mFragmentSignal)
                .commit();
    }

    public void setSettingsFragment() {
        mFragmentSettings = new FragmentSettings();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, mFragmentSettings, "settings")
                .commit();
    }

    public void setInfoFragment() {
        fragmentInfo = new FragmentInfo();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, fragmentInfo, "info")
                .commit();
    }

    public void setUIForDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Сигнал SOS");
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                AppUtils.hideKeyBoard(MainActivity.this);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((TextView) navigationView.getHeaderView(0).findViewById(R.id.user)).setText(SettingsService.get(this, Constants.NAME) + " " + SettingsService.get(this, Constants.SURNAME));
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (mFragmentTransaction.findFragmentByTag("settings") != null) {
            setSignalFragment();
            navigationView.setCheckedItem(R.id.sos);
        } else {
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.sos) {
            setSignalFragment();
        } else if (id == R.id.settings) {
            setSettingsFragment();
        } else if (id == R.id.message) {
            DialogFragment dialogFragment = new RatingDialogFragment();
            dialogFragment.show(getSupportFragmentManager(), "dialog");
        } else if (id == R.id.info) {
            setInfoFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
        SettingsService.set(this, Constants.LATITUDE, String.valueOf(Constants.latitude));
        SettingsService.set(this, Constants.LONGITUDE, String.valueOf(Constants.longitude));
    }
}
