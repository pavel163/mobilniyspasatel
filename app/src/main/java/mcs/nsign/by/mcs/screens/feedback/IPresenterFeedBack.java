package mcs.nsign.by.mcs.screens.feedback;

import android.content.Context;

/**
 * Created by mac1 on 25.08.16.
 */
public interface IPresenterFeedBack {

    void sendMessage(Context context, String id, String message);
}
