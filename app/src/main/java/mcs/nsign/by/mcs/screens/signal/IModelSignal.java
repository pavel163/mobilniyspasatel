package mcs.nsign.by.mcs.screens.signal;

import mcs.nsign.by.mcs.utils.OnServerCallback;

/**
 * Created by Ruslan on 22.11.2015.
 */
public interface IModelSignal extends OnServerCallback {

    void getSos(String token, String id, double lan, double lon, String test);

    void sendMyCurrentLocation(String token, String sosId, String latitude, String longitude);
}
