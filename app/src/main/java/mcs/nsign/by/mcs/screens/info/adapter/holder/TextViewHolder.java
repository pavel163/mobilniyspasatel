package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.view.View;
import android.widget.TextView;

import java.util.Map;

import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 24.09.2016.
 */
public class TextViewHolder extends BaseViewHolder<Map<String, Object>> {

    protected View itemView;

    public TextViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
    }

    @Override
    public void bind(Map<String, Object> data) {
        ((TextView) itemView).setText(data.get(Constants.TEXT).toString().trim());
    }
}
