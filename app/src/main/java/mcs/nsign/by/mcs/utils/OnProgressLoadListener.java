package mcs.nsign.by.mcs.utils;

/**
 * Created by mac1 on 12.10.16.
 */

public interface OnProgressLoadListener {
    void showLoadingProcess();

    void hideLoadingProcess();
}
