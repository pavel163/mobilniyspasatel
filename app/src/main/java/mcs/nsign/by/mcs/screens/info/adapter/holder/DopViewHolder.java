package mcs.nsign.by.mcs.screens.info.adapter.holder;

import android.view.View;
import android.widget.TextView;

import java.util.Map;

import mcs.nsign.by.mcs.R;
import mcs.nsign.by.mcs.constants.Constants;

/**
 * Created by Bakht on 24.09.2016.
 */
public class DopViewHolder extends BaseViewHolder<Map<String, Object>> {

    private TextView textView;

    public DopViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.text);
    }

    @Override
    public void bind(Map<String, Object> data) {
        textView.setText(data.get(Constants.TEXT).toString());
    }
}
