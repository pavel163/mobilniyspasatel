package constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by mac1 on 12.10.16.
 */

public class WebSettings {

    public static final String URL = "http://spasatel.mchs.ru/v3/";

    public static OkHttpClient getDownloadHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(chain -> {
                    Request request = chain.request().newBuilder()
                            .addHeader("Host", "spasatel.mchs.ru")
                            .addHeader("User-Agent", "Mozilla/5.0")
                            .build();
                    return chain.proceed(request);
                })
                .build();
    }
}
