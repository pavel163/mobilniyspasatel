package constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by mac1 on 12.10.16.
 */

public class WebSettings {

    public static final String URL = "http://dev.mchs.dev-vps.ru/v4/";

    public static OkHttpClient getDownloadHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(10, TimeUnit.SECONDS)
                .connectTimeout(10, TimeUnit.SECONDS)
                .build();
    }
}
